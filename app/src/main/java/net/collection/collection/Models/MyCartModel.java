package net.collection.collection.Models;

import java.util.List;

public class MyCartModel {
    private String id;
    private String name;
    private String model;
    private String option;
    private String download;
    private String quantity;
    private String substract;
    private String price;
    private String total;
    private String tax;
    private String reward;
    private String image;

    public MyCartModel(String id, String name, String model, String option,
                       String download, String quantity, String substract,
                       String price, String total, String tax, String reward, String image) {
        this.id = id;
        this.name = name;
        this.model = model;
        this.option = option;
        this.download = download;
        this.quantity = quantity;
        this.substract = substract;
        this.price = price;
        this.total = total;
        this.tax = tax;
        this.reward = reward;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSubstract() {
        return substract;
    }

    public void setSubstract(String substract) {
        this.substract = substract;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
