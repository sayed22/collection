package net.collection.collection.Models;

public class WishListModel {
    private int columnId;
    private String id;
    private String name;
    private String price;
    private String image;

    public WishListModel(int columnId, String id, String name, String price, String image) {
        this.columnId = columnId;
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
