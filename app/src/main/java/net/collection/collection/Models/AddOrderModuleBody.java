package net.collection.collection.Models;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.collection.collection.Networks.ResponseModels.AddOrderResponseModel;

import java.util.ArrayList;
import java.util.List;

public class AddOrderModuleBody {

    @Expose
    @SerializedName("order_status_id")
    private String order_status_id;
    @Expose
    @SerializedName("accept_language")
    private String acceptLanguage;
    @Expose
    @SerializedName("user_agent")
    private String userAgent;
    @Expose
    @SerializedName("forwarded_ip")
    private String forwardedIp;
    @Expose
    @SerializedName("ip")
    private String ip;
    @Expose
    @SerializedName("currency_value")
    private String currencyValue;
    @Expose
    @SerializedName("currency_code")
    private String currencyCode;
    @Expose
    @SerializedName("currency_id")
    private String currencyId;
    @Expose
    @SerializedName("language_id")
    private String languageId;
    @Expose
    @SerializedName("tracking")
    private String tracking;
    @Expose
    @SerializedName("marketing_id")
    private int marketingId;
    @Expose
    @SerializedName("commission")
    private int commission;
    @Expose
    @SerializedName("affiliate_id")
    private int affiliateId;
    @Expose
    @SerializedName("total")
    private double total;
    @Expose
    @SerializedName("comment")
    private String comment;
    @Expose
    @SerializedName("vouchers")
    private List<String> vouchers;
    @Expose
    @SerializedName("products")
    private List<CartMoudel> products;
    @Expose
    @SerializedName("shipping_code")
    private String shippingCode;
    @Expose
    @SerializedName("shipping_method")
    private String shippingMethod;
    @Expose
    @SerializedName("shipping_custom_field")
    private boolean shippingCustomField;
    @Expose
    @SerializedName("shipping_address_format")
    private String shippingAddressFormat;
    @Expose
    @SerializedName("shipping_country_id")
    private String shippingCountryId;
    @Expose
    @SerializedName("shipping_country")
    private String shippingCountry;
    @Expose
    @SerializedName("shipping_zone_id")
    private String shippingZoneId;
    @Expose
    @SerializedName("shipping_zone")
    private String shippingZone;
    @Expose
    @SerializedName("shipping_postcode")
    private String shippingPostcode;
    @Expose
    @SerializedName("shipping_city")
    private String shippingCity;
    @Expose
    @SerializedName("shipping_address_2")
    private String shippingAddress2;
    @Expose
    @SerializedName("shipping_address_1")
    private String shippingAddress1;
    @Expose
    @SerializedName("shipping_company")
    private String shippingCompany;
    @Expose
    @SerializedName("shipping_lastname")
    private String shippingLastname;
    @Expose
    @SerializedName("shipping_firstname")
    private String shippingFirstname;
    @Expose
    @SerializedName("payment_code")
    private String paymentCode;
    @Expose
    @SerializedName("payment_method")
    private String paymentMethod;
    @Expose
    @SerializedName("payment_custom_field")
    private boolean paymentCustomField;
    @Expose
    @SerializedName("payment_address_format")
    private String paymentAddressFormat;
    @Expose
    @SerializedName("payment_country_id")
    private String paymentCountryId;
    @Expose
    @SerializedName("payment_country")
    private String paymentCountry;
    @Expose
    @SerializedName("payment_zone_id")
    private String paymentZoneId;
    @Expose
    @SerializedName("payment_zone")
    private String paymentZone;
    @Expose
    @SerializedName("payment_postcode")
    private String paymentPostcode;
    @Expose
    @SerializedName("payment_city")
    private String paymentCity;
    @Expose
    @SerializedName("payment_address_2")
    private String paymentAddress2;
    @Expose
    @SerializedName("payment_address_1")
    private String paymentAddress1;
    @Expose
    @SerializedName("payment_company")
    private String paymentCompany;
    @Expose
    @SerializedName("payment_lastname")
    private String paymentLastname;
    @Expose
    @SerializedName("payment_firstname")
    private String paymentFirstname;
    @Expose
    @SerializedName("custom_field")
    private boolean customField;
    @Expose
    @SerializedName("fax")
    private String fax;
    @Expose
    @SerializedName("telephone")
    private String telephone;
    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("lastname")
    private String lastname;
    @Expose
    @SerializedName("firstname")
    private String firstname;
    @Expose
    @SerializedName("customer_group_id")
    private String customerGroupId;
    @Expose
    @SerializedName("customer_id")
    private String customerId;
    @Expose
    @SerializedName("store_url")
    private String storeUrl;
    @Expose
    @SerializedName("store_name")
    private String storeName;
    @Expose
    @SerializedName("store_id")
    private int storeId;
    @Expose
    @SerializedName("invoice_prefix")
    private String invoicePrefix;
    @Expose
    @SerializedName("totals")
    private List<Totals> totals;

    public String getorder_status_id() {
        return order_status_id;
    }

    public void setorder_status_id(String order_status_id) {
        this.order_status_id = order_status_id;
    }

    public String getAcceptLanguage() {
        return acceptLanguage;
    }

    public void setAcceptLanguage(String acceptLanguage) {
        this.acceptLanguage = acceptLanguage;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getForwardedIp() {
        return forwardedIp;
    }

    public void setForwardedIp(String forwardedIp) {
        this.forwardedIp = forwardedIp;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(String currencyValue) {
        this.currencyValue = currencyValue;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }

    public int getMarketingId() {
        return marketingId;
    }

    public void setMarketingId(int marketingId) {
        this.marketingId = marketingId;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    public int getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(int affiliateId) {
        this.affiliateId = affiliateId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<String> vouchers) {
        this.vouchers = vouchers;
    }

    public List<CartMoudel> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<CartMoudel> products) {
        this.products = products;
    }

    public String getShippingCode() {
        return shippingCode;
    }

    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public boolean getShippingCustomField() {
        return shippingCustomField;
    }

    public void setShippingCustomField(boolean shippingCustomField) {
        this.shippingCustomField = shippingCustomField;
    }

    public String getShippingAddressFormat() {
        return shippingAddressFormat;
    }

    public void setShippingAddressFormat(String shippingAddressFormat) {
        this.shippingAddressFormat = shippingAddressFormat;
    }

    public String getShippingCountryId() {
        return shippingCountryId;
    }

    public void setShippingCountryId(String shippingCountryId) {
        this.shippingCountryId = shippingCountryId;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getShippingZoneId() {
        return shippingZoneId;
    }

    public void setShippingZoneId(String shippingZoneId) {
        this.shippingZoneId = shippingZoneId;
    }

    public String getShippingZone() {
        return shippingZone;
    }

    public void setShippingZone(String shippingZone) {
        this.shippingZone = shippingZone;
    }

    public String getShippingPostcode() {
        return shippingPostcode;
    }

    public void setShippingPostcode(String shippingPostcode) {
        this.shippingPostcode = shippingPostcode;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(String shippingAddress2) {
        this.shippingAddress2 = shippingAddress2;
    }

    public String getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(String shippingAddress1) {
        this.shippingAddress1 = shippingAddress1;
    }

    public String getShippingCompany() {
        return shippingCompany;
    }

    public void setShippingCompany(String shippingCompany) {
        this.shippingCompany = shippingCompany;
    }

    public String getShippingLastname() {
        return shippingLastname;
    }

    public void setShippingLastname(String shippingLastname) {
        this.shippingLastname = shippingLastname;
    }

    public String getShippingFirstname() {
        return shippingFirstname;
    }

    public void setShippingFirstname(String shippingFirstname) {
        this.shippingFirstname = shippingFirstname;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public boolean getPaymentCustomField() {
        return paymentCustomField;
    }

    public void setPaymentCustomField(boolean paymentCustomField) {
        this.paymentCustomField = paymentCustomField;
    }

    public String getPaymentAddressFormat() {
        return paymentAddressFormat;
    }

    public void setPaymentAddressFormat(String paymentAddressFormat) {
        this.paymentAddressFormat = paymentAddressFormat;
    }

    public String getPaymentCountryId() {
        return paymentCountryId;
    }

    public void setPaymentCountryId(String paymentCountryId) {
        this.paymentCountryId = paymentCountryId;
    }

    public String getPaymentCountry() {
        return paymentCountry;
    }

    public void setPaymentCountry(String paymentCountry) {
        this.paymentCountry = paymentCountry;
    }

    public String getPaymentZoneId() {
        return paymentZoneId;
    }

    public void setPaymentZoneId(String paymentZoneId) {
        this.paymentZoneId = paymentZoneId;
    }

    public String getPaymentZone() {
        return paymentZone;
    }

    public void setPaymentZone(String paymentZone) {
        this.paymentZone = paymentZone;
    }

    public String getPaymentPostcode() {
        return paymentPostcode;
    }

    public void setPaymentPostcode(String paymentPostcode) {
        this.paymentPostcode = paymentPostcode;
    }

    public String getPaymentCity() {
        return paymentCity;
    }

    public void setPaymentCity(String paymentCity) {
        this.paymentCity = paymentCity;
    }

    public String getPaymentAddress2() {
        return paymentAddress2;
    }

    public void setPaymentAddress2(String paymentAddress2) {
        this.paymentAddress2 = paymentAddress2;
    }

    public String getPaymentAddress1() {
        return paymentAddress1;
    }

    public void setPaymentAddress1(String paymentAddress1) {
        this.paymentAddress1 = paymentAddress1;
    }

    public String getPaymentCompany() {
        return paymentCompany;
    }

    public void setPaymentCompany(String paymentCompany) {
        this.paymentCompany = paymentCompany;
    }

    public String getPaymentLastname() {
        return paymentLastname;
    }

    public void setPaymentLastname(String paymentLastname) {
        this.paymentLastname = paymentLastname;
    }

    public String getPaymentFirstname() {
        return paymentFirstname;
    }

    public void setPaymentFirstname(String paymentFirstname) {
        this.paymentFirstname = paymentFirstname;
    }

    public boolean getCustomField() {
        return customField;
    }

    public void setCustomField(boolean customField) {
        this.customField = customField;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStoreUrl() {
        return storeUrl;
    }

    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getInvoicePrefix() {
        return invoicePrefix;
    }

    public void setInvoicePrefix(String invoicePrefix) {
        this.invoicePrefix = invoicePrefix;
    }

    public List<Totals> getTotals() {
        return totals;
    }

    public void setTotals(List<Totals> totals) {
        this.totals = totals;
    }

    public static class Totals {
        @Expose
        @SerializedName("sort_order")
        private String sortOrder;
        @Expose
        @SerializedName("value")
        private double value;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("code")
        private String code;

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
