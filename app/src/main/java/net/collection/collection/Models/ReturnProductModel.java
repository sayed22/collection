package net.collection.collection.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnProductModel {
    @Expose
    @SerializedName("firstname")
    private String firstName;
    @Expose
    @SerializedName("lastname")
    private String lastname;
    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("telephone")
    private String telephone;
    @Expose
    @SerializedName("order_id")
    private String order_id;
    @Expose
    @SerializedName("date_ordered")
    private String date_ordered;
    @Expose
    @SerializedName("product")
    private String product;
    @Expose
    @SerializedName("model")
    private String model;
    @Expose
    @SerializedName("quantity")
    private String quantity;
    @Expose
    @SerializedName("return_reason_id")
    private String return_reason_id;
    @Expose
    @SerializedName("opened")
    private String opened;
    @Expose
    @SerializedName("comment")
    private String comment;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDate_ordered() {
        return date_ordered;
    }

    public void setDate_ordered(String date_ordered) {
        this.date_ordered = date_ordered;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getReturn_reason_id() {
        return return_reason_id;
    }

    public void setReturn_reason_id(String return_reason_id) {
        this.return_reason_id = return_reason_id;
    }

    public String getOpened() {
        return opened;
    }

    public void setOpened(String opened) {
        this.opened = opened;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
