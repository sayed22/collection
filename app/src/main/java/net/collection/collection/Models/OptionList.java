package net.collection.collection.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OptionList {

    public OptionList(String productOptionId, String productOptionValueId, String optionId, String optionValueId, String name, String value, String type) {
        this.type = type;
        this.value = value;
        this.name = name;
        this.optionValueId = optionValueId;
        this.optionId = optionId;
        this.productOptionValueId = productOptionValueId;
        this.productOptionId = productOptionId;
    }


    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("value")
    private String value;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("option_value_id")
    private String optionValueId;
    @Expose
    @SerializedName("option_id")
    private String optionId;
    @Expose
    @SerializedName("product_option_value_id")
    private String productOptionValueId;
    @Expose
    @SerializedName("product_option_id")
    private String productOptionId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOptionValueId() {
        return optionValueId;
    }

    public void setOptionValueId(String optionValueId) {
        this.optionValueId = optionValueId;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getProductOptionValueId() {
        return productOptionValueId;
    }

    public void setProductOptionValueId(String productOptionValueId) {
        this.productOptionValueId = productOptionValueId;
    }

    public String getProductOptionId() {
        return productOptionId;
    }

    public void setProductOptionId(String productOptionId) {
        this.productOptionId = productOptionId;
    }
}
