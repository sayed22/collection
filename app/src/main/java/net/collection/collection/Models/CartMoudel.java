package net.collection.collection.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.collection.collection.Networks.ResponseModels.CartProductsResponseModel;

import org.json.JSONObject;

import java.util.List;

@SuppressWarnings("unused")
public class CartMoudel {

    @SerializedName("download")
    private List<Object> mDownload;
    @SerializedName("height")
    private String mHeight;
    @SerializedName("image")
    private String mImage;
    @SerializedName("key")
    private String mKey;
    @SerializedName("length")
    private String mLength;
    @SerializedName("length_class_id")
    private String mLengthClassId;
    @SerializedName("minimum")
    private String mMinimum;
    @SerializedName("model")
    private String mModel;
    @SerializedName("name")
    private String mName;
    @SerializedName("option")
    private List<Object> mOption;
    @SerializedName("points")
    private Long mPoints;
    @SerializedName("price")
    private double mPrice;
    @SerializedName("product_id")
    private String mProductId;
    @SerializedName("quantity")
    private double mQuantity;
    @SerializedName("recurring")
    private Boolean mRecurring;
    @SerializedName("reward")
    private Long mReward;
    @SerializedName("shipping")
    private String mShipping;
    @SerializedName("stock")
    private Boolean mStock;
    @SerializedName("subtract")
    private String mSubtract;
    @SerializedName("tax_class_id")
    private String mTaxClassId;
    @SerializedName("total")
    private double mTotal;
    @SerializedName("weight")
    private double mWeight;
    @SerializedName("weight_class_id")
    private String mWeightClassId;
    @SerializedName("width")
    private String mWidth;

    public List<Object> getDownload() {
        return mDownload;
    }

    public void setDownload(List<Object> download) {
        mDownload = download;
    }

    public String getHeight() {
        return mHeight;
    }

    public void setHeight(String height) {
        mHeight = height;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        mKey = key;
    }

    public String getLength() {
        return mLength;
    }

    public void setLength(String length) {
        mLength = length;
    }

    public String getLengthClassId() {
        return mLengthClassId;
    }

    public void setLengthClassId(String lengthClassId) {
        mLengthClassId = lengthClassId;
    }

    public String getMinimum() {
        return mMinimum;
    }

    public void setMinimum(String minimum) {
        mMinimum = minimum;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        mModel = model;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<Object> getOption() {
        return mOption;
    }

    public void setOption(List<Object> option) {
        mOption = option;
    }

    public Long getPoints() {
        return mPoints;
    }

    public void setPoints(Long points) {
        mPoints = points;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        mProductId = productId;
    }

    public double getQuantity() {
        return mQuantity;
    }

    public void setQuantity(double quantity) {
        mQuantity = quantity;
    }

    public Boolean getRecurring() {
        return mRecurring;
    }

    public void setRecurring(Boolean recurring) {
        mRecurring = recurring;
    }

    public Long getReward() {
        return mReward;
    }

    public void setReward(Long reward) {
        mReward = reward;
    }

    public String getShipping() {
        return mShipping;
    }

    public void setShipping(String shipping) {
        mShipping = shipping;
    }

    public Boolean getStock() {
        return mStock;
    }

    public void setStock(Boolean stock) {
        mStock = stock;
    }

    public String getSubtract() {
        return mSubtract;
    }

    public void setSubtract(String subtract) {
        mSubtract = subtract;
    }

    public String getTaxClassId() {
        return mTaxClassId;
    }

    public void setTaxClassId(String taxClassId) {
        mTaxClassId = taxClassId;
    }

    public double getTotal() {
        return mTotal;
    }

    public void setTotal(double total) {
        mTotal = total;
    }

    public double getWeight() {
        return mWeight;
    }

    public void setWeight(Long weight) {
        mWeight = weight;
    }

    public String getWeightClassId() {
        return mWeightClassId;
    }

    public void setWeightClassId(String weightClassId) {
        mWeightClassId = weightClassId;
    }

    public String getWidth() {
        return mWidth;
    }

    public void setWidth(String width) {
        mWidth = width;
    }

}