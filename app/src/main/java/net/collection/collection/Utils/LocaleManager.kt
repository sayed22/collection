package net.collection.collection.Utils

import android.content.Context
import java.util.*

class LocaleManager {
    fun setLocale(context: Context): Context {
        val sharedPreferencesManagerImpl = SharedPrefLanguage(context)
        return setNewLocale(context, sharedPreferencesManagerImpl.LoadData())
    }

    fun setNewLocale(context: Context, language: String): Context {
        Locale.setDefault(Locale(language))
        return updateResources(context, language)
    }

    fun updateResources(context: Context, language: String): Context {
        val res = context.resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.setLocale(Locale(language)) // API 17+ only.
        // Use conf.locale = new Locale(...) if targeting lower versions
        res.updateConfiguration(conf, dm)
        return context.createConfigurationContext(conf)


    }


}