package net.collection.collection.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefLanguage {

    Context context;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "net.collection.collection.Utils.Language" ;
    public static String APP_LANGUAGE = null;//app rate 0 not rate 1 is rate

     public SharedPrefLanguage(Context context) {
        this.context=context;
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

    }
    public void SaveData()  {

        try

        {
            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString("App_Language", APP_LANGUAGE);
             editor.commit();
            LoadData( );
        }

        catch( Exception e)

        {
            // Toast.makeText(context, "Unable to write to the SettingFile file.", Toast.LENGTH_LONG).show();
        }
    }
    public String LoadData( ) {

        APP_LANGUAGE = sharedpreferences.getString("App_Language", null);
        return APP_LANGUAGE ;
    }

}
