package net.collection.collection.Utils;

import android.app.Application;
import android.content.res.Resources;

import com.tsengvn.typekit.Typekit;

import net.collection.collection.Activities.HomeActivity;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this,"fonts/Bahij_TheSansArabic_ExtraLight.ttf"))
        .addBold(Typekit.createFromAsset(this,"fonts/bahij_helvetica_neue_bold_by_bahijvirtualacademy-dammcmt.ttf"));

        SharedPrefLanguage sv = new SharedPrefLanguage(this);
        sv.LoadData();
        String myLanguage = sv.LoadData();
        if (myLanguage == null) {
           // String localLanguage = Resources.getSystem().getConfiguration().locale.getLanguage();
            Utils.setUpAppLanguage(this, "en");
            SharedPrefLanguage.APP_LANGUAGE = "en";
            SharedPrefLanguage sv2 = new SharedPrefLanguage(getApplicationContext());
            sv2.SaveData();
        } else {
            Utils.setUpAppLanguage(this, myLanguage);

        }
    }

}
