package net.collection.collection.Utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.collection.collection.R;

public class ViewsUtils {
    public static void showSnackBar(Context mContext, View coordinatorLayout, String message, int ColorRes) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, message, Snackbar.LENGTH_SHORT);
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        //FonTChange(mContext, textView, Fonts.regularFont);
        textView.setTextColor(ContextCompat.getColor(mContext, ColorRes));
        snackbar.show();
    }

    public static void showSnackBarWithLong(Context mContext, View coordinatorLayout, String message, int ColorRes) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
       // FonTChange(mContext, textView, Fonts.regularFont);
        textView.setTextColor(ContextCompat.getColor(mContext, ColorRes));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f);
        snackbar.show();
    }
//
//    private static void FonTChange(Context con, TextView textView, String Fonts) {
//        String fontPath = "fonts/" + Fonts;
//        // Loading Font Face
//        Typeface tf = Typeface.createFromAsset(con.getAssets(), fontPath);
//        // Applying font
//        textView.setTypeface(tf);
//    }
//
//    public static void requestFocus(View view, Window window) {
//        if (view.requestFocus()) {
//            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//        }
//    }
//
//    public static boolean validateName(AppCompatActivity context, EditText etName) {
//        if (etName.getText().toString().trim().isEmpty()) {
//            showSnackBarWithLong(context, etName, context.getResources().getString(R.string.error_user_name), R.color.colorError);
//            requestFocus(etName, context.getWindow());
//            return false;
//        }
//        return true;
//    }
//
//    public static boolean validateMessage(AppCompatActivity context, EditText etMessage) {
//        if (etMessage.getText().toString().trim().isEmpty()) {
//            showSnackBarWithLong(context, etMessage, context.getResources().getString(R.string.error_enter_message), R.color.colorError);
//            requestFocus(etMessage, context.getWindow());
//            return false;
//        }
//        return true;
//    }
//
//    public static boolean validateEmail(AppCompatActivity context, EditText etEmail) {
//        if (etEmail.getText().toString().trim().isEmpty()) {
//            showSnackBarWithLong(context, etEmail, context.getString(R.string.error_empty_email), R.color.colorError);
//            requestFocus(etEmail, context.getWindow());
//            return false;
//        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
//            showSnackBarWithLong(context, etEmail, context.getString(R.string.error_email_format), R.color.colorError);
//            requestFocus(etEmail, context.getWindow());
//            return false;
//        }
//        return true;
//    }
//
//    public static boolean validatePassword(AppCompatActivity context, EditText etPassword) {
//        if (etPassword.getText().toString().trim().isEmpty()) {
//            showSnackBarWithLong(context, etPassword, context.getResources().getString(R.string.error_empty_password), R.color.colorError);
//            requestFocus(etPassword, context.getWindow());
//            return false;
//        } else if (etPassword.getText().toString().trim().length() < 6) {
//            showSnackBarWithLong(context, etPassword, context.getResources().getString(R.string.error_password_length), R.color.colorError);
//            requestFocus(etPassword, context.getWindow());
//            return false;
//        }
//        return true;
//    }
//
//    public static boolean validateConfirmPassword(AppCompatActivity context, EditText etPassword, EditText etConfirmPassword) {
//        if (etConfirmPassword.getText().toString().trim().isEmpty()) {
//            showSnackBarWithLong(context, etConfirmPassword, context.getResources().getString(R.string.error_empty_confirm_password), R.color.colorError);
//            requestFocus(etConfirmPassword, context.getWindow());
//            return false;
//        } else if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString().trim())) {
//            showSnackBarWithLong(context, etConfirmPassword, context.getString(R.string.password_not_matched), R.color.colorError);
//            requestFocus(etConfirmPassword, context.getWindow());
//            return false;
//        }
//        return true;
//    }
//
////    public static void loadImage(Context context, String url, ImageView imageView) {
////        if (url.equals(""))
////            imageView.setImageResource(R.drawable.user);
////        else
////            Glide.with(context).load(Constants.IMAGES_BASE_URL + url).into(imageView);
////    }
//
//    public static void loadImage(Context context, int drawableID, ImageView imageView) {
//        imageView.setImageDrawable(context.getResources().getDrawable(drawableID));
//    }
//
//    public static void setTextViewDrawable(TextView textView, int drawable) {
//        Drawable img = textView.getContext().getResources().getDrawable(drawable);
//        img.setBounds(0, 0, 75, 75);
//        textView.setCompoundDrawables(null, null, img, null);
//    }
}
