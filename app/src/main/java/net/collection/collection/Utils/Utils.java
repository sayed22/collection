package net.collection.collection.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import net.collection.collection.Activities.AdressesBookActivity;
import net.collection.collection.Activities.HomeActivity;
import net.collection.collection.Activities.LogInActivity;
import net.collection.collection.Adapters.OffersAdapter;
import net.collection.collection.Adapters.WishListAdapter;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Models.WishListModel;
import net.collection.collection.Networks.WebServices.AddressesWebService;
import net.collection.collection.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static net.collection.collection.Database.DataBase.COLUMN_ID;
import static net.collection.collection.Database.DataBase.ID_COL;
import static net.collection.collection.Database.DataBase.IMAGE_COL;
import static net.collection.collection.Database.DataBase.NAME_COL;
import static net.collection.collection.Database.DataBase.PRICE_COL;

public class Utils {
    Context context;
    public static String ARABIC_LANGUAGE = "ar";
    public static String ENGLISH_LANGUAGE = "en";

    public static void Share(Context context, String text) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(sharingIntent, "Share With "));
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void callPhoneNumber(Context context, String phoneNumber) {
        Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(
                "tel", phoneNumber, null));
        context.startActivity(phoneIntent);
    }

    public static void openUrl(Context context, String url) {
        context.startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse(url)));
    }

    public static void logInFirest(final Context context) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.app_name);
        dialog.setMessage(R.string.login_first);
        dialog.setIcon(R.drawable.logo);
        dialog.setCancelable(true);
        dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(context, LogInActivity.class);
                context.startActivity(intent);
            }
        });
        dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show().cancel();
            }
        });
        dialog.show();
    }



    public static void setUpAppLanguage(Context context, String language) {



        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getApplicationContext().getResources()
                .updateConfiguration(
                        config,
                        context.getApplicationContext().getResources()
                                .getDisplayMetrics());
    }

    public static void checkIfProductInCart(Context context, String productId) {

        DataBase db = new DataBase(context);
        Cursor cr = db.getDataFromTable(db,DataBase.MY_CART_TABLE);
        if (cr.getCount() > 0) {
            cr.moveToFirst();

            do {
                if (productId.equals(cr.getString(cr.getColumnIndex(ID_COL)))) {

                }

            } while (cr.moveToNext());

        }
        cr.close();
        db.close();
    }

public static void checkConnection(Context context, CustomLoadingDialog loadingDialog){
        loadingDialog = new CustomLoadingDialog(context, R.style.DialogSlideAnim);
    if (!DetectConnection.isInternetAvailable(context)) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setIcon(context.getResources().getDrawable(R.drawable.logo));
        alertDialog.setTitle(context.getResources().getString(R.string.app_name));
        alertDialog.setMessage(context.getResources().getString(R.string.check_connection));
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, context.getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        loadingDialog.cancel();
    }
}
}
