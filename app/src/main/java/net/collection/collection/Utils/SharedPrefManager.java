package net.collection.collection.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import net.collection.collection.Models.Userdata;
import net.collection.collection.Networks.ResponseModels.WishListResponseModel;

public class SharedPrefManager {
    final static String SHARED_PREF_NAME = "net.collection.collection.Utils.shared_preferences";
    final static String LOGIN_STATUS = "net.collection.collection.Utils.login_status";
    final static String USER_OBJECT = "net.collection.collection.Utils.user_object";
    private String USER_FOLLOWERS_COUNT = "net.collection.collection.Utils.user_followers_count";
    private String IS_USER_ACTIVATE = "net.collection.collection.Utils.user_is_activate";

    Context context;


    public SharedPrefManager(Context context) {
        this.context = context;
    }

    public Boolean getLoginStatus() {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        Boolean value = sharedPreferences.getBoolean(LOGIN_STATUS, false);
        return value;
    }

    public void setLoginStatus(Boolean status) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOGIN_STATUS, status);
        editor.commit();
    }

    public Boolean isUserActivated() {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        Boolean value = sharedPreferences.getBoolean(IS_USER_ACTIVATE, false);
        return value;
    }

    public void setUserActivate(int activate) {
        boolean status = activate == 1 ? true : false;
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_USER_ACTIVATE, status);
        editor.commit();
    }

    public void setUserData(Userdata userdata) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userdata);
        editor.putString(USER_OBJECT, json);
        editor.commit();
    }

    public void setUserFollowersCount(String followersCount) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_FOLLOWERS_COUNT, followersCount);
        editor.commit();
    }

    public String getUserFollowersCount() {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        return sharedPreferences.getString(USER_FOLLOWERS_COUNT, "");
    }

    public Userdata getUserData() {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARED_PREF_NAME, 0);

        Gson gson = new Gson();
        String json = sharedPreferences.getString(USER_OBJECT, "");
        Userdata userdata = gson.fromJson(json, Userdata.class);
        return userdata;
    }

  /*  public void setWishList(WishListResponseModel wishList) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(wishList);
        editor.putString(WISH_LIST, json);
        editor.commit();
    }
    public WishListResponseModel getWishList() {
        final SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARED_PREF_NAME, 0);

        Gson gson = new Gson();
        String json = sharedPreferences.getString(WISH_LIST, "");
        WishListResponseModel wishList = gson.fromJson(json, WishListResponseModel.class);
        return wishList;
    }*/
    /**
     * this method is responsible for user logout and clearing cache
     */
//    public void Logout() {
//        LoginManager.getInstance().logOut();
//        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, 0);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.clear();
//        editor.commit();
//    }
}
