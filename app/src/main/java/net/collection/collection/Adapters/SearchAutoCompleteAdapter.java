package net.collection.collection.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.collection.collection.Networks.ResponseModels.ProductsResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import java.util.ArrayList;

public class SearchAutoCompleteAdapter extends ArrayAdapter<ProductsResponseModel.Products> {
    private final String MY_DEBUG_TAG = "SearchAutoCompleteAdapter";
    private ArrayList<ProductsResponseModel.Products> items;
    private ArrayList<ProductsResponseModel.Products> itemsAll;
    private ArrayList<ProductsResponseModel.Products> suggestions;
    Context context;
    public SearchAutoCompleteAdapter(Context context, ArrayList<ProductsResponseModel.Products> items) {
        super(context, R.layout.home_fragment, items);
        this.items = items;
        this.itemsAll = (ArrayList<ProductsResponseModel.Products>) items.clone();
        this.suggestions = new ArrayList<ProductsResponseModel.Products>();
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate( R.layout.search_autocomplete_item, null);
        }
        ProductsResponseModel.Products itemsData = items.get(position);
        if (itemsData != null) {
            TextView itemId = (TextView) v.findViewById(R.id.tv_item_id);
            TextView itemName = (TextView) v.findViewById(R.id.tv_item_name);
            ImageView img_item = (ImageView) v.findViewById(R.id.img_item);
            if (itemName != null) {
//              Log.i(MY_DEBUG_TAG, "getView Customer Name:"+customer.getName());
                itemId.setText(String.valueOf(itemsData.getId()));
                itemName.setText(itemsData.getName());
                String url = Constants.IMAGE_URL + itemsData.getThumb();
                url = url.replaceAll(" ","%20");
                System.out.println("cat image : " + url);

                Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(img_item);
            }
        }

        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((ProductsResponseModel.Products)(resultValue)).getName();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (ProductsResponseModel.Products customer : itemsAll) {
                    if(customer.getName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        suggestions.add(customer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<ProductsResponseModel.Products> filteredList = (ArrayList<ProductsResponseModel.Products>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (ProductsResponseModel.Products c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}
