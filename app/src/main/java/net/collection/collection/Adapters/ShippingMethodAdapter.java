package net.collection.collection.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import net.collection.collection.Fragments.CartFragment;
import net.collection.collection.Networks.ResponseModels.PaymentMethodResponseModel;
import net.collection.collection.Networks.ResponseModels.ShippingMethodResponseModel;
import net.collection.collection.R;

import java.util.List;

public class ShippingMethodAdapter extends RecyclerView.Adapter<ShippingMethodAdapter.listItemHolder> {

    Context context;
    List<ShippingMethodResponseModel.Shipping> shippings;
    public int mSelectedItem = -1;
    // public static ArrayList<String> productOptions = new ArrayList<String>();

    public ShippingMethodAdapter(Context context, List<ShippingMethodResponseModel.Shipping> shippings) {
        this.context = context;
        this.shippings = shippings;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.method_item_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final ShippingMethodResponseModel.Shipping shipping = shippings.get(position);
        holder.tvItemRow.setChecked(position == mSelectedItem);

        holder.tvItemRow.setText(shipping.getQuote().get(0).getTitle() + "            " + String.valueOf(shipping.getQuote().get(0).getText()));
        holder.code = shipping.getQuote().get(0).getCode();
        holder.titlle = shipping.getQuote().get(0).getTitle();
        holder.cost = shipping.getQuote().get(0).getCost();
        holder.sort_order = shipping.getSortOrder();
    }

    @Override
    public int getItemCount() {
        return shippings.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private RadioButton tvItemRow;
        String code, titlle, cost, sort_order;

        public listItemHolder(View itemView) {
            super(itemView);
            tvItemRow = (RadioButton) itemView.findViewById(R.id.tv_item_row);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, shippings.size());


                    CartFragment.shippingCode = code;
                    CartFragment.shippingTitle = titlle;
                    CartFragment.shippingValue = cost;
                    CartFragment.shippingSort_order = sort_order;
                    System.out.println("shipping data  code : " + code + " title : " + titlle + " cost : " + cost + " sort order : " + sort_order);
                    CartFragment.linShipping.setVisibility(View.VISIBLE);
                    CartFragment.tvShipping.setText(titlle);
                    CartFragment.tvShippingCost.setText(cost);
                    double final_total = Double.parseDouble(CartFragment.tvTotal.getText().toString())+Double.parseDouble(CartFragment.tvShippingCost.getText().toString());
                    CartFragment.tvFinalTotal.setText(String.valueOf(final_total));
                }

            };
            itemView.setOnClickListener(clickListener);
            tvItemRow.setOnClickListener(clickListener);
        }
    }

}
