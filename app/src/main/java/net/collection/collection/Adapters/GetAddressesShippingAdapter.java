package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.collection.collection.Activities.EditAddressActivity;
import net.collection.collection.Dialogs.CustomAddressesDialog;
import net.collection.collection.Fragments.CartFragment;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Networks.ResponseModels.GetAddressesResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;

public class GetAddressesShippingAdapter extends RecyclerView.Adapter<GetAddressesShippingAdapter.listItemHolder> {

    Context context;
    List<GetAddressesResponseModel.Addressesdata> addressesdata;


    public GetAddressesShippingAdapter(Context context, List<GetAddressesResponseModel.Addressesdata> addressesdata) {
        this.context = context;
        this.addressesdata = addressesdata;

    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_addresses_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final GetAddressesResponseModel.Addressesdata address = addressesdata.get(position);

        holder.tvAddressMenuItem.setText(address.getFirstname() + " " + address.getLastname() + "\n"
                + address.getCompany() + "\n"
                + address.getAddress1() + "\n"
                + address.getAddress2() + "\n"
                + address.getCity() + "\n"
                + address.getPostcode() + "\n"
                + address.getCountry() + "\n"
                + address.getZone());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CartFragment._ShippingFirstname = address.getFirstname();
                CartFragment._ShippingLastname = address.getLastname();
                CartFragment._ShippingCompany = address.getCompany();
                CartFragment._ShippingAddress1 = address.getAddress1();
                CartFragment._ShippingAddress2 = address.getAddress2();
                CartFragment._ShippingCity = address.getCity();
                CartFragment._ShippingPostcode = address.getPostcode();
                CartFragment._ShippingZone = address.getZone();
                CartFragment._ShippingZoneId = address.getZoneId();
                CartFragment._ShippingCountry = address.getCountry();
                CartFragment._ShippingCountryId = address.getCountryId();
                CartFragment._ShippingAddressFormat = address.getAddressFormat();
                CustomAddressesDialog.addressesShippingRecyclerView.setVisibility(View.GONE);
                CartFragment.shippingAddressChoosed = 1;

            }
        });

    }

    @Override
    public int getItemCount() {
        return addressesdata.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private TextView tvAddressMenuItem;

        public listItemHolder(View itemView) {
            super(itemView);
            tvAddressMenuItem = (TextView) itemView.findViewById(R.id.tv_address_menu_item);
        }
    }

}
