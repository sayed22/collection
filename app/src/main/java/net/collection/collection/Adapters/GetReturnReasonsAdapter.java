package net.collection.collection.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import net.collection.collection.Networks.ResponseModels.GetReturnReasonsResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;

public class GetReturnReasonsAdapter extends RecyclerView.Adapter<GetReturnReasonsAdapter.listItemHolder> {

    Context context;
    List<GetReturnReasonsResponseModel.Returnreasons> returnreasons;
    public int mSelectedItem = -1;


    public GetReturnReasonsAdapter(Context context, List<GetReturnReasonsResponseModel.Returnreasons> returnreasons) {
        this.context = context;
        this.returnreasons = returnreasons;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_option_item_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return  holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final GetReturnReasonsResponseModel.Returnreasons reason = returnreasons.get(position);
        holder.tvItemRow.setChecked(position == mSelectedItem);

        holder.tvItemRow.setText(reason.getName());
        holder.returnReasonId=reason.getReturnReasonId();
    }

    @Override
    public int getItemCount() {
        return returnreasons.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private RadioButton tvItemRow;
        private String returnReasonId;
        public listItemHolder(View itemView) {
            super(itemView);
            tvItemRow = (RadioButton) itemView.findViewById(R.id.tv_item_row);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, returnreasons.size());
                    Constants.returnReasonId=returnReasonId;

                }
            };
            itemView.setOnClickListener(clickListener);
            tvItemRow.setOnClickListener(clickListener);

        }
    }

}
