package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Activities.SectionProductsActivity;
import net.collection.collection.Networks.ResponseModels.SectionsResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;

public class SectionsAdapter extends RecyclerView.Adapter<SectionsAdapter.listItemHolder> {

    Context context;
    List<SectionsResponseModel.CategoryInfo> sectionsModels;


    public SectionsAdapter(Context context, List<SectionsResponseModel.CategoryInfo> sectionsModels) {
        this.context = context;
        this.sectionsModels = sectionsModels;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.sections_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final SectionsResponseModel.CategoryInfo sectionsModel = sectionsModels.get(position);

        String url = Constants.IMAGE_URL + sectionsModel.getImage();
        url = url.replaceAll(" ", "%20");

        Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgOne);

        //holder.tvTypeName.setText(sectionsModel.getName());
        holder.name = sectionsModel.getName();
        holder.name = holder.name.replaceAll("&amp;", " & ");
        holder.tvTypeName.setText(holder.name);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SectionProductsActivity.class);
                intent.putExtra("produact_id", String.valueOf(sectionsModel.getCategoryId()));
                intent.putExtra("produact_name", sectionsModel.getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sectionsModels.size();
    }


    class listItemHolder extends RecyclerView.ViewHolder {
        String name;
        private ImageView imgOne;
        private TextView tvTypeName;

        public listItemHolder(View itemView) {
            super(itemView);

            imgOne = (ImageView) itemView.findViewById(R.id.img_one);
            tvTypeName = (TextView) itemView.findViewById(R.id.tv_type_name);
        }
    }

}
