package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Activities.ReturnProductActivity;
import net.collection.collection.Networks.ResponseModels.OneOrderResponseModel;
import net.collection.collection.Networks.WebServices.CartWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.SharedPrefManager;

import java.util.List;

public class OneOrderProductsAdapter extends RecyclerView.Adapter<OneOrderProductsAdapter.listItemHolder> {

    Context context;
    List<OneOrderResponseModel.Products> products;


    public OneOrderProductsAdapter(Context context, List<OneOrderResponseModel.Products> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.one_order_products_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final OneOrderResponseModel.Products product = products.get(position);

        holder.tvTotal.setText(product.getTotal());
        holder.tvName.setText(product.getName());
        holder.tvType.setText(product.getModel());
        holder.tvQuantity.setText(product.getQuantity());
        holder.tvPrice.setText(product.getPrice());
        for (int i = 0; i < product.getOption().size(); i++) {
            if (TextUtils.isEmpty(holder.tvOptions.getText().toString())) {

                holder.tvOptions.setText(holder.tvOptions.getText().toString() + "" + product.getOption().get(i).getName());
            } else {
                holder.tvOptions.setText(holder.tvOptions.getText().toString() + "  ـ  " + product.getOption().get(i).getName());
            }
            System.out.println("product option : " + product.getOption().get(i).getName() + " / " + product.getOption().get(i).getValue()
                    + product.getOption().get(i).getProductOptionValueId() + "/" + product.getOption().get(i).getProductOptionId());
        }
        if (TextUtils.isEmpty(holder.tvOptions.getText().toString())) {
            holder.tvOptions.setVisibility(View.GONE);
        }
        holder.btnReOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductDataActivity.class);
                intent.putExtra("produact_id", String.valueOf(product.getProductId()));
                intent.putExtra("produact_name", product.getName());
                context.startActivity(intent);
//                SharedPrefManager sharedPrefManager = new SharedPrefManager(context.getApplicationContext());
//                if (sharedPrefManager.getLoginStatus()) {
//                    String product_options[] = product.getOption().toArray(new String[product.getOption().size()]);
//
//                    CartWebService.addProductToCart(sharedPrefManager.getUserData().getCustomerId(), product.getProductId(), "1", product_options);
//                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.product_added_to_cart), Toast.LENGTH_LONG).show();
//                }
            }
        });

        holder.btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReturnProductActivity.class);
                intent.putExtra("order_id", product.getOrderId());
                intent.putExtra("product_id", String.valueOf(product.getProductId()));
                intent.putExtra("productName", product.getName());
                intent.putExtra("model", product.getModel());
                intent.putExtra("quantity", product.getQuantity());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvType;
        private TextView tvQuantity;
        private TextView tvPrice;
        private TextView tvTotal;
        private TextView tvOptions;
        private Button btnReOrder;
        private Button btnReturn;

        public listItemHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvType = (TextView) itemView.findViewById(R.id.tv_type);
            tvQuantity = (TextView) itemView.findViewById(R.id.tv_quantity);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            tvTotal = (TextView) itemView.findViewById(R.id.tv_total);
            tvOptions = (TextView) itemView.findViewById(R.id.tv_options);
            btnReOrder = (Button) itemView.findViewById(R.id.btn_re_order);
            btnReturn = (Button) itemView.findViewById(R.id.btn_return);
        }
    }

}
