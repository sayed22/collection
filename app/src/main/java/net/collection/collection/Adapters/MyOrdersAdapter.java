package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.collection.collection.Activities.EditAddressActivity;
import net.collection.collection.Activities.OneOrderDetailsActivity;
import net.collection.collection.Networks.ResponseModels.GetOrdersResponseModel;
import net.collection.collection.R;

import java.util.List;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.listItemHolder> {

    Context context;
    List<GetOrdersResponseModel.Orders> orders;


    public MyOrdersAdapter(Context context, List<GetOrdersResponseModel.Orders> orders) {
        this.context = context;
        this.orders = orders;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_orders_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final GetOrdersResponseModel.Orders order = orders.get(position);

        holder.tvOrderNo.setText(order.getOrderId());
        holder.tvOrderStatus.setText(order.getStatus());
        holder.tvOrderDate.setText(order.getDateAdded());
        holder.tvProductsNo.setText(String.valueOf(order.getProducts()));
        holder.tvClient.setText(order.getName());
        holder.tvTotal.setText(order.getTotal());
        holder.sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OneOrderDetailsActivity.class);
                intent.putExtra("order_id", order.getOrderId());
                intent.putExtra("order_status", order.getStatus());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }


    class listItemHolder extends RecyclerView.ViewHolder {
        private TextView tvOrderNo;
        private TextView tvOrderStatus;
        private TextView tvOrderDate;
        private TextView tvProductsNo;
        private TextView tvClient;
        private TextView tvTotal;
        private Button sendRequest;

        public listItemHolder(View itemView) {
            super(itemView);
            tvOrderNo = (TextView) itemView.findViewById(R.id.tv_order_no);
            tvOrderStatus = (TextView) itemView.findViewById(R.id.tv_order_status);
            tvOrderDate = (TextView) itemView.findViewById(R.id.tv_order_date);
            tvProductsNo = (TextView) itemView.findViewById(R.id.tv_products_no);
            tvClient = (TextView) itemView.findViewById(R.id.tv_client);
            tvTotal = (TextView) itemView.findViewById(R.id.tv_total);
            sendRequest = (Button) itemView.findViewById(R.id.send_request);
        }
    }

}
