package net.collection.collection.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.AdressesBookActivity;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Networks.ResponseModels.CountryZonesResponseModel;
import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;

public class CountryZonesAdapter extends RecyclerView.Adapter<CountryZonesAdapter.listItemHolder> {

    Context context;
    List<CountryZonesResponseModel.Zones> countryZones;
    IRecyclerItemClicked mRecyclerItemClickListener;


    public CountryZonesAdapter(Context context, List<CountryZonesResponseModel.Zones> countryZones, IRecyclerItemClicked mRecyclerItemClickListener) {
        this.context = context;
        this.countryZones = countryZones;
        this.mRecyclerItemClickListener = mRecyclerItemClickListener;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.countries_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final CountryZonesResponseModel.Zones zone = countryZones.get(position);

        holder.tvAddressMenuItem.setText(zone.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.itemClicked = 2;
                Constants.zoneId = zone.getZoneId();
                Constants.zoneName = zone.getName();
                mRecyclerItemClickListener.onRecyclerItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return countryZones.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private TextView tvAddressMenuItem;

        public listItemHolder(View itemView) {
            super(itemView);
            tvAddressMenuItem = (TextView) itemView.findViewById(R.id.tv_address_menu_item);

        }
    }

}
