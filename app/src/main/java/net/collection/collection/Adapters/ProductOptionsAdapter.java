package net.collection.collection.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.R;

import java.util.List;

public class ProductOptionsAdapter extends RecyclerView.Adapter<ProductOptionsAdapter.listItemHolder> {

    Context context;
    List<ProductOptionsResponseModel.ProductsOption> productsOptions;


    public ProductOptionsAdapter(Context context, List<ProductOptionsResponseModel.ProductsOption> productsOptions) {
        this.context = context;
        this.productsOptions = productsOptions;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_option_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final ProductOptionsResponseModel.ProductsOption option = productsOptions.get(position);

        holder.tvProductOption.setText(option.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.productItemRecyclerView.getVisibility() == View.VISIBLE) {
                    holder.productItemRecyclerView.setVisibility(View.GONE);
                } else {
                    holder.productItemRecyclerView.setVisibility(View.VISIBLE);
                }
                System.out.println("ProductOptionId : " + option.getProductOptionId() + "OptionId : " + option.getOptionId()
                        + "Value : " + option.getValue() + "Type :" + option.getType());
                List<ProductOptionsResponseModel.ProductOptionValue> optionProductValue = option.getProductOptionValue();
                ProductOptionsItemAdapter adapter = new ProductOptionsItemAdapter(context, optionProductValue,
                        holder.tvProductOption.getText().toString(), option.getProductOptionId(), option.getOptionId(),
                        option.getValue(), option.getType());
                holder.productItemRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                holder.productItemRecyclerView.setNestedScrollingEnabled(false);
                holder.productItemRecyclerView.setAdapter(adapter);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productsOptions.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private TextView tvProductOption;
        private RecyclerView productItemRecyclerView;

        public listItemHolder(View itemView) {
            super(itemView);
            tvProductOption = (TextView) itemView.findViewById(R.id.tv_product_option);
            productItemRecyclerView = (RecyclerView) itemView.findViewById(R.id.product_item_recycler_view);

        }
    }

}
