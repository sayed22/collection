package net.collection.collection.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import net.collection.collection.Fragments.CartFragment;
import net.collection.collection.Models.OptionList;
import net.collection.collection.Networks.ResponseModels.PaymentMethodResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.R;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.listItemHolder> {

    Context context;
    List<PaymentMethodResponseModel.Payment> payments;
    public int mSelectedItem = -1;
    // public static ArrayList<String> productOptions = new ArrayList<String>();

    public PaymentMethodAdapter(Context context, List<PaymentMethodResponseModel.Payment> payments) {
        this.context = context;
        this.payments = payments;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.method_item_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final PaymentMethodResponseModel.Payment payment = payments.get(position);
        holder.tvItemRow.setChecked(position == mSelectedItem);

        holder.tvItemRow.setText(payment.getTitle());
        holder.code = payment.getCode();
        holder.titlle = payment.getTitle();
        holder.sort_order = payment.getSortOrder();    }

    @Override
    public int getItemCount() {
        return payments.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private RadioButton tvItemRow;
        String code, titlle, sort_order;

        public listItemHolder(View itemView) {
            super(itemView);
            tvItemRow = (RadioButton) itemView.findViewById(R.id.tv_item_row);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, payments.size());

                            CartFragment.paymentCode = code;
                            CartFragment.paymentTitle = titlle;
                            CartFragment.paymentSort_order = sort_order;
                            System.out.println("payment data  code : " + code + " title : " + titlle + " cost : " + " sort order : " + sort_order);

                }

            };
            itemView.setOnClickListener(clickListener);
            tvItemRow.setOnClickListener(clickListener);
        }
    }

}
