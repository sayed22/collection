package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Models.MyCartModel;
import net.collection.collection.Models.OptionList;
import net.collection.collection.Models.WishListModel;

import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import static net.collection.collection.Database.DataBase.DOWNLOAD_COL;
import static net.collection.collection.Database.DataBase.ID_COL;
import static net.collection.collection.Database.DataBase.IMAGE_COL;
import static net.collection.collection.Database.DataBase.MODEL_COL;
import static net.collection.collection.Database.DataBase.NAME_COL;
import static net.collection.collection.Database.DataBase.OPTION_COL;
import static net.collection.collection.Database.DataBase.PRICE_COL;
import static net.collection.collection.Database.DataBase.QUANTITY_COL;
import static net.collection.collection.Database.DataBase.REWARD_COL;
import static net.collection.collection.Database.DataBase.SUBSTRACT_COL;
import static net.collection.collection.Database.DataBase.TAX_COL;
import static net.collection.collection.Database.DataBase.TOTAL_COL;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.listItemHolder> {

    Context context;
    List<WishListModel> wishList;
    Gson gson = new Gson();

    public WishListAdapter(Context context, List<WishListModel> wishList) {
        this.context = context;
        this.wishList = wishList;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.wish_list_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final WishListModel wish = wishList.get(position);

        String url = Constants.IMAGE_URL + wish.getImage();
        url = url.replaceAll(" ", "%20");
        System.out.println("cat image : " + url);

        Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgProduct);
        holder.tvProductName.setText(wish.getName());
        // holder.tvProductType.setText(wish.getModel());
        holder.tvItemPrice.setText(wish.getPrice());
        // holder.tvStockStatus.setText(wish.getStock_status());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductDataActivity.class);
                intent.putExtra("produact_id", wish.getId());
                intent.putExtra("produact_name", wish.getName());
                context.startActivity(intent);
            }
        });
        holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
            ArrayList<MyCartModel> cartMoudels = new ArrayList<>();
            MyCartModel listModel;
            boolean isExist;

            @Override
            public void onClick(View view) {
                DataBase db = new DataBase(context);
                Cursor cr = db.getDataFromTable(db, DataBase.MY_CART_TABLE);
                List<OptionList> productOptions = new ArrayList<>();
                String json = gson.toJson(productOptions);
                System.out.println(json);
                if (cr.getCount() > 0) {
                    cr.moveToFirst();
                    do {
                        listModel = new MyCartModel(cr.getString(cr.getColumnIndex(ID_COL))
                                , cr.getString(cr.getColumnIndex(NAME_COL))
                                , cr.getString(cr.getColumnIndex(MODEL_COL))
                                , cr.getString(cr.getColumnIndex(OPTION_COL))
                                , cr.getString(cr.getColumnIndex(DOWNLOAD_COL))
                                , cr.getString(cr.getColumnIndex(QUANTITY_COL))
                                , cr.getString(cr.getColumnIndex(SUBSTRACT_COL))
                                , cr.getString(cr.getColumnIndex(PRICE_COL))
                                , cr.getString(cr.getColumnIndex(TOTAL_COL))
                                , cr.getString(cr.getColumnIndex(TAX_COL))
                                , cr.getString(cr.getColumnIndex(REWARD_COL))
                                , cr.getString(cr.getColumnIndex(IMAGE_COL)));
                        cartMoudels.add(listModel);
                    } while (cr.moveToNext());

                    for (int i = 0; i < cartMoudels.size(); i++) {
                        if (cartMoudels.get(i).getId().equals(wish.getId())) {
                            isExist = true;
                            break;
                        } else {
                            isExist = false;
                        }
                    }
                    if (isExist) {
                        Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.product_exist), Toast.LENGTH_LONG).show();
                    } else {
                        Animation animZoomIn = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
                        holder.btnAddToCart.startAnimation(animZoomIn);
                        String price = wish.getPrice();
                        price = price.replaceAll("KD", "");
                        //price = price.replaceAll("[^\\d-]", "");
                        DataBase db2 = new DataBase(context);

                        db2.insertIntoMyCart(db2
                                , String.valueOf(wish.getId())
                                , wish.getName()
                                , ""
                                , json
                                , ""
                                , "1"
                                , ""
                                , price
                                , price
                                , "1"
                                , ""
                                , wish.getImage());
                        db2.close();
                        Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.product_added_to_cart), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Animation animZoomIn = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
                    holder.btnAddToCart.startAnimation(animZoomIn);
                    String price = wish.getPrice();
                    price = price.replaceAll("KD", "");
                    // price = price.replaceAll("[^\\d-]", "");
                    DataBase db2 = new DataBase(context);
                    db2.insertIntoMyCart(db2
                            , String.valueOf(wish.getId())
                            , wish.getName()
                            , ""
                            , json
                            , ""
                            , "1"
                            , ""
                            , price
                            , price
                            , "1"
                            , ""
                            , wish.getImage());
                    db2.close();
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.product_added_to_cart), Toast.LENGTH_LONG).show();
                }
                cr.close();
                db.close();

            }

        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animZoomIn = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
                holder.btnDelete.startAnimation(animZoomIn);
                DataBase db = new DataBase(context);
                db.deleteSingleRow(db, DataBase.MY_WISH_LIST_TABLE, wish.getId());
                db.close();
                Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.removed_from_wish_list), Toast.LENGTH_LONG).show();
                wishList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, wishList.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return wishList.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {
        private ImageView imgProduct;
        private TextView tvProductName;
        private TextView tvItemPrice;
        private Button btnAddToCart;
        private Button btnDelete;

        public listItemHolder(View itemView) {
            super(itemView);
            imgProduct = (ImageView) itemView.findViewById(R.id.img_product);
            tvProductName = (TextView) itemView.findViewById(R.id.tv_product_name);
            tvItemPrice = (TextView) itemView.findViewById(R.id.tv_item_price);
            btnAddToCart = (Button) itemView.findViewById(R.id.btn_add_to_cart);
            btnDelete = (Button) itemView.findViewById(R.id.btn_delete);
        }
    }

}
