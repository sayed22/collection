package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.SectionProductsActivity;
import net.collection.collection.Networks.Api.ApiClient;
import net.collection.collection.Networks.Api.ApiInterface;
import net.collection.collection.Networks.Events.SectionProductsEvent;
import net.collection.collection.Networks.ResponseModels.GetChildCategoryResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionProductsResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SectionCategoryAdapter extends RecyclerView.Adapter<SectionCategoryAdapter.listItemHolder> {

    Context context;
    List<GetChildCategoryResponseModel.CategoryInfo> sectionCategory;


    public SectionCategoryAdapter(Context context, List<GetChildCategoryResponseModel.CategoryInfo> sectionCategory) {
        this.context = context;
        this.sectionCategory = sectionCategory;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.sections_category_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final GetChildCategoryResponseModel.CategoryInfo categoryInfo = sectionCategory.get(position);
        String url = Constants.IMAGE_URL + categoryInfo.getImage();
        url = url.replaceAll(" ", "%20");

        Glide.with(context).load(url).asBitmap().placeholder(context.getResources().getDrawable(R.drawable.placeholder)).override(900, 500).into(holder.imgOne);
        holder.name = categoryInfo.getName();
        holder.name = holder.name.replaceAll("&amp;", " & ");

        holder.tvTypeName.setText(holder.name + "(" + String.valueOf(categoryInfo.getProductNo()) + ")");
        // Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgOne);
        //Picasso.with(context).load("http://brook1.com/image/catalog/الرحلات%20البريه/item_XL_6617463_4166801.jpg").into(holder.imgOne);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SectionProductsActivity.class);
                intent.putExtra("produact_id", String.valueOf(categoryInfo.getCategoryId()));
                intent.putExtra("produact_name", categoryInfo.getName());
                context.startActivity(intent);
            }
        });

//
//
//        ApiInterface apiService =
//                ApiClient.getClient().create(ApiInterface.class);
//        Call<SectionProductsResponseModel> call = apiService.GET_CATEGORY_PRODUCTS_RESPONSE_MODEL_CALL(String.valueOf(categoryInfo.getCategoryId()),1);
//        call.enqueue(new Callback<SectionProductsResponseModel>() {
//            @Override
//            public void onResponse(Call<SectionProductsResponseModel> call, Response<SectionProductsResponseModel> response) {
//                if (response.body().getSuccess()) {
//                    holder.name = categoryInfo.getName();
//                    holder.name = holder.name.replaceAll("&amp;", " & ");
//
//                    holder.tvTypeName.setText(holder.name + "(" + String.valueOf(categoryInfo.getProductNo()) + ")");
//                }else {
//                    holder.tvTypeName.setText(categoryInfo.getName());
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SectionProductsResponseModel> call, Throwable t) {
//                // Log error here since request failed
//                holder.tvTypeName.setText(categoryInfo.getName());
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return sectionCategory.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {
        private ImageView imgOne;
        private TextView tvTypeName;
        String name;

        public listItemHolder(View itemView) {
            super(itemView);
            imgOne = (ImageView) itemView.findViewById(R.id.img_one);
            tvTypeName = (TextView) itemView.findViewById(R.id.tv_type_name);
        }
    }

}
