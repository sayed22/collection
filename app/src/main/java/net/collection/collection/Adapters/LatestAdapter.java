package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.HomeActivity;
import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Networks.ResponseModels.LatestResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;


public class LatestAdapter extends RecyclerView.Adapter<LatestAdapter.listItemHolder> {

    Context context;
    List<LatestResponseModel.Latest> latests;

    public LatestAdapter(Context context, List<LatestResponseModel.Latest> latests) {
        this.context = context;
        this.latests = latests;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.latest_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final LatestResponseModel.Latest latest = latests.get(position);

        String url = Constants.IMAGE_URL + latest.getThumb();
        url = url.replaceAll(" ","%20");
        System.out.println("cat image : " + url);

        Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgOne);
        holder.tvTypeName.setText(latest.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductDataActivity.class);
                intent.putExtra("produact_id", latest.getProductId());
                intent.putExtra("produact_name", latest.getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return latests.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private ImageView imgOne;
        private TextView tvTypeName;

        public listItemHolder(View itemView) {
            super(itemView);
            imgOne = (ImageView) itemView.findViewById(R.id.img_one);
            tvTypeName = (TextView) itemView.findViewById(R.id.tv_type_name);


        }
    }

}
