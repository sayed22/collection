package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Models.MyCartModel;
import net.collection.collection.Models.OptionList;
import net.collection.collection.Networks.ResponseModels.SectionProductsResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import static net.collection.collection.Database.DataBase.DOWNLOAD_COL;
import static net.collection.collection.Database.DataBase.ID_COL;
import static net.collection.collection.Database.DataBase.IMAGE_COL;
import static net.collection.collection.Database.DataBase.MODEL_COL;
import static net.collection.collection.Database.DataBase.NAME_COL;
import static net.collection.collection.Database.DataBase.OPTION_COL;
import static net.collection.collection.Database.DataBase.PRICE_COL;
import static net.collection.collection.Database.DataBase.QUANTITY_COL;
import static net.collection.collection.Database.DataBase.REWARD_COL;
import static net.collection.collection.Database.DataBase.SUBSTRACT_COL;
import static net.collection.collection.Database.DataBase.TAX_COL;
import static net.collection.collection.Database.DataBase.TOTAL_COL;

public class SectionProductsAdapter extends RecyclerView.Adapter<SectionProductsAdapter.listItemHolder> {

    Gson gson = new Gson();
    private String json;


    Context context;
    List<SectionProductsResponseModel.Products> sectionProducts;

    public SectionProductsAdapter(Context context, List<SectionProductsResponseModel.Products> sectionProducts) {
        this.context = context;
        this.sectionProducts = sectionProducts;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.offers_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final SectionProductsResponseModel.Products products = sectionProducts.get(position);
        DataBase db = new DataBase(context);
        Cursor cr = db.getDataFromTable(db, DataBase.MY_WISH_LIST_TABLE);
        if (cr.getCount() > 0) {
            cr.moveToFirst();
            do {
                if (products.getId().equals(cr.getString(cr.getColumnIndex(ID_COL)))) {
                    holder.imgLike.setImageResource(R.drawable.ic_is_like);
                    holder.isLiked = true;
                }
            } while (cr.moveToNext());
        }
        cr.close();
        db.close();
        holder.imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animZoomIn = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
                holder.imgLike.startAnimation(animZoomIn);
                if (!holder.isLiked) {
                    holder.imgLike.setImageResource(R.drawable.ic_is_like);
                    Toast.makeText(context, context.getResources().getString(R.string.added_to_your_wish_list), Toast.LENGTH_LONG).show();
                    DataBase db = new DataBase(context);
                    String price;
                    if (products.getSpecial().equals("0")) {
                        price = products.getPrice();
                    } else {
                        price = products.getSpecial();
                    }
                    db.insertIntoMyWishList(db
                            , products.getId()
                            , products.getName()
                            , price
                            , products.getThumb());
                    db.close();
                } else {
                    holder.imgLike.setImageResource(R.drawable.ic_like);
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.removed_from_wish_list), Toast.LENGTH_LONG).show();
                    DataBase db = new DataBase(context);
                    db.deleteSingleRow(db, DataBase.MY_WISH_LIST_TABLE, products.getId());
                    db.close();
                }
                holder.isLiked = !holder.isLiked;
                //notifyDataSetChanged();
            }
        });

        String url = Constants.IMAGE_URL + products.getThumb();
        url = url.replaceAll(" ", "%20");

        Glide.with(context).load(url).asBitmap().placeholder(context.getResources().getDrawable(R.drawable.placeholder)).override(900, 500).into(holder.imgOne);

        //Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgOne);
        holder.tvDeprecatedPrice.setText(products.getPrice());
        holder.tvNewPrice.setText(products.getSpecial());
        if (products.getSpecial().equals("0")) {
            holder.tvNewPrice.setText(products.getPrice());
            holder.tvDeprecatedPrice.setVisibility(View.GONE);
        }
        holder.tvItemName.setText(products.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductDataActivity.class);
                intent.putExtra("produact_id", products.getId());
                intent.putExtra("produact_name", products.getName());
                context.startActivity(intent);
            }
        });


        holder.imgAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<OptionList> productOptions = new ArrayList<>();
                productOptions = ProductOptionsItemAdapter.productOptions;

                json = gson.toJson(productOptions);
                System.out.println(json);

                ArrayList<MyCartModel> cartMoudels = new ArrayList<>();
                MyCartModel listModel;
                boolean isExist = false;
                DataBase db = new DataBase(context);
                Cursor cr = db.getDataFromTable(db, DataBase.MY_CART_TABLE);
                if (cr.getCount() > 0) {
                    cr.moveToFirst();
                    do {
                        listModel = new MyCartModel(cr.getString(cr.getColumnIndex(ID_COL))
                                , cr.getString(cr.getColumnIndex(NAME_COL))
                                , cr.getString(cr.getColumnIndex(MODEL_COL))
                                , cr.getString(cr.getColumnIndex(OPTION_COL))
                                , cr.getString(cr.getColumnIndex(DOWNLOAD_COL))
                                , cr.getString(cr.getColumnIndex(QUANTITY_COL))
                                , cr.getString(cr.getColumnIndex(SUBSTRACT_COL))
                                , cr.getString(cr.getColumnIndex(PRICE_COL))
                                , cr.getString(cr.getColumnIndex(TOTAL_COL))
                                , cr.getString(cr.getColumnIndex(TAX_COL))
                                , cr.getString(cr.getColumnIndex(REWARD_COL))
                                , cr.getString(cr.getColumnIndex(IMAGE_COL)));
                        cartMoudels.add(listModel);
                    } while (cr.moveToNext());

                    for (int i = 0; i < cartMoudels.size(); i++) {
                        if (cartMoudels.get(i).getId().equals(products.getId())) {
                            isExist = true;
                            break;
                        } else {
                            isExist = false;
                        }
                    }
                    if (isExist) {
                        Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.product_exist), Toast.LENGTH_LONG).show();
                    } else {
                        addToCart(holder, products);
                    }
                } else {
                    addToCart(holder, products);
                }
                cr.close();
                db.close();

            }
        });
    }


    public void addToCart(listItemHolder listItemHolder, SectionProductsResponseModel.Products products) {
        String price = listItemHolder.tvNewPrice.getText().toString();
        price = price.replaceAll("KD", "");
        // price = price.replaceAll("[^\\d-]", "");
        double total = Double.parseDouble(price) * Double.parseDouble("1");
        DataBase db2 = new DataBase(context);
        System.out.println("array :" + json);
//        String type = tvProductType.getText().toString();
//        type = type.replace(getResources().getString(R.string.type), "");
        db2.insertIntoMyCart(db2
                , String.valueOf(products.getId())
                , products.getName()
                , ""
                , json
                , ""
                , "1"
                , ""
                , price
                , String.valueOf(total)
                , ""
                , ""
                , products.getThumb());
        db2.close();
        Toast.makeText(context, context.getResources().getString(R.string.product_added_to_cart), Toast.LENGTH_LONG).show();
    }


    @Override
    public int getItemCount() {
        return sectionProducts.size();
    }


    public void addProductsList(ArrayList<SectionProductsResponseModel.Products> productsList) {
        this.sectionProducts = productsList;
        notifyDataSetChanged();

    }

    class listItemHolder extends RecyclerView.ViewHolder {
        private ImageView imgOne;
        private ImageView imgLike;

        private ImageView imgAddToCart;

        private TextView tvItemName;
        private TextView tvDeprecatedPrice;
        private TextView tvNewPrice;
        boolean isLiked = false;

        public listItemHolder(View itemView) {
            super(itemView);
            imgOne = (ImageView) itemView.findViewById(R.id.img_one);
            imgLike = (ImageView) itemView.findViewById(R.id.img_like);
            tvItemName = (TextView) itemView.findViewById(R.id.tv_item_name);
            tvDeprecatedPrice = (TextView) itemView.findViewById(R.id.tv_deprecated_price);
            tvNewPrice = (TextView) itemView.findViewById(R.id.tv_new_price);

            imgAddToCart = (ImageView) itemView.findViewById(R.id.img_add_to_cart);
        }
    }

}
