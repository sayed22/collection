package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Models.WishListModel;
import net.collection.collection.Networks.ResponseModels.RelatedProductsResponseModel;
import net.collection.collection.Networks.WebServices.WishListWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static net.collection.collection.Database.DataBase.ID_COL;

public class RelatedProductsAdapter extends RecyclerView.Adapter<RelatedProductsAdapter.listItemHolder> {

    Context context;
    List<RelatedProductsResponseModel.Relatedproduct> relatedproducts;


    public RelatedProductsAdapter(Context context, List<RelatedProductsResponseModel.Relatedproduct> relatedproducts) {
        this.context = context;
        this.relatedproducts = relatedproducts;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.offers_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final RelatedProductsResponseModel.Relatedproduct related = relatedproducts.get(position);

        String url = related.getThumb();
        url = url.replaceAll(" ","%20");
        System.out.println("cat image : " + url);

        Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgOne);
        holder.tvDeprecatedPrice.setText(String.valueOf(related.getPrice()));
        holder.tvNewPrice.setText(String.valueOf(related.getSpecial()));
        holder.tvItemName.setText(related.getName());
        holder.imgOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductDataActivity.class);
                intent.putExtra("produact_id", related.getProductId());
                intent.putExtra("produact_name", related.getName());
                context.startActivity(intent);
            }
        });
        if (related.getSpecial().equals("0")) {
            holder.tvNewPrice.setText(related.getPrice());
            holder.tvDeprecatedPrice.setVisibility(View.GONE);
        }
        List<WishListModel> wishListModels = new ArrayList<>();
        WishListModel listModel;
        DataBase db = new DataBase(context);
        Cursor cr = db.getDataFromTable(db, DataBase.MY_WISH_LIST_TABLE);
        if (cr.getCount() > 0) {
            cr.moveToFirst();

            do {
                if (related.getProductId().equals(cr.getString(cr.getColumnIndex(ID_COL)))) {
                    holder.imgLike.setImageResource(R.drawable.ic_is_like);
                    holder.isLiked = true;
                }

            } while (cr.moveToNext());

        }
        cr.close();
        db.close();

        holder.imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animZoomIn = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
                holder.imgLike.startAnimation(animZoomIn);
                if (!holder.isLiked) {
                    holder.imgLike.setImageResource(R.drawable.ic_is_like);
                    Toast.makeText(context, context.getResources().getString(R.string.added_to_your_wish_list), Toast.LENGTH_LONG).show();
                    //WishListWebService.addToWishList(sharedPrefManager.getUserData().getCustomerId(), offer.getProductId());

                    DataBase db = new DataBase(context);
                    String price;
                    if (related.getSpecial().equals("0")){
                        price=related.getPrice();
                    }else {
                        price = related.getSpecial();
                    }
                    db.insertIntoMyWishList(db
                            , related.getProductId()
                            , related.getName()
                            , price
                            , related.getThumb());
                    db.close();
                } else {
                    holder.imgLike.setImageResource(R.drawable.ic_like);
                    //WishListWebService.removeFromWishList(sharedPrefManager.getUserData().getCustomerId(), offer.getProductId());
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.removed_from_wish_list), Toast.LENGTH_LONG).show();
                    DataBase db = new DataBase(context);
                    db.deleteSingleRow(db, DataBase.MY_WISH_LIST_TABLE, related.getProductId());
                    db.close();

                    holder.isLiked = !holder.isLiked;
                }
//                HomeActivity.updateWishList();
                notifyDataSetChanged();

            }
        });
//        if (sharedPrefManager.getLoginStatus()) {
//            WishListWebService.wishList(sharedPrefManager.getUserData().getCustomerId());
//            if (sharedPrefManager.getWishList() != null) {
//                for (int i = 0; i < sharedPrefManager.getWishList().getWishlist().size(); i++) {
//                    if (sharedPrefManager.getWishList().getWishlist().get(i).getProductId().equals(related.getProductId())) {
//                        holder.imgLike.setImageResource(R.drawable.ic_is_like);
//                        isLiked = true;
//                    }
//                }
//            }
//        }
//        final boolean finalIsLiked = isLiked;
//        holder.imgLike.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (sharedPrefManager.getLoginStatus()) {
//                    Animation animZoomIn = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
//                    holder.imgLike.startAnimation(animZoomIn);
//                    if (!finalIsLiked) {
//                        holder.imgLike.setImageResource(R.drawable.ic_is_like);
//                        Toast.makeText(context, context.getResources().getString(R.string.added_to_your_wish_list), Toast.LENGTH_LONG).show();
//                        WishListWebService.addToWishList(sharedPrefManager.getUserData().getCustomerId(), related.getProductId());
//
//                    } else {
//                        holder.imgLike.setImageResource(R.drawable.ic_like);
//                        WishListWebService.removeFromWishList(sharedPrefManager.getUserData().getCustomerId(), related.getProductId());
//                        Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.removed_from_wish_list), Toast.LENGTH_LONG).show();
//                    }
//
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return relatedproducts.size();
    }


    class listItemHolder extends RecyclerView.ViewHolder {
        private ImageView imgOne;
        private ImageView imgLike;
        private TextView tvItemName;
        private TextView tvDeprecatedPrice;
        private TextView tvNewPrice;
        boolean isLiked = false;

        public listItemHolder(View itemView) {
            super(itemView);
            imgOne = (ImageView) itemView.findViewById(R.id.img_one);
            imgLike = (ImageView) itemView.findViewById(R.id.img_like);
            tvItemName = (TextView) itemView.findViewById(R.id.tv_item_name);
            tvDeprecatedPrice = (TextView) itemView.findViewById(R.id.tv_deprecated_price);
            tvNewPrice = (TextView) itemView.findViewById(R.id.tv_new_price);
        }
    }

}
