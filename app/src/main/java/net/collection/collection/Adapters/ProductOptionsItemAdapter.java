package net.collection.collection.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Models.OptionList;
import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ProductOptionsItemAdapter extends RecyclerView.Adapter<ProductOptionsItemAdapter.listItemHolder> {

    Context context;
    List<ProductOptionsResponseModel.ProductOptionValue> productOptionValue;
    String parentOption;
    public int mSelectedItem = -1;
    // public static ArrayList<String> productOptions = new ArrayList<String>();
    public static List<OptionList> productOptions = new ArrayList<>();
    String productOptionId;
    String optionId;
    String value;
    String type;

    public ProductOptionsItemAdapter(Context context, List<ProductOptionsResponseModel.ProductOptionValue> productOptionValue, String parentOption,
                                     String productOptionId, String optionId,
                                     String value, String type) {
        this.context = context;
        this.productOptionValue = productOptionValue;
        this.parentOption = parentOption;
        this.productOptionId = productOptionId;
        this.optionId = optionId;
        this.value = value;
        this.type = type;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_option_item_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final ProductOptionsResponseModel.ProductOptionValue optionValue = productOptionValue.get(position);
        holder.tvItemRow.setChecked(position == mSelectedItem);

        holder.tvItemRow.setText(optionValue.getName() + "(" + optionValue.getPrice() + ")");
        holder.childOption = holder.tvItemRow.getText().toString();
        holder.product_option_value_id = optionValue.getProductOptionValueId();
        holder.option_value_id = optionValue.getOptionValueId();
        holder.name = parentOption + " : "+optionValue.getName();
    }

    @Override
    public int getItemCount() {
        return productOptionValue.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private RadioButton tvItemRow;
        String childOption;
        String title;
        String product_option_value_id;
        String option_value_id;
        String name;
        OptionList optionList;

        public listItemHolder(View itemView) {
            super(itemView);
            tvItemRow = (RadioButton) itemView.findViewById(R.id.tv_item_row);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyItemRangeChanged(0, productOptionValue.size());
                    if (!productOptions.isEmpty()) {
                        for (int i = productOptions.size() - 1; i >= 0; i--) {
                            title = productOptions.get(i).getName();
                            if (title.startsWith(parentOption)) {
                                productOptions.remove(productOptions.get(i));
                            }
                        }
                        optionList = new OptionList(productOptionId, product_option_value_id, optionId, option_value_id, name, value, type);
                        productOptions.add(optionList);
                    } else {
                        optionList = new OptionList(productOptionId, product_option_value_id, optionId, option_value_id, name, value, type);
                        productOptions.add(optionList);
                    }
                }

            };
            itemView.setOnClickListener(clickListener);
            tvItemRow.setOnClickListener(clickListener);
        }
    }

}
