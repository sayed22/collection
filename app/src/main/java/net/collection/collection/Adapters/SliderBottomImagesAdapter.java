package net.collection.collection.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.ImagePreviewActivity;
import net.collection.collection.Networks.ResponseModels.SliderBottomBannarResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;

public class SliderBottomImagesAdapter extends RecyclerView.Adapter<SliderBottomImagesAdapter.listItemHolder> {

    Context context;
    List<SliderBottomBannarResponseModel.Banners> sliderModels;


    public SliderBottomImagesAdapter(Context context, List<SliderBottomBannarResponseModel.Banners> sliderModels) {
        this.context = context;
        this.sliderModels = sliderModels;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_fragment_slider_bottom_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final SliderBottomBannarResponseModel.Banners sliderModel = sliderModels.get(position);

        String url = Constants.IMAGE_URL + sliderModel.getImage();
        url = url.replaceAll(" ", "%20");
        System.out.println("cat image : " + url);

        Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgItem);
        final String finalUrl = url;
        holder.imgItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ImagePreviewActivity.class);

//// Pass data object in the bundle and populate details activity.
//                intent.putExtra("photo", finalUrl);
//                ActivityOptionsCompat options = ActivityOptionsCompat.
//                        makeSceneTransitionAnimation((Activity) context, (View) holder.imgItem, "profile");
//                context.startActivity(intent, options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return sliderModels.size();
    }


    class listItemHolder extends RecyclerView.ViewHolder {

        private ImageView imgItem;


        public listItemHolder(View itemView) {
            super(itemView);
            imgItem = (ImageView) itemView.findViewById(R.id.img_item_images);

        }
    }

}
