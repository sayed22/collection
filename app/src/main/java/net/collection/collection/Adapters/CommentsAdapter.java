package net.collection.collection.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;
import net.collection.collection.R;

import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.listItemHolder> {

    Context context;
    List<ImagesCommentsDiscountDataResponseModel.Reviewsdata> commentsList;



    public CommentsAdapter(Context context, List<ImagesCommentsDiscountDataResponseModel.Reviewsdata> commentsList) {
        this.context = context;
        this.commentsList = commentsList;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final ImagesCommentsDiscountDataResponseModel.Reviewsdata comment = commentsList.get(position);

        holder.tvUserName.setText(comment.getAuthor());
        holder.tvDate.setText(comment.getDateAdded());
        holder.tvContent.setText(comment.getText());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }


    class listItemHolder extends RecyclerView.ViewHolder {

        private TextView tvUserName;
        private TextView tvDate;
        private TextView tvContent;

        public listItemHolder(View itemView) {
            super(itemView);
            tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvContent = (TextView) itemView.findViewById(R.id.tv_content);
        }
    }

}
