package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.collection.collection.Activities.AdressesBookActivity;
import net.collection.collection.Activities.EditAddressActivity;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Networks.ResponseModels.GetAddressesResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;

public class GetAddressesAdapter extends RecyclerView.Adapter<GetAddressesAdapter.listItemHolder> {

    Context context;
    List<GetAddressesResponseModel.Addressesdata> addressesdata;
    IRecyclerItemClicked mRecyclerItemClickListener;


    public GetAddressesAdapter(Context context, List<GetAddressesResponseModel.Addressesdata> addressesdata, IRecyclerItemClicked mRecyclerItemClickListener) {
        this.context = context;
        this.addressesdata = addressesdata;
        this.mRecyclerItemClickListener = mRecyclerItemClickListener;

    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.addresses_menu_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final GetAddressesResponseModel.Addressesdata address = addressesdata.get(position);

        holder.tvAddressMenuItem.setText(address.getFirstname() + " " + address.getLastname() + "\n"
                + address.getCompany() + "\n"
                + address.getAddress1() + "\n"
                + address.getAddress2() + "\n"
                + address.getCity() + "\n"
                + address.getPostcode() + "\n"
                + address.getCountry() + "\n"
                + address.getZone());

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.itemClicked = 3;
                Constants.addressId = String.valueOf(address.getAddressId());
                mRecyclerItemClickListener.onRecyclerItemClicked(position);
            }
        });
        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EditAddressActivity.class);
                intent.putExtra("address_id", String.valueOf(address.getAddressId()));
                intent.putExtra("first_name", address.getFirstname());
                intent.putExtra("last_name", address.getLastname());
                intent.putExtra("company", address.getCompany());
                intent.putExtra("first_address", address.getAddress1());
                intent.putExtra("second_address", address.getAddress2());
                intent.putExtra("city", address.getCity());
                intent.putExtra("post_code", address.getPostcode());
                intent.putExtra("zone_name", address.getZone());
                intent.putExtra("country_name", address.getCountry());
                //intent.putExtra("set_default",address.());
                context.startActivity(intent);
                Constants.countryId = address.getCountryId();
                Constants.zoneId = address.getZoneId();

            }
        });


    }

    @Override
    public int getItemCount() {
        return addressesdata.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private TextView tvAddressMenuItem;
        private ImageView btnDelete;
        private ImageView btnEdit;

        public listItemHolder(View itemView) {
            super(itemView);
            tvAddressMenuItem = (TextView) itemView.findViewById(R.id.tv_address_menu_item);
            btnDelete = (ImageView) itemView.findViewById(R.id.btn_delete);
            btnEdit = (ImageView) itemView.findViewById(R.id.btn_edit);

        }
    }

}
