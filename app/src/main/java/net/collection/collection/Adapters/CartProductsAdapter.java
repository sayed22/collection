package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Fragments.CartFragment;
import net.collection.collection.Models.MyCartModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.ArrayList;

public class CartProductsAdapter extends RecyclerView.Adapter<CartProductsAdapter.listItemHolder> {

    Context context;
    ArrayList<MyCartModel> cartCartMoudels;

    public CartProductsAdapter(Context context, ArrayList<MyCartModel> cartCartMoudels) {
        this.context = context;
        this.cartCartMoudels = cartCartMoudels;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final MyCartModel cartMoudel = cartCartMoudels.get(position);

        String url = Constants.IMAGE_URL + cartMoudel.getImage();
        url = url.replaceAll(" ","%20");
        System.out.println("cat image : " + url);

        Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgProduct);
        holder.tvProductName.setText(String.valueOf(cartMoudel.getName()));
        System.out.println("price : " + cartMoudel.getPrice() + "quantity : " + cartMoudel.getQuantity());
        System.out.println("total : " + cartMoudel.getTotal() );
        holder.tvItemPrice.setText(String.valueOf(cartMoudel.getPrice()));
        holder.etCount.setText(String.valueOf(cartMoudel.getQuantity()));
        holder.tvTotalPrice.setText(cartMoudel.getTotal());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductDataActivity.class);
                intent.putExtra("produact_id", cartMoudel.getId());
                intent.putExtra("produact_name", cartMoudel.getName());
                context.startActivity(intent);
            }
        });
        holder.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.count = Integer.parseInt(holder.etCount.getText().toString());
                holder.count++;
                holder.etCount.setText(String.valueOf(holder.count));
                DataBase db = new DataBase(context);
                db.updateSellInTable(db,DataBase.MY_CART_TABLE,cartMoudel.getId(),DataBase.QUANTITY_COL,holder.etCount.getText().toString());
                db.close();
                String price = cartMoudel.getPrice();
                price = price.replaceAll("KD", "");

                //price = price.replaceAll("[^\\d-]", "");
                double total = Double.parseDouble(price) * Double.parseDouble(holder.etCount.getText().toString());
                DataBase db2 = new DataBase(context);
                db2.updateSellInTable(db2,DataBase.MY_CART_TABLE,cartMoudel.getId(),DataBase.TOTAL_COL,String.valueOf(total));
                db2.close();
                holder.tvTotalPrice.setText(String.valueOf(total));
                CartFragment.getCartTotal();
            }
        });
        holder.tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.count = Integer.parseInt(holder.etCount.getText().toString());
                if (holder.count > 1) {
                    holder.count--;
                } else {
                    holder.count = 1;
                }
                holder.etCount.setText(String.valueOf(holder.count));
                DataBase db = new DataBase(context);
                db.updateSellInTable(db,DataBase.MY_CART_TABLE,cartMoudel.getId(),DataBase.QUANTITY_COL,holder.etCount.getText().toString());
                db.close();

                String price = cartMoudel.getPrice();
                price = price.replaceAll("KD", "");

                // price = price.replaceAll("[^\\d-]", "");
                double total = Double.parseDouble(price) * Double.parseDouble(holder.etCount.getText().toString());
                DataBase db2 = new DataBase(context);
                db2.updateSellInTable(db2,DataBase.MY_CART_TABLE,cartMoudel.getId(),DataBase.TOTAL_COL,String.valueOf(total));
                db2.close();
                holder.tvTotalPrice.setText(String.valueOf(total));
                CartFragment.getCartTotal();
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataBase db = new DataBase(context);
                db.deleteSingleRow(db, DataBase.MY_CART_TABLE, cartMoudel.getId());
                db.close();
                Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.removed_from_cart), Toast.LENGTH_LONG).show();
                cartCartMoudels.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, cartCartMoudels.size());
                CartFragment.getCartTotal();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartCartMoudels.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {
        private ImageView imgProduct;
        private TextView tvProductName;
        private TextView tvItemPrice;
        private LinearLayout linQuantity;
        private TextView tvPlus;
        private TextView etCount;
        private TextView tvMinus;
        private TextView tvTotalPrice;
        private int count;
        private Button btnDelete;

        public listItemHolder(View itemView) {
            super(itemView);
            imgProduct = (ImageView) itemView.findViewById(R.id.img_product);
            btnDelete = (Button) itemView.findViewById(R.id.btn_delete);
            tvProductName = (TextView) itemView.findViewById(R.id.tv_product_name);
            tvItemPrice = (TextView) itemView.findViewById(R.id.tv_item_price);
            linQuantity = (LinearLayout) itemView.findViewById(R.id.lin_quantity);
            tvPlus = (TextView) itemView.findViewById(R.id.tv_plus);
            etCount = (TextView) itemView.findViewById(R.id.et_count);
            tvMinus = (TextView) itemView.findViewById(R.id.tv_minus);
            tvTotalPrice = (TextView) itemView.findViewById(R.id.tv_total_price);
        }
    }

}
