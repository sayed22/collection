package net.collection.collection.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.ImagePreviewActivity;
import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.listItemHolder> {

    Context context;
    List<ImagesCommentsDiscountDataResponseModel.AddImgs> imgsList;


    public ImagesAdapter(Context context, List<ImagesCommentsDiscountDataResponseModel.AddImgs> imgsList) {
        this.context = context;
        this.imgsList = imgsList;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.images_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final ImagesCommentsDiscountDataResponseModel.AddImgs image = imgsList.get(position);

        String url = Constants.IMAGE_URL + image.getImage();
        url = url.replaceAll(" ","%20");
        System.out.println("cat image : " + url);

        Picasso.with(context).load(url).placeholder(context.getResources().getDrawable(R.drawable.placeholder)).into(holder.imgItem);
        final String finalUrl = url;
        holder.imgItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ImagePreviewActivity.class);

// Pass data object in the bundle and populate details activity.
                intent.putExtra("photo", finalUrl);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation((Activity) context, (View) holder.imgItem, "profile");
                context.startActivity(intent, options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return imgsList.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private ImageView imgItem;


        public listItemHolder(View itemView) {
            super(itemView);
            imgItem = (ImageView) itemView.findViewById(R.id.img_item);

        }
    }

}
