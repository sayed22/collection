package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.collection.collection.Activities.OneOrderDetailsActivity;
import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Networks.ResponseModels.ReturnedProductsListResponseModel;
import net.collection.collection.R;

import java.util.List;

public class ReturnedProductsListAdapter extends RecyclerView.Adapter<ReturnedProductsListAdapter.listItemHolder> {

    Context context;
    List<ReturnedProductsListResponseModel.Results> returnedProducts;

    public ReturnedProductsListAdapter(Context context, List<ReturnedProductsListResponseModel.Results> returnedProducts) {
        this.context = context;
        this.returnedProducts = returnedProducts;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.returned_products_list_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final ReturnedProductsListResponseModel.Results returnedProduct = returnedProducts.get(position);
        holder.tvReturnedNumber.setText(returnedProduct.getReturnId());
        holder.tvReturnedStatus.setText(returnedProduct.getStatus());
        holder.tvStartDate.setText(returnedProduct.getDateAdded());
        holder.tvOrderNo.setText(returnedProduct.getOrderId());
        holder.tvClient.setText(returnedProduct.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OneOrderDetailsActivity.class);
                intent.putExtra("order_id", returnedProduct.getOrderId());
                intent.putExtra("order_status", returnedProduct.getStatus());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return returnedProducts.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {
        private TextView tvReturnedNumber;
        private TextView tvReturnedStatus;
        private TextView tvStartDate;
        private TextView tvOrderNo;
        private TextView tvClient;
        private Button btnReview;

        public listItemHolder(View itemView) {
            super(itemView);
            tvReturnedNumber = (TextView) itemView.findViewById(R.id.tv_returned_number);
            tvReturnedStatus = (TextView) itemView.findViewById(R.id.tv_returned_status);
            tvStartDate = (TextView) itemView.findViewById(R.id.tv_start_date);
            tvOrderNo = (TextView) itemView.findViewById(R.id.tv_order_no);
            tvClient = (TextView) itemView.findViewById(R.id.tv_client);
            btnReview = (Button) itemView.findViewById(R.id.btn_review);
        }
    }

}
