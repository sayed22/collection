package net.collection.collection.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.collection.collection.Activities.AdressesBookActivity;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Networks.ResponseModels.CountriesResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import java.util.List;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.listItemHolder> {

    Context context;
    List<CountriesResponseModel.Countries> countries;
    IRecyclerItemClicked mRecyclerItemClickListener;


    public CountriesAdapter(Context context, List<CountriesResponseModel.Countries> countries, IRecyclerItemClicked mRecyclerItemClickListener) {
        this.context = context;
        this.countries = countries;
        this.mRecyclerItemClickListener = mRecyclerItemClickListener;

    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.countries_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, final int position) {
        final CountriesResponseModel.Countries country = countries.get(position);

        holder.tvAddressMenuItem.setText(country.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.itemClicked = 1;
                Constants.countryId = country.getCountryId();
                Constants.countryName = country.getName();
                mRecyclerItemClickListener.onRecyclerItemClicked(position);


            }
        });
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {

        private TextView tvAddressMenuItem;

        public listItemHolder(View itemView) {
            super(itemView);
            tvAddressMenuItem = (TextView) itemView.findViewById(R.id.tv_address_menu_item);

        }
    }

}
