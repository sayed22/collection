package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Fragments.HomeFragment;
import net.collection.collection.Networks.ResponseModels.SliderResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.Utils;

import java.util.List;


public class SliderAdapter extends PagerAdapter {
    Context context;
    //    List<SliderResponseModel.Data> imagesListModels;
//    SliderResponseModel.Data imagesModel;
    List<SliderResponseModel.Banners> sliderModels;
    SliderResponseModel.Banners sliderModel;
    LayoutInflater layoutInflater;


    public SliderAdapter(Context context, List<SliderResponseModel.Banners> sliderModels) {
        this.context = context;
        this.sliderModels = sliderModels;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return sliderModels.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.home_fragment_slider_row, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_item_images);
        ImageView img_cancel = (ImageView) itemView.findViewById(R.id.img_cancel);
        sliderModel = sliderModels.get(position);

        String url = Constants.IMAGE_URL + sliderModel.getImage();
        url = url.replaceAll(" ", "%20");
        System.out.println("cat image : " + url);

        Picasso.with(context).load(url).into(imageView);
        container.addView(itemView);

        //listening to image click
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openUrl(context, "http://"+sliderModel.getLink());
            }
        });

        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeFragment.frmHomeImagesSlider.setVisibility(View.GONE);
            }
        });
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }

}
