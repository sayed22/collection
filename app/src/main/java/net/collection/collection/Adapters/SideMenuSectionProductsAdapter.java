package net.collection.collection.Adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.HomeActivity;
import net.collection.collection.Activities.ProductDataActivity;
import net.collection.collection.Activities.SectionProductsActivity;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Networks.ResponseModels.SectionProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionsResponseModel;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.SharedPrefLanguage;

import java.util.List;

import static net.collection.collection.Database.DataBase.ID_COL;

public class SideMenuSectionProductsAdapter extends RecyclerView.Adapter<SideMenuSectionProductsAdapter.listItemHolder> {

    Context context;
    List<SectionsResponseModel.CategoryInfo> sectionsModels;
    public SideMenuSectionProductsAdapter(Context context,  List<SectionsResponseModel.CategoryInfo> sectionsModels) {
        this.context = context;
        this.sectionsModels = sectionsModels;
    }

    @Override
    public listItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.side_menu_section_products_row, parent, false);
        listItemHolder holder = new listItemHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(final listItemHolder holder, int position) {
        final SectionsResponseModel.CategoryInfo sectionsModel = sectionsModels.get(position);
        holder.name = sectionsModel.getName();
        holder.name = holder.name.replaceAll("&amp;"," & ");
        holder.tvItemName.setText(holder.name);

        SharedPrefLanguage sharedPrefLanguage = new SharedPrefLanguage(context.getApplicationContext());

        System.out.println("language : " + sharedPrefLanguage.LoadData());
        if (sharedPrefLanguage.LoadData().equals("ar")){
            holder.tvItemName.setGravity(Gravity.CENTER_VERTICAL | Gravity.END);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SectionProductsActivity.class);
                intent.putExtra("produact_id", sectionsModel.getCategoryId());
                intent.putExtra("produact_name", sectionsModel.getName());
                context.startActivity(intent);
                HomeActivity.homeActivity.onBackPressed();
            }
        });
    }

    @Override
    public int getItemCount() {
        return sectionsModels.size();
    }

    class listItemHolder extends RecyclerView.ViewHolder {
        private TextView tvItemName;
        String name;

        public listItemHolder(View itemView) {
            super(itemView);
            tvItemName = (TextView) itemView.findViewById(R.id.tv_item_name);
        }
    }

}
