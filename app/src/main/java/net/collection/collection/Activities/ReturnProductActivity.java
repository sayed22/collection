package net.collection.collection.Activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.GetReturnReasonsAdapter;
import net.collection.collection.Adapters.ProductOptionsAdapter;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Models.ReturnProductModel;
import net.collection.collection.Networks.Events.GetReturnReasonEvent;
import net.collection.collection.Networks.Events.ProductOptionsEvent;
import net.collection.collection.Networks.Events.ReturnProductEvent;
import net.collection.collection.Networks.ResponseModels.GetReturnReasonsResponseModel;
import net.collection.collection.Networks.WebServices.OrdersWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.SharedPrefManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReturnProductActivity extends ParentActivity {

    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etEmail;
    private EditText etPhoneNumber;
    private EditText etOrderNo;
    private TextView tvOrderDate;
    private ImageView btnCalendar;
    private EditText etProductName;
    private EditText etProductType;
    private EditText etQuantity;
    private RecyclerView returnReasonRecyclerVieww;
    private RadioButton rdbtnNo;
    private RadioButton rdbtnYes;
    private EditText etAnotherReasons;
    private List<GetReturnReasonsResponseModel.Returnreasons> returnreasonsList;
    private android.widget.Button btnSend;
    String order_id, productName,productId, model, quantity;
    public Calendar myCalendar;
    public int myear, mmonth, mday, mhour, mminute;
    String isOpend = "0";
    CustomLoadingDialog customLoadingDialog;
    private TextView return_reason;

    @Override
    protected void initializeComponents() {
        initView();
        customLoadingDialog = new CustomLoadingDialog(ReturnProductActivity.this, R.style.DialogSlideAnim);

        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        myCalendar = Calendar.getInstance();
        myear = myCalendar.get(Calendar.YEAR);
        mmonth = myCalendar.get(Calendar.MONTH);
        mday = myCalendar.get(Calendar.DAY_OF_MONTH);
        mhour = myCalendar.get(Calendar.HOUR_OF_DAY);
        mminute = myCalendar.get(Calendar.MINUTE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvToolbarTitle.setText(getResources().getString(R.string.returned_products));

        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        if (sharedPrefManager.getLoginStatus()) {
            order_id = getIntent().getStringExtra("order_id");
            productName = getIntent().getStringExtra("productName");
            productId = getIntent().getStringExtra("product_id");
            model = getIntent().getStringExtra("model");
            quantity = getIntent().getStringExtra("quantity");

            etFirstName.setText(sharedPrefManager.getUserData().getFirstname());
            etLastName.setText(sharedPrefManager.getUserData().getLastname());
            etEmail.setText(sharedPrefManager.getUserData().getEmail());
            etPhoneNumber.setText(sharedPrefManager.getUserData().getTelephone());
            etOrderNo.setText(order_id);
            tvOrderDate.setText(Constants.orderDate);
            etProductName.setText(productName);
            etProductType.setText(model);
            etQuantity.setText(quantity);
        }

        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ReturnProductActivity.this, fromDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
                    etFirstName.setError(getResources().getString(R.string.enter_first_name));
                } else if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
                    etLastName.setError(getResources().getString(R.string.enter_last_name));
                } else if (TextUtils.isEmpty(etPhoneNumber.getText().toString().trim())) {
                    etPhoneNumber.setError(getResources().getString(R.string.enter_phone_number));
                } else if (etPhoneNumber.length() < 10) {
                    etPhoneNumber.setError(getResources().getString(R.string.wrong_phone_number));
                } else if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    etEmail.setError(getResources().getString(R.string.enter_email));
                } else if (!emailValidator(etEmail.getText().toString())) {
                    etEmail.setError(getResources().getString(R.string.wrong_email));
                } else if (TextUtils.isEmpty(etOrderNo.getText().toString().trim())) {
                    etOrderNo.setError(getResources().getString(R.string.enter_order_no));
                } else if (TextUtils.isEmpty(tvOrderDate.getText().toString().trim())) {
                    tvOrderDate.setError(getResources().getString(R.string.enter_order_date));
                } else if (TextUtils.isEmpty(etProductName.getText().toString().trim())) {
                    etProductName.setError(getResources().getString(R.string.enter_product_name));
                } else if (TextUtils.isEmpty(etProductType.getText().toString().trim())) {
                    etProductType.setError(getResources().getString(R.string.enter_product_type));
                } else if (TextUtils.isEmpty(etQuantity.getText().toString().trim())) {
                    etQuantity.setError(getResources().getString(R.string.enter_quantity));
                } else if (TextUtils.isEmpty(etQuantity.getText().toString().trim())) {
                    etQuantity.setError(getResources().getString(R.string.enter_quantity));
                } else if (TextUtils.isEmpty(Constants.returnReasonId)) {
                    return_reason.setError(getResources().getString(R.string.choose_return_reason));
                } else {
                    customLoadingDialog.show();
                        OrdersWebService.returnProduct(etFirstName.getText().toString(),etLastName.getText().toString(),etEmail.getText().toString(),
                                etPhoneNumber.getText().toString(),etOrderNo.getText().toString(),tvOrderDate.getText().toString(),productId,
                                etProductType.getText().toString(),etQuantity.getText().toString(),Constants.returnReasonId,isOpend,
                                etAnotherReasons.getText().toString(),String.valueOf(sharedPrefManager.getUserData().getCustomerId()));

//                    ReturnProductModel return_data = new ReturnProductModel();
//                    return_data.setFirstName(etFirstName.getText().toString());
//                    return_data.setLastname(etLastName.getText().toString());
//                    return_data.setEmail(etEmail.getText().toString());
//                    return_data.setTelephone(etPhoneNumber.getText().toString());
//                    return_data.setOrder_id(etOrderNo.getText().toString());
//                    return_data.setDate_ordered(tvOrderDate.getText().toString());
//                    return_data.setProduct(etProductName.getText().toString());
//                    return_data.setModel(etProductType.getText().toString());
//                    return_data.setQuantity(etQuantity.getText().toString());
//                    return_data.setReturn_reason_id(Constants.returnReasonId);
//                    return_data.setOpened(isOpend);
//                    return_data.setComment(etAnotherReasons.getText().toString());
//
//                    //OrdersWebService.returnProduct(return_data);
//                    customLoadingDialog.show();
//
//                    Gson gson2 = new Gson();
//                    try {
//                        String json2 = gson2.toJson(return_data);
//                        System.out.println("return json : "+json2);
//                        JSONObject mainObject = new JSONObject(json2);
//                        OrdersWebService.returnProduct(mainObject);
//
////                    String json2 = gson2.toJson(addOrderModuleBody);
////                    JSONArray jsonArr = new JSONArray(json2);
////                    OrdersWebService.addOrder(jsonArr);
////                    System.out.println("Json object : " + jsonArr);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                }
            }
        });
        rdbtnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isOpend = "0";
            }
        });
        rdbtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isOpend = "1";
            }
        });
        OrdersWebService.getRturnReason();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_return_product;
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    //============================ for date picker ===============================================
    DatePickerDialog.OnDateSetListener fromDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateFromDate();
        }
    };

    private void updateFromDate() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tvOrderDate.setText(sdf.format(myCalendar.getTime()));
    }

    //======================================================================================================
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GetReturnReasonEvent getReturnReasonEvent) {
        if (getReturnReasonEvent.getGetReturnReasonsResponseModel().getSuccess()) {
            returnreasonsList = getReturnReasonEvent.getGetReturnReasonsResponseModel().getReturnreasons();
            GetReturnReasonsAdapter adapter = new GetReturnReasonsAdapter(ReturnProductActivity.this, returnreasonsList);
            returnReasonRecyclerVieww.setLayoutManager(new LinearLayoutManager(ReturnProductActivity.this));
            returnReasonRecyclerVieww.setNestedScrollingEnabled(false);
            returnReasonRecyclerVieww.setAdapter(adapter);
        } else {
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ReturnProductEvent returnProductEvent) {
        if (returnProductEvent.getReturnProductResponseModel().getSuccess()) {
            customLoadingDialog.cancel();
            Toast.makeText(ReturnProductActivity.this,getResources().getString(R.string.product_has_been_returned),Toast.LENGTH_LONG).show();
            finish();
        } else {
            customLoadingDialog.cancel();
        }
    }

    private void initView() {
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etEmail = (EditText) findViewById(R.id.et_email);
        etPhoneNumber = (EditText) findViewById(R.id.et_phone_number);
        etOrderNo = (EditText) findViewById(R.id.et_order_no);
        tvOrderDate = (TextView) findViewById(R.id.tv_order_date);
        btnCalendar = (ImageView) findViewById(R.id.btn_calendar);
        etProductName = (EditText) findViewById(R.id.et_product_name);
        etProductType = (EditText) findViewById(R.id.et_product_type);
        etQuantity = (EditText) findViewById(R.id.et_quantity);
        return_reason = (TextView) findViewById(R.id.return_reason);
        returnReasonRecyclerVieww = (RecyclerView) findViewById(R.id.return_reason_recycler_vieww);
        rdbtnNo = (RadioButton) findViewById(R.id.rdbtn_no);
        rdbtnYes = (RadioButton) findViewById(R.id.rdbtn_yes);
        etAnotherReasons = (EditText) findViewById(R.id.et_another_reasons);
        btnSend = (Button) findViewById(R.id.btn_send);
    }
}
