package net.collection.collection.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Models.Userdata;
import net.collection.collection.Networks.Events.LogInEvent;
import net.collection.collection.Networks.Events.RegisterEvent;
import net.collection.collection.Networks.WebServices.UserWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.ViewsUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogInActivity extends ParentActivity {

    private EditText etEmail;
    private Button btnLogInByPhone;
    private EditText etPassword;
    private Button btnLogIn;
    private Button btnForgotPassword;
    private Button btnNewUser;
    @Override
    protected void initializeComponents() {
        initView();
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LogInActivity.this, ForgotPasswordActivity.class);

                Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                startActivity(intent, bndlanimation);
            }
        });
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    etEmail.setError(getResources().getString(R.string.enter_email));
                } else if (!emailValidator(etEmail.getText().toString())) {
                    etEmail.setError(getResources().getString(R.string.wrong_email));
                } else if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                    etPassword.setError(getResources().getString(R.string.enter_password));
                }else {
                    UserWebService.logInUser(etEmail.getText().toString(),etPassword.getText().toString());
                    customLoadingDialog.show();
                }
            }
        });
        btnNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LogInActivity.this, RegisterActivity.class);

                Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                startActivity(intent, bndlanimation);
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_log_in;
    }


    private void initView() {
        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_password);
        btnLogIn = (Button) findViewById(R.id.btn_log_in);
        btnForgotPassword = (Button) findViewById(R.id.btn_forgot_password);
        btnNewUser = (Button) findViewById(R.id.btn_new_user);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        LogInActivity.this.overridePendingTransition(R.anim.animation, R.anim.animation2);
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLogIn(LogInEvent logInEvent) {
        if (logInEvent.getLogInResponseModel().getSuccess()) {
            Userdata userdata = logInEvent.getLogInResponseModel().getUserdata();
            sharedPrefManager.setUserData(userdata);
            sharedPrefManager.setLoginStatus(true);
           // Intent intent = new Intent(LogInActivity.this, HomeActivity.class);
           // Bundle bndlanimation =
              //      ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
           // startActivity(intent, bndlanimation);
            finish();
            customLoadingDialog.cancel();

        } else {
            ViewsUtils.showSnackBarWithLong(context, etEmail, getString(R.string.wrong_email_or_password), R.color.white);
            customLoadingDialog.dismiss();
        }
    }
}
