package net.collection.collection.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.R;
import net.collection.collection.Utils.LocaleManager;

/*created at 24/09/2018  11:45 AM Mohamed Ghoneim*/
public class SplashActivity extends ParentActivity {

    private Thread splashTread;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    protected void initializeComponents() {

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 2000) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);

                    Bundle bndlanimation =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                    startActivity(intent, bndlanimation);

//                    boolean isLogged = sharedPrefManager.getLoginStatus();
//                    if (isLogged) {
//                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
//
//                        Bundle bndlanimation =
//                                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
//                        startActivity(intent, bndlanimation);
//
//                    } else {
//                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
//                        Bundle bndlanimation =
//                                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
//                        startActivity(intent, bndlanimation);
//                    }
                    finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    finish();
                }
            }
        };
        splashTread.start();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }
}
