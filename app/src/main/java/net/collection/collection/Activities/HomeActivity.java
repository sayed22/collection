package net.collection.collection.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.processphoenix.ProcessPhoenix;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.SearchAutoCompleteAdapter;
import net.collection.collection.Adapters.SectionsAdapter;
import net.collection.collection.Adapters.SideMenuSectionProductsAdapter;
import net.collection.collection.Fragments.WishListFragment;
import net.collection.collection.Fragments.HomeFragment;
import net.collection.collection.Fragments.ProfileFragment;
import net.collection.collection.Fragments.CartFragment;
import net.collection.collection.Models.Userdata;
import net.collection.collection.Networks.Events.ProductsEvent;
import net.collection.collection.Networks.Events.SectionsEvent;
import net.collection.collection.Networks.ResponseModels.ProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionsResponseModel;
import net.collection.collection.Networks.WebServices.ProductsWebService;
import net.collection.collection.Networks.WebServices.WishListWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.DetectConnection;
import net.collection.collection.Utils.LocaleManager;
import net.collection.collection.Utils.SharedPrefLanguage;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends ParentActivity {

    private int[] tabIcons = {
            R.drawable.ic_home,
            //R.drawable.ic_calendar,
            R.drawable.ic_heart,
            R.drawable.ic_online_shopping_cart,
            R.drawable.ic_profile
    };
    private int[] tabLabels = {
            R.string.home_tab,
            //  R.string.best_offers,
            R.string.favorit_tab,
            R.string.shop_car_tab,
            R.string.my_profile
    };
    public static String TAG = "";
    private DrawerLayout drawerLayout;
    private android.support.design.widget.CollapsingToolbarLayout collapsingToolbar;
    private android.widget.ImageView btnBack;
    private android.widget.ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private android.widget.ImageView btnNotifications;
    private android.widget.ImageView btnMenu;
    private android.widget.ImageView btnSettings;
    private FrameLayout frameLayoutMain;
    private TabLayout homeTabLayout;
    private NavigationView navView;
    private ArrayList<ProductsResponseModel.Products> productList = new ArrayList<>();
    private android.widget.LinearLayout lyoutAppBar;
    boolean doubleBackToExitPressedOnce = false;
    private ImageView imgLogo;
    private TextView tvSections;
    private RecyclerView navRecyclerView;
    private TextView tvSettings;
    private TextView tvFaqs;
    private TextView tvCustomerService;
    private TextView tvContactUs;
    private TextView tvLogIn;
    private TextView tvLogOut;
    private List<SectionsResponseModel.CategoryInfo> sectionsList;
    private TextView tvSelectLanguage;
    private android.widget.RadioGroup radioGroup;
    private android.widget.RadioButton rdBtnEnglish;
    private android.widget.RadioButton rdBtnArabic;
    private String currentLang;
    boolean isLanguageChecked = true;
    public int upArrow, downArrow, ic_world;
    private TextView tvUserName;
    public static Activity homeActivity;
    private ImageView imgTitleBarLogo;

    @Override
    protected void initializeComponents() {
        initView();
        homeActivity = this;
        LocaleManager localeManager = new LocaleManager();
        localeManager.setLocale(this);
        upArrow = R.drawable.ic_up_arrow_key;
        downArrow = R.drawable.ic_expand_button;
        ic_world = R.drawable.ic_world_white;
        sharedPrefManager = new SharedPrefManager(HomeActivity.this);
        //imgTitleBarLogo.setVisibility(View.VISIBLE);
        tvSections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navRecyclerView.getVisibility() == View.VISIBLE) {
                    navRecyclerView.setVisibility(View.GONE);
                    tvSections.setCompoundDrawablesRelativeWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_menu), null, getResources().getDrawable(R.drawable.ic_expand_button), null);
                } else {
                    navRecyclerView.setVisibility(View.VISIBLE);
                    tvSections.setCompoundDrawablesRelativeWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_menu), null, getResources().getDrawable(R.drawable.ic_up_arrow_key), null);

                }
            }
        });
        navRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
                startActivity(intent);
                onBackPressed();
            }
        });
        tvFaqs.setVisibility(View.GONE);
        tvFaqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, PopularQuestionsActivity.class);
                startActivity(intent);
                onBackPressed();
            }
        });
        tvCustomerService.setVisibility(View.GONE);
        tvCustomerService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CustomerServiceActivity.class);
                startActivity(intent);
                onBackPressed();
            }
        });
        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ContactUsActivity.class);
                startActivity(intent);
                onBackPressed();
            }
        });
        tvLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, LogInActivity.class);
                startActivity(intent);
                onBackPressed();
            }
        });
        tvLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPrefManager.setLoginStatus(false);
                Userdata userdata = new Userdata();
                sharedPrefManager.setUserData(userdata);
                tvUserName.setText(getResources().getString(R.string.app_name));
                tvLogIn.setVisibility(View.VISIBLE);
                tvLogOut.setVisibility(View.GONE);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

//                Intent intent = new Intent(HomeActivity.this, LogInActivity.class);
//                startActivity(intent);
//                finish();
            }
        });

        currentLang = Locale.getDefault().getLanguage();
        if (currentLang.equals("en")) {
            rdBtnEnglish.setChecked(true);
        } else {
            rdBtnArabic.setChecked(true);
        }

        rdBtnArabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocaleManager localeManager = new LocaleManager();

                localeManager.setNewLocale(HomeActivity.this, Utils.ARABIC_LANGUAGE);

                String lang = Utils.ARABIC_LANGUAGE;
                SharedPrefLanguage.APP_LANGUAGE = lang;
                SharedPrefLanguage sv = new SharedPrefLanguage(HomeActivity.this);
                sv.SaveData();
                Intent nextIntent = new Intent(HomeActivity.this, HomeActivity.class);
                ProcessPhoenix.triggerRebirth(HomeActivity.this, nextIntent);
            }
        });
        rdBtnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocaleManager localeManager = new LocaleManager();
                localeManager.setNewLocale(HomeActivity.this, Utils.ENGLISH_LANGUAGE);
                String lang = Utils.ENGLISH_LANGUAGE;
                SharedPrefLanguage.APP_LANGUAGE = lang;
                SharedPrefLanguage sv = new SharedPrefLanguage(HomeActivity.this);
                sv.SaveData();
                Intent nextIntent = new Intent(HomeActivity.this, HomeActivity.class);
                ProcessPhoenix.triggerRebirth(HomeActivity.this, nextIntent);
            }
        });

        tvSelectLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLanguageChecked) {
                    radioGroup.setVisibility(View.VISIBLE);
                    tvSelectLanguage.setCompoundDrawablesRelativeWithIntrinsicBounds(ic_world, 0, upArrow, 0);
                } else {
                    radioGroup.setVisibility(View.GONE);
                    tvSelectLanguage.setCompoundDrawablesRelativeWithIntrinsicBounds(ic_world, 0, downArrow, 0);
                }
                isLanguageChecked = !isLanguageChecked;
            }
        });

        //====================================
        if (sharedPrefManager.getLoginStatus()) {
            tvUserName.setText(sharedPrefManager.getUserData().getFirstname() + " " + sharedPrefManager.getUserData().getLastname());
            tvLogIn.setVisibility(View.GONE);
            tvLogOut.setVisibility(View.VISIBLE);
        } else {
            tvUserName.setText(getResources().getString(R.string.app_name));
            tvLogIn.setVisibility(View.VISIBLE);
            tvLogOut.setVisibility(View.GONE);
        }
        for (int i = 0; i < 4; i++) {
            FrameLayout tab = (FrameLayout) LayoutInflater.from(this).inflate(R.layout.activity_home_nav_tab, null);
            TextView tab_label = (TextView) tab.findViewById(R.id.nav_label);
            tab_label.setCompoundDrawablesRelativeWithIntrinsicBounds(null, getResources().getDrawable(tabIcons[i]), null, null);
            tab_label.setText(getResources().getString(tabLabels[i]));
            //homeTabLayout.getTabAt(i).setCustomView(tab);
            if (i == 0) {
                tab_label.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
            homeTabLayout.addTab(homeTabLayout.newTab().setCustomView(tab));
        }
        homeTabLayout.setSelectedTabIndicatorHeight(0);
        homeTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View tabView = tab.getCustomView();
                TextView tab_label = (TextView) tabView.findViewById(R.id.nav_label);
                tab_label.setTextColor(getResources().getColor(R.color.colorPrimary));
                switch (tab.getPosition()) {
                    case 0:
                        HomeFragment home_fragment = new HomeFragment();
                        FragmentTransaction transaction0 = getSupportFragmentManager().beginTransaction();
                        transaction0.replace(R.id.frame_layout_main, home_fragment);
                        transaction0.commit();
                        autocompleteSearch.setVisibility(View.VISIBLE);
                        tvToolbarTitle.setVisibility(View.GONE);
                        break;

                    case 1:
                        WishListFragment favorit_fragment = new WishListFragment();
                        FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                        transaction2.replace(R.id.frame_layout_main, favorit_fragment);
                        transaction2.addToBackStack(null);
                        transaction2.commit();
                        autocompleteSearch.setVisibility(View.GONE);
                        tvToolbarTitle.setVisibility(View.VISIBLE);
                        tvToolbarTitle.setText(getResources().getString(R.string.favorit_tab));
                        break;
                    case 2:
                        CartFragment shop_car_fragment = new CartFragment();
                        FragmentTransaction transaction3 = getSupportFragmentManager().beginTransaction();
                        Bundle bundle3 = new Bundle();
                        transaction3.replace(R.id.frame_layout_main, shop_car_fragment);
                        transaction3.addToBackStack(null);
                        transaction3.commit();
                        autocompleteSearch.setVisibility(View.GONE);
                        tvToolbarTitle.setVisibility(View.VISIBLE);
                        tvToolbarTitle.setText(getResources().getString(R.string.shop_car_tab));
                        break;
                    case 3:
                        ProfileFragment profile_fragment = new ProfileFragment();
                        FragmentTransaction transaction4 = getSupportFragmentManager().beginTransaction();
                        transaction4.replace(R.id.frame_layout_main, profile_fragment);
                        transaction4.addToBackStack(null);
                        transaction4.commit();
                        autocompleteSearch.setVisibility(View.GONE);
                        tvToolbarTitle.setVisibility(View.VISIBLE);
                        tvToolbarTitle.setText(getResources().getString(R.string.my_profile));
                        break;
                    default:
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View tabView = tab.getCustomView();
                TextView tab_label = (TextView) tabView.findViewById(R.id.nav_label);
                tab_label.setTextColor(getResources().getColor(R.color.text_font_gray));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        HomeFragment fragment = new HomeFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
//        bundle.putString("fragment_key", "my_account");
//        fragment.setArguments(bundle);
        transaction.replace(R.id.frame_layout_main, fragment);
        //transaction.addToBackStack(null);
        transaction.commit();

        // Navigation Bar
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        btnSideBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.START);
            }
        });

        btnSideBar.setVisibility(View.VISIBLE);
        autocompleteSearch.setVisibility(View.VISIBLE);
        btnNotifications.setVisibility(View.INVISIBLE);


        autocompleteSearch.setHintTextColor(getResources().getColor(R.color.grey));
        autocompleteSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                TextView tvId = (TextView) view.findViewById(R.id.tv_item_id);//your textview id
                TextView tvname = (TextView) view.findViewById(R.id.tv_item_name);//your textview id
                String productId = tvId.getText().toString();
                String productName = tvname.getText().toString();
                Intent intent = new Intent(HomeActivity.this, ProductDataActivity.class);
                intent.putExtra("produact_id", productId);
                intent.putExtra("produact_name", productName);
                startActivity(intent);
                autocompleteSearch.setText("");
            }
        });
        ProductsWebService.getSections();
        ProductsWebService.getProducts();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_home;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                finish();
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getString(R.string.on_back_pressd_toast), Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    private void initView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        imgTitleBarLogo = (ImageView) findViewById(R.id.img_title_bar_logo);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        frameLayoutMain = (FrameLayout) findViewById(R.id.frame_layout_main);
        homeTabLayout = (TabLayout) findViewById(R.id.home_tab_layout);
        navView = (NavigationView) findViewById(R.id.nav_view);
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        imgLogo = (ImageView) findViewById(R.id.img_logo);
        tvSections = (TextView) findViewById(R.id.tv_sections);
        navRecyclerView = (RecyclerView) findViewById(R.id.navRecyclerView);
        tvSettings = (TextView) findViewById(R.id.tv_settings);
        tvFaqs = (TextView) findViewById(R.id.tv_faqs);
        tvCustomerService = (TextView) findViewById(R.id.tv_customer_service);
        tvContactUs = (TextView) findViewById(R.id.tv_contact_us);
        tvLogIn = (TextView) findViewById(R.id.tv_log_in);
        tvLogOut = (TextView) findViewById(R.id.tv_log_out);
        tvSelectLanguage = (TextView) findViewById(R.id.tv_select_language);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        rdBtnEnglish = (RadioButton) findViewById(R.id.rd_btn_english);
        rdBtnArabic = (RadioButton) findViewById(R.id.rd_btn_arabic);
        tvUserName = (TextView) findViewById(R.id.tv_user_name);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefManager.getLoginStatus()) {
            tvUserName.setText(sharedPrefManager.getUserData().getFirstname() + " " + sharedPrefManager.getUserData().getLastname());
            tvLogIn.setVisibility(View.GONE);
            tvLogOut.setVisibility(View.VISIBLE);
        } else {
            tvUserName.setText(getResources().getString(R.string.app_name));
            tvLogIn.setVisibility(View.VISIBLE);
            tvLogOut.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ProductsEvent productsEvent) {
        if (productsEvent.getProductsResponseModel().getSuccess()) {

            productList = productsEvent.getProductsResponseModel().getProducts();
            SearchAutoCompleteAdapter adapter = new SearchAutoCompleteAdapter(HomeActivity.this, productList);
            autocompleteSearch.setAdapter(adapter);
            autocompleteSearch.setThreshold(1);
        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SectionsEvent sectionsEvent) {
        if (sectionsEvent.getSectionsResponseModel().getSuccess()) {
            ProductsWebService.getLatest();
            sectionsList = sectionsEvent.getSectionsResponseModel().getCategoryInfo();
            SideMenuSectionProductsAdapter adapter = new SideMenuSectionProductsAdapter(HomeActivity.this, sectionsList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HomeActivity.this);
            layoutManager.setAutoMeasureEnabled(true);
            navRecyclerView.setLayoutManager(layoutManager);
            navRecyclerView.setNestedScrollingEnabled(false);
            navRecyclerView.setAdapter(adapter);
        } else {
        }
    }
}
