package net.collection.collection.Activities;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.EndlessRecyclerOnScrollListener;
import net.collection.collection.Adapters.RelatedProductsAdapter;
import net.collection.collection.Adapters.SectionCategoryAdapter;
import net.collection.collection.Adapters.SectionProductsAdapter;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Networks.Events.GetChildCategoryEvent;
import net.collection.collection.Networks.Events.GetProductEvent;
import net.collection.collection.Networks.Events.RelatedProductsEvent;
import net.collection.collection.Networks.Events.SectionProductsEvent;
import net.collection.collection.Networks.ResponseModels.GetChildCategoryResponseModel;
import net.collection.collection.Networks.ResponseModels.RelatedProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionProductsResponseModel;
import net.collection.collection.Networks.WebServices.ProductsWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class SectionProductsActivity extends ParentActivity {

    private CustomLoadingDialog customLoadingDialog2;
    private String productId;
    private String productName;
    private android.widget.LinearLayout rltvItemData;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private RecyclerView sectionProductsRecyclerView;
    private ArrayList<SectionProductsResponseModel.Products> allProducts;
    //    private List<SectionProductsResponseModel.Products> sectionProductsList;
    private List<GetChildCategoryResponseModel.CategoryInfo> sectioncategoryList;
    private LinearLayout lyoutAppBar;
    private RecyclerView sectionCategoryRecyclerView;
    static int current_page = 1;
    private GridLayoutManager linearLayoutManager;
    private SectionProductsAdapter adapter;
    private static int displayedposition = 0;
    @Override
    protected void initializeComponents() {
        initView();
        allProducts = new ArrayList<>();
        adapter = new SectionProductsAdapter(SectionProductsActivity.this, allProducts);

        btnBack.setVisibility(View.VISIBLE);
        btnMenu.setVisibility(View.INVISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        productId = getIntent().getStringExtra("produact_id");
        productName = getIntent().getStringExtra("produact_name");
        productName = productName.replaceAll("&amp;", " & ");

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvToolbarTitle.setText(productName);
        customLoadingDialog2 = new CustomLoadingDialog(SectionProductsActivity.this, R.style.DialogSlideAnim);
        customLoadingDialog2.show();
        ProductsWebService.getSectionCategory(productId);
       // ProductsWebService.getSectionProducts(productId, current_page);

        linearLayoutManager = new GridLayoutManager(SectionProductsActivity.this, 2);
        sectionProductsRecyclerView.setLayoutManager(linearLayoutManager);
        sectionProductsRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                SectionProductsActivity.current_page++;
                ProductsWebService.getSectionProducts(productId, SectionProductsActivity.current_page);

            }
        });
        resetPagination();

    }

    private void resetPagination() {
        current_page = 1;
        EndlessRecyclerOnScrollListener.previousTotal = 0;
        EndlessRecyclerOnScrollListener.loading = true;
        EndlessRecyclerOnScrollListener.visibleThreshold = 1;
        EndlessRecyclerOnScrollListener.firstVisibleItem = 0;
        EndlessRecyclerOnScrollListener.visibleItemCount = 0;
        EndlessRecyclerOnScrollListener.totalItemCount = 0;
        EndlessRecyclerOnScrollListener.current_page = 1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetPagination();
        allProducts = new ArrayList<>();
        sectioncategoryList = new ArrayList<>();
        ProductsWebService.getSectionCategory(productId);
        ProductsWebService.getSectionProducts(productId, current_page);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_section_products;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SectionProductsEvent sectionProductsEvent) {
        if (sectionProductsEvent.getSectionProductsResponseModel().getSuccess()) {
            System.out.println("current page : " + String.valueOf(current_page));
            ArrayList<SectionProductsResponseModel.Products> sectionProductsList = sectionProductsEvent.getSectionProductsResponseModel().getProducts();
          // if (allProducts.isEmpty()) {
           //    allProducts = sectionProductsList;
          // } else {
                allProducts.addAll(sectionProductsList);
                adapter.addProductsList(allProducts);
          //  }

            //allProducts.addAll(sectionProductsList);
            // adapter.notifyDataSetChanged();
            sectionProductsRecyclerView.setNestedScrollingEnabled(false);
            sectionProductsRecyclerView.setAdapter(adapter);
            customLoadingDialog2.cancel();
        } else {
            customLoadingDialog2.cancel();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GetChildCategoryEvent getChildCategoryEvent) {
        if (getChildCategoryEvent.getGetChildCategoryResponseModel().getSuccess()) {
            sectionCategoryRecyclerView.setVisibility(View.VISIBLE);
            sectioncategoryList = getChildCategoryEvent.getGetChildCategoryResponseModel().getCategoryInfo();
            SectionCategoryAdapter adapter = new SectionCategoryAdapter(SectionProductsActivity.this, sectioncategoryList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SectionProductsActivity.this, LinearLayoutManager.HORIZONTAL, false);
            sectionCategoryRecyclerView.setLayoutManager(layoutManager);
            //sectionCategoryRecyclerView.setNestedScrollingEnabled(false);
            sectionCategoryRecyclerView.setAdapter(adapter);
            customLoadingDialog2.cancel();
        } else {
            sectionCategoryRecyclerView.setVisibility(View.GONE);
            customLoadingDialog2.cancel();
        }
    }

    private void initView() {
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        sectionProductsRecyclerView = (RecyclerView) findViewById(R.id.section_products_recycler_view);
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        sectionCategoryRecyclerView = (RecyclerView) findViewById(R.id.section_category_recycler_view);
    }
}
