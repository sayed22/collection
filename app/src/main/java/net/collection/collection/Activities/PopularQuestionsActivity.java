package net.collection.collection.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.R;
public class PopularQuestionsActivity extends ParentActivity {
    private WebView mapview;
    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;

    @Override
    protected void initializeComponents() {
        initView();

        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.popular_questions));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mapview.loadUrl("http://www.collection-kw.com/index.php?route=information/information&information_id=10&app=1");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_popular_questions;
    }

    private void initView() {
        mapview = (WebView) findViewById(R.id.mapview);
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}