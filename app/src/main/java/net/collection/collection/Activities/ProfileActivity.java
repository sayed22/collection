package net.collection.collection.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Models.Userdata;
import net.collection.collection.Networks.Events.EditProfileEvent;
import net.collection.collection.Networks.Events.LogInEvent;
import net.collection.collection.Networks.WebServices.UserWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.ViewsUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProfileActivity extends ParentActivity {

    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etPhoneNumber;
    private EditText etEmail;
    private Button btnUpdateData;
    private String first_name;
    private String last_name;
    private String email;
    private String phone_number;
    private String customer_id;

    @Override
    protected void initializeComponents() {
        initView();
        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.my_profile));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        sharedPrefManager = new SharedPrefManager(this);
        etFirstName.setHint(getResources().getString(R.string.first_name) + " : " + sharedPrefManager.getUserData().getFirstname());
        etLastName.setHint(getResources().getString(R.string.last_name) + " : " +sharedPrefManager.getUserData().getLastname());
        etEmail.setHint(getResources().getString(R.string.email) + " : " +sharedPrefManager.getUserData().getEmail());
        etPhoneNumber.setHint(getResources().getString(R.string.phone_number) + " : " +sharedPrefManager.getUserData().getTelephone());


        btnUpdateData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                first_name = etFirstName.getText().toString();
                last_name = etLastName.getText().toString();
                email = etEmail.getText().toString();
                phone_number = etPhoneNumber.getText().toString();
                customer_id = sharedPrefManager.getUserData().getCustomerId();

                if (TextUtils.isEmpty(first_name)&&TextUtils.isEmpty(last_name)&&TextUtils.isEmpty(email)&&
                        TextUtils.isEmpty(phone_number)){
                    ViewsUtils.showSnackBarWithLong(context, etFirstName, getString(R.string.no_data_changed), R.color.white);
                }else {
                    if (TextUtils.isEmpty(first_name)){
                        first_name=sharedPrefManager.getUserData().getFirstname();
                    }if (TextUtils.isEmpty(last_name)){
                        last_name=sharedPrefManager.getUserData().getLastname();
                    }if (TextUtils.isEmpty(email)){
                        email=sharedPrefManager.getUserData().getEmail();
                    }if (TextUtils.isEmpty(phone_number)){
                        phone_number=sharedPrefManager.getUserData().getTelephone();
                    }
                    UserWebService.editProfile(first_name,last_name,email,phone_number,"12345",sharedPrefManager.getUserData().getCustomerId());
                    customLoadingDialog.show();
                }
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }

    private void initView() {
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etPhoneNumber = (EditText) findViewById(R.id.et_phone_number);
        etEmail = (EditText) findViewById(R.id.et_email);
        btnUpdateData = (Button) findViewById(R.id.btn_update_data);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        LogInActivity.this.overridePendingTransition(R.anim.animation, R.anim.animation2);
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLogIn(EditProfileEvent editProfileEvent) {
        if (editProfileEvent.getEditProfileResponseModel().getSuccess()) {
            Userdata userdata = sharedPrefManager.getUserData();
            userdata.setFirstname(first_name);
            userdata.setLastname(last_name);
            userdata.setEmail(email);
            userdata.setTelephone(phone_number);
            sharedPrefManager.setUserData(userdata);
            sharedPrefManager.setLoginStatus(true);
            finish();
            Toast.makeText(ProfileActivity.this,getResources().getString(R.string.data_updated),Toast.LENGTH_LONG).show();

            customLoadingDialog.cancel();

        }

    }
}
