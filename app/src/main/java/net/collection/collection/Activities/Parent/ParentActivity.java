package net.collection.collection.Activities.Parent;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.tsengvn.typekit.TypekitContextWrapper;

import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.R;
import net.collection.collection.Utils.SharedPrefManager;

public abstract class ParentActivity extends AppCompatActivity {

    protected AppCompatActivity activity;
   // protected SharedPrefManager sharedPrefManager;


    Toolbar toolbar;
    TextView tv_toolbar_title;
    public AppCompatActivity context;
    private int menuId;
    private String currentToolbarTitle;
    public CustomLoadingDialog customLoadingDialog;
    public static SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        setContentView(getLayoutResource());
        activity = this;
        //sharedPrefManager = new SharedPrefManager(activity);
        initializeComponents();
        customLoadingDialog = new CustomLoadingDialog(this, R.style.DialogSlideAnim);
        sharedPrefManager = new SharedPrefManager(activity);

    }

    protected abstract void initializeComponents();


    /**
     *
     * @return the layout resource id
     */
    protected abstract int getLayoutResource();


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    /**
     * this method allowed me to initialize the typeKit for font change
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}