package net.collection.collection.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.R;

public class MainActivity extends ParentActivity {

    private Button btnNewUser;
    private Button btnLogIn;
    private android.widget.TextView tvSkip;

    @Override
    protected void initializeComponents() {
        initView();
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LogInActivity.class);

                Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                startActivity(intent, bndlanimation);
            }
        });
        btnNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);

                Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                startActivity(intent, bndlanimation);
            }
        });
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);

                Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                startActivity(intent, bndlanimation);
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }



    private void initView() {
        btnNewUser = (Button) findViewById(R.id.btn_new_user);
        btnLogIn = (Button) findViewById(R.id.btn_log_in);
        tvSkip = (TextView) findViewById(R.id.tv_skip);
    }
}
