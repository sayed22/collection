package net.collection.collection.Activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.github.florent37.viewanimator.ViewAnimator;
import com.squareup.picasso.Callback;
import net.collection.collection.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ImagePreviewActivity extends AppCompatActivity {
    ImageView profileImageView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);
        profileImageView = (ImageView) findViewById(R.id.profile_image_view) ;
        setTitle("");

        if (getIntent().getStringExtra("photo")==null || getIntent().getStringExtra("photo").equals("")) {
            //profileImageView.setImageResource(R.drawable.no_main_img);
        } else {
            Picasso.with(ImagePreviewActivity.this).load(getIntent().getStringExtra("photo")).into(profileImageView);

        }
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1){
            ViewAnimator
                    .animate(profileImageView)
                    //.bounceIn().interpolator(new BounceInterpolator())
                    //.shake().interpolator(new LinearInterpolator())
                    //.flash().repeatCount(4) // good for waiting btn
                    //.flipHorizontal()
                    .duration(1000)
//                .tada()
//                .pulse()
//                .standUp()
//                .swing()
//                .wobble()
                    .newsPaper()  // good for splash
                    //.rubber()
                    //.slideTop()
                    .start();
        }
    }

}
