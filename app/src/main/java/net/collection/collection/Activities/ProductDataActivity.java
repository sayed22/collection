package net.collection.collection.Activities;

import android.database.Cursor;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.CommentsAdapter;
import net.collection.collection.Adapters.ImagesAdapter;
import net.collection.collection.Adapters.ProductOptionsAdapter;
import net.collection.collection.Adapters.ProductOptionsItemAdapter;
import net.collection.collection.Adapters.RelatedProductsAdapter;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Fragments.CartFragment;
import net.collection.collection.Models.MyCartModel;
import net.collection.collection.Models.OptionList;
import net.collection.collection.Networks.Events.AddProductToCartEvent;
import net.collection.collection.Networks.Events.GetProductEvent;
import net.collection.collection.Networks.Events.ImagesCommentsDiscountDataEvent;
import net.collection.collection.Networks.Events.ProductOptionsEvent;
import net.collection.collection.Networks.Events.RelatedProductsEvent;
import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.Networks.ResponseModels.RelatedProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.WishListResponseModel;
import net.collection.collection.Networks.WebServices.CartWebService;
import net.collection.collection.Networks.WebServices.ProductsWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static net.collection.collection.Database.DataBase.DOWNLOAD_COL;
import static net.collection.collection.Database.DataBase.ID_COL;
import static net.collection.collection.Database.DataBase.IMAGE_COL;
import static net.collection.collection.Database.DataBase.MODEL_COL;
import static net.collection.collection.Database.DataBase.NAME_COL;
import static net.collection.collection.Database.DataBase.OPTION_COL;
import static net.collection.collection.Database.DataBase.PRICE_COL;
import static net.collection.collection.Database.DataBase.QUANTITY_COL;
import static net.collection.collection.Database.DataBase.REWARD_COL;
import static net.collection.collection.Database.DataBase.SUBSTRACT_COL;
import static net.collection.collection.Database.DataBase.TAX_COL;
import static net.collection.collection.Database.DataBase.TOTAL_COL;

public class ProductDataActivity extends ParentActivity {

    String productId;
    String productName;
    private List<RelatedProductsResponseModel.Relatedproduct> relatedProductsList;
    private CustomLoadingDialog customLoadingDialog2;
    private List<ImagesCommentsDiscountDataResponseModel.AddImgs> imagesList;
    private List<ImagesCommentsDiscountDataResponseModel.Reviewsdata> commentsList;
    private List<ImagesCommentsDiscountDataResponseModel.Discountsdata> discountsDataList;
    private List<ProductOptionsResponseModel.ProductsOption> productOptionList;
    private RecyclerView productOptionsRecyclerView;
    String product_options[];
    String productUrl;
    int count;
    private List<WishListResponseModel.Wishlist> wishList;
    private boolean isLiked;
    String imgUrl;

    private Animation animZoomIn;
    private RelativeLayout rltvItemData;
    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private ScrollView scrollProductData;
    private FrameLayout frmImg;
    private ImageView imgItem;
    private ImageView imgShare;
    private TextView tvItemTitle;
    private RelativeLayout linPrice;
    private TextView tvNewPrice;
    private TextView tvDeprecatedPrice;
    private TextView tvStockStatus;
    private TextView tvProductType;
    private TextView tvReward;
    private TextView tvPriceByReward;
    private TextView tvDiscountsdata;
    private LinearLayout linQuantity;
    private TextView tvPlus;
    private TextView etCount;
    private TextView tvMinus;
    private TextView tvProductOptions;
    private TextView tvItemDescribe;
    private RecyclerView productImagesRecyclerView;
    private TextView tvAddComment;
    private RecyclerView productCommentsRecyclerView;
    private RecyclerView relatedProductsRecyclerView;
    private TextView tvAddToPasket;
    private LinearLayout linAddTofavorit;
    private ImageView imgLike;
    private TextView tvAddToFavorit;
    Gson gson = new Gson();
    private String json;
    private TextView tvRelatedProducts;

    @Override
    protected void initializeComponents() {
        initView();
        sharedPrefManager = new SharedPrefManager(ProductDataActivity.this);
        btnBack.setVisibility(View.VISIBLE);
        btnMenu.setVisibility(View.INVISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        productId = getIntent().getStringExtra("produact_id");
        System.out.println("product id : " + productId);
        productName = getIntent().getStringExtra("produact_name");
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvToolbarTitle.setText(productName);
        customLoadingDialog2 = new CustomLoadingDialog(ProductDataActivity.this, R.style.DialogSlideAnim);
        customLoadingDialog2.show();
        ProductsWebService.getProduct(productId);
        ProductsWebService.getProductOptions(productId);
        ProductsWebService.getImagesCommentsDiscountData(productId);

        tvProductOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productOptionsRecyclerView.getVisibility() == View.VISIBLE) {
                    productOptionsRecyclerView.setVisibility(View.GONE);
                } else {
                    productOptionsRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });

        tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = Integer.parseInt(etCount.getText().toString());
                count++;
                etCount.setText(String.valueOf(count));
            }
        });
        tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = Integer.parseInt(etCount.getText().toString());
                if (count > 1) {
                    count--;
                } else {
                    count = 1;
                }
                etCount.setText(String.valueOf(count));
            }
        });

        tvAddToPasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAddToPasket.setBackground(getResources().getDrawable(R.color.colorPrimary));
                tvAddToPasket.setTextColor(getResources().getColor(R.color.white));
                List<OptionList> productOptions = new ArrayList<>();
                productOptions = ProductOptionsItemAdapter.productOptions;

                json = gson.toJson(productOptions);
                System.out.println(json);
//
//                DataBase db = new DataBase(context);
//                Cursor cr = db.getDataFromTable(db, DataBase.MY_CART_TABLE);
//                if (cr.getCount() > 0) {
//                    cr.moveToFirst();
//
//                    do {
//                        if (productId.equals(cr.getString(cr.getColumnIndex(ID_COL)))) {
//                            Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.product_exist), Toast.LENGTH_LONG).show();
//                        } else {
//                            addToCart();
//                        }
//                    } while (cr.moveToNext());
//
//                } else {
//                    addToCart();
//                }
//                cr.close();
//                db.close();
                ArrayList<MyCartModel> cartMoudels = new ArrayList<>();
                MyCartModel listModel;
                boolean isExist = false;
                DataBase db = new DataBase(context);
                Cursor cr = db.getDataFromTable(db, DataBase.MY_CART_TABLE);
                if (cr.getCount() > 0) {
                    cr.moveToFirst();
                    do {
                        listModel = new MyCartModel(cr.getString(cr.getColumnIndex(ID_COL))
                                , cr.getString(cr.getColumnIndex(NAME_COL))
                                , cr.getString(cr.getColumnIndex(MODEL_COL))
                                , cr.getString(cr.getColumnIndex(OPTION_COL))
                                , cr.getString(cr.getColumnIndex(DOWNLOAD_COL))
                                , cr.getString(cr.getColumnIndex(QUANTITY_COL))
                                , cr.getString(cr.getColumnIndex(SUBSTRACT_COL))
                                , cr.getString(cr.getColumnIndex(PRICE_COL))
                                , cr.getString(cr.getColumnIndex(TOTAL_COL))
                                , cr.getString(cr.getColumnIndex(TAX_COL))
                                , cr.getString(cr.getColumnIndex(REWARD_COL))
                                , cr.getString(cr.getColumnIndex(IMAGE_COL)));
                        cartMoudels.add(listModel);
                    } while (cr.moveToNext());

                    for (int i = 0; i < cartMoudels.size(); i++) {
                        if (cartMoudels.get(i).getId().equals(productId)) {
                            isExist = true;
                            break;
                        } else {
                            isExist = false;
                        }
                    }
                    if (isExist) {
                        Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.product_exist), Toast.LENGTH_LONG).show();
                    } else {
                        addToCart();
                    }
                } else {
                    addToCart();
                }
                cr.close();
                db.close();


            }
        });
        DataBase db = new DataBase(context);
        Cursor cr = db.getDataFromTable(db, DataBase.MY_WISH_LIST_TABLE);
        if (cr.getCount() > 0) {
            cr.moveToFirst();

            do {
                if (productId.equals(cr.getString(cr.getColumnIndex(ID_COL)))) {
                    imgLike.setImageResource(R.drawable.ic_is_like);
                    isLiked = true;
                }

            } while (cr.moveToNext());

        }
        cr.close();
        tvAddToFavorit.setText(String.valueOf(db.getTableRowsCount(DataBase.MY_WISH_LIST_TABLE)));
        db.close();

        linAddTofavorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgLike.startAnimation(animZoomIn);
                if (!isLiked) {
                    imgLike.setImageResource(R.drawable.ic_is_like);
                    Toast.makeText(context, context.getResources().getString(R.string.added_to_your_wish_list), Toast.LENGTH_LONG).show();
                    DataBase db = new DataBase(context);

                    db.insertIntoMyWishList(db
                            , productId
                            , productName
                            , tvNewPrice.getText().toString()
                            , imgUrl);
                    tvAddToFavorit.setText(String.valueOf(db.getTableRowsCount(DataBase.MY_WISH_LIST_TABLE)));
                    db.close();

                } else {
                    imgLike.setImageResource(R.drawable.heart_button);
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.removed_from_wish_list), Toast.LENGTH_LONG).show();
                    DataBase db = new DataBase(context);
                    db.deleteSingleRow(db, DataBase.MY_WISH_LIST_TABLE, productId);
                    tvAddToFavorit.setText(String.valueOf(db.getTableRowsCount(DataBase.MY_WISH_LIST_TABLE)));
                    db.close();
                }
                isLiked = !isLiked;

            }

        });

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgShare.startAnimation(animZoomIn);
                Utils.Share(ProductDataActivity.this, productUrl);
            }
        });

    }

    public void addToCart() {
        tvAddToPasket.startAnimation(animZoomIn);
        String price = tvNewPrice.getText().toString();
        price = price.replaceAll("KD", "");
        // price = price.replaceAll("[^\\d-]", "");
        double total = Double.parseDouble(price) * Double.parseDouble(etCount.getText().toString());
        DataBase db2 = new DataBase(context);
        System.out.println("array :" + json);
        String type = tvProductType.getText().toString();
        type = type.replace(getResources().getString(R.string.type), "");
        db2.insertIntoMyCart(db2
                , String.valueOf(productId)
                , productName
                , type
                , json
                , ""
                , etCount.getText().toString()
                , ""
                , price
                , String.valueOf(total)
                , ""
                , ""
                , imgUrl);
        db2.close();
        Toast.makeText(ProductDataActivity.this, getResources().getString(R.string.product_added_to_cart), Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_product_data;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GetProductEvent getProductEvent) {
        if (getProductEvent.getGetProductResponseModel().getSuccess()) {
            productUrl = getProductEvent.getGetProductResponseModel().getProduct().getHref();
            productUrl = productUrl.replace("amp;", "");
            imgUrl = getProductEvent.getGetProductResponseModel().getProduct().getThumb();
            String url = Constants.IMAGE_URL + imgUrl;
            url = url.replaceAll(" ", "%20");
            System.out.println("cat image : " + url);

            Picasso.with(context).load(url).into(imgItem);
            tvItemTitle.setText(getProductEvent.getGetProductResponseModel().getProduct().getName());
            if (getProductEvent.getGetProductResponseModel().getProduct().getSpecial().equals("0")) {
                tvNewPrice.setText(String.valueOf(getProductEvent.getGetProductResponseModel().getProduct().getPrice()));
                tvDeprecatedPrice.setText("");
            } else {
                tvNewPrice.setText(String.valueOf(getProductEvent.getGetProductResponseModel().getProduct().getSpecial()));
                tvDeprecatedPrice.setText(String.valueOf(getProductEvent.getGetProductResponseModel().getProduct().getPrice()));
            }
            String quantity = getProductEvent.getGetProductResponseModel().getProduct().getQuantity();
            if (quantity.equals("0")){
                tvStockStatus.setText(getResources().getString(R.string.stock_status) + " : " + getProductEvent.getGetProductResponseModel().getProduct().getStockStatus());
            }else {
                tvStockStatus.setText(getResources().getString(R.string.stock_status_in_stock));
            }
            tvItemDescribe.setText(getProductEvent.getGetProductResponseModel().getProduct().getDescription());
            tvProductType.setText(getResources().getString(R.string.product_type) + " : " + getProductEvent.getGetProductResponseModel().getProduct().getModel());
            tvReward.setText(getResources().getString(R.string.reward) + " : " + getProductEvent.getGetProductResponseModel().getProduct().getReward());
            if (getProductEvent.getGetProductResponseModel().getProduct().getReward() == null) {
                tvReward.setVisibility(View.GONE);
            }
            tvPriceByReward.setText(getResources().getString(R.string.price_by_reward) + " : " + getProductEvent.getGetProductResponseModel().getProduct().getPoints());
            int points = Integer.parseInt(getProductEvent.getGetProductResponseModel().getProduct().getPoints());
            if (points <= 0) {
                tvPriceByReward.setVisibility(View.GONE);
            }
            ProductsWebService.getRelatedProduct(productId);
            customLoadingDialog2.cancel();

        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RelatedProductsEvent relatedProductsEvent) {
        if (relatedProductsEvent.getRelatedProductsResponseModel().getSuccess()) {
            tvRelatedProducts.setVisibility(View.VISIBLE);
            relatedProductsList = relatedProductsEvent.getRelatedProductsResponseModel().getRelatedproduct();
            RelatedProductsAdapter adapter = new RelatedProductsAdapter(ProductDataActivity.this, relatedProductsList);
            relatedProductsRecyclerView.setLayoutManager(new GridLayoutManager(ProductDataActivity.this, 2));
            relatedProductsRecyclerView.setNestedScrollingEnabled(false);
            relatedProductsRecyclerView.setAdapter(adapter);

        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ProductOptionsEvent productOptionsEvent) {
        if (productOptionsEvent.getProductOptionsResponseModel().getSuccess()) {
            tvProductOptions.setVisibility(View.VISIBLE);
            productOptionList = productOptionsEvent.getProductOptionsResponseModel().getProductsOption();
            ProductOptionsAdapter adapter = new ProductOptionsAdapter(ProductDataActivity.this, productOptionList);
            productOptionsRecyclerView.setLayoutManager(new LinearLayoutManager(ProductDataActivity.this));
            productOptionsRecyclerView.setNestedScrollingEnabled(false);
            productOptionsRecyclerView.setAdapter(adapter);
        } else {
            tvProductOptions.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AddProductToCartEvent addProductToCartEvent) {
        if (addProductToCartEvent.getAddProductToCartResponseModel().getSuccess()) {
            Toast.makeText(ProductDataActivity.this, getResources().getString(R.string.product_added_to_cart), Toast.LENGTH_LONG).show();
            customLoadingDialog2.cancel();
            //finish();
        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ImagesCommentsDiscountDataEvent imagesCommentsDiscountDataEvent) {
        if (imagesCommentsDiscountDataEvent.getImagesCommentsDiscountDataResponseModel().getSuccess()) {
            imagesList = imagesCommentsDiscountDataEvent.getImagesCommentsDiscountDataResponseModel().getProductsProdadditional().getAddImgs();
            commentsList = imagesCommentsDiscountDataEvent.getImagesCommentsDiscountDataResponseModel().getProductsProdadditional().getReviewsdata();
            discountsDataList = imagesCommentsDiscountDataEvent.getImagesCommentsDiscountDataResponseModel().getProductsProdadditional().getDiscountsdata();
            if (!discountsDataList.isEmpty()) {
                tvDiscountsdata.setVisibility(View.VISIBLE);
                for (int i = 0; i < discountsDataList.size(); i++) {
                    tvDiscountsdata.setText(discountsDataList.get(i).getQuantity() + " " + getResources().getString(R.string.discountsdata) + " " + discountsDataList.get(i).getPrice());
                }
            } else {
                tvDiscountsdata.setVisibility(View.GONE);
            }
            ImagesAdapter adapter = new ImagesAdapter(ProductDataActivity.this, imagesList);
            productImagesRecyclerView.setLayoutManager(new LinearLayoutManager(ProductDataActivity.this));
            productImagesRecyclerView.setNestedScrollingEnabled(false);
            productImagesRecyclerView.setAdapter(adapter);

            CommentsAdapter adapter2 = new CommentsAdapter(ProductDataActivity.this, commentsList);
            productCommentsRecyclerView.setLayoutManager(new LinearLayoutManager(ProductDataActivity.this));
            productCommentsRecyclerView.setNestedScrollingEnabled(false);
            productCommentsRecyclerView.setAdapter(adapter2);

        } else {

        }
    }

    private void initView() {
        rltvItemData = (RelativeLayout) findViewById(R.id.rltv_item_data);
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        scrollProductData = (ScrollView) findViewById(R.id.scroll_product_data);
        frmImg = (FrameLayout) findViewById(R.id.frm_img);
        imgItem = (ImageView) findViewById(R.id.img_item);
        imgShare = (ImageView) findViewById(R.id.img_share);
        tvItemTitle = (TextView) findViewById(R.id.tv_item_title);
        linPrice = (RelativeLayout) findViewById(R.id.lin_price);
        tvNewPrice = (TextView) findViewById(R.id.tv_new_price);
        tvDeprecatedPrice = (TextView) findViewById(R.id.tv_deprecated_price);
        tvStockStatus = (TextView) findViewById(R.id.tv_stock_status);
        tvProductType = (TextView) findViewById(R.id.tv_product_type);
        tvReward = (TextView) findViewById(R.id.tv_reward);
        tvPriceByReward = (TextView) findViewById(R.id.tv_price_by_reward);
        tvDiscountsdata = (TextView) findViewById(R.id.tv_discountsdata);
        linQuantity = (LinearLayout) findViewById(R.id.lin_quantity);
        tvPlus = (TextView) findViewById(R.id.tv_plus);
        etCount = (TextView) findViewById(R.id.et_count);
        tvMinus = (TextView) findViewById(R.id.tv_minus);
        tvProductOptions = (TextView) findViewById(R.id.tv_product_options);
        tvItemDescribe = (TextView) findViewById(R.id.tv_item_describe);
        productOptionsRecyclerView = (RecyclerView) findViewById(R.id.product_options_recycler_view);
        productImagesRecyclerView = (RecyclerView) findViewById(R.id.product_images_recycler_view);
        tvAddComment = (TextView) findViewById(R.id.tv_add_comment);
        productCommentsRecyclerView = (RecyclerView) findViewById(R.id.product_comments_recycler_view);
        tvRelatedProducts = (TextView) findViewById(R.id.tv_related_products);
        relatedProductsRecyclerView = (RecyclerView) findViewById(R.id.related_products_recycler_view);
        tvAddToPasket = (TextView) findViewById(R.id.tv_add_to_pasket);
        linAddTofavorit = (LinearLayout) findViewById(R.id.lin_add_tofavorit);
        imgLike = (ImageView) findViewById(R.id.img_like);
        tvAddToFavorit = (TextView) findViewById(R.id.tv_add_to_favorit);
        animZoomIn = AnimationUtils.loadAnimation(context, R.anim.zoom_in);

    }
}
