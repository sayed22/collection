package net.collection.collection.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Networks.Events.ForgotPasswordEvent;
import net.collection.collection.Networks.Events.LogInEvent;
import net.collection.collection.Networks.WebServices.UserWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.ViewsUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForgotPasswordActivity extends ParentActivity {

    private ImageView btnBack;
    private TextView tvToolbarTitle;
    private ImageView btnNotifications;
    private EditText etEmail;
    private Button btnLogInByPhone;
    private Button btnConfirm;

    @Override
    protected void initializeComponents() {
        initView();
        tvToolbarTitle.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvToolbarTitle.setText(getResources().getString(R.string.forgot_password));

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    etEmail.setError(getResources().getString(R.string.enter_email));
                } else if (!emailValidator(etEmail.getText().toString())) {
                    etEmail.setError(getResources().getString(R.string.wrong_email));
                }else {
                    UserWebService.forgotPassword(etEmail.getText().toString());
                    customLoadingDialog.show();
                }
            }
        });

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_forgot_password;
    }


    private void initView() {
        btnBack = (ImageView) findViewById(R.id.btn_back);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        etEmail = (EditText) findViewById(R.id.et_email);
        btnConfirm = (Button) findViewById(R.id.btn_confirm);
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        LogInActivity.this.overridePendingTransition(R.anim.animation, R.anim.animation2);
    }
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ForgotPasswordEvent forgotPasswordEvent) {
        if (forgotPasswordEvent.getForgotPasswordResponseModel().getSuccess()) {
            Intent intent = new Intent(ForgotPasswordActivity.this, LogInActivity.class);
            Bundle bndlanimation =
                    ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
            startActivity(intent, bndlanimation);
            finish();
            customLoadingDialog.cancel();
            Toast.makeText(ForgotPasswordActivity.this,getResources().getString(R.string.new_password_sent),Toast.LENGTH_LONG).show();
        } else {
            ViewsUtils.showSnackBarWithLong(context, etEmail, getString(R.string.email_not_in_our_data), R.color.white);
            customLoadingDialog.dismiss();
        }
    }
}
