package net.collection.collection.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Models.Userdata;
import net.collection.collection.Networks.Events.EditPasswordEvent;
import net.collection.collection.Networks.Events.LogInEvent;
import net.collection.collection.Networks.WebServices.UserWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.ViewsUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditPasswordActivity extends ParentActivity {

    private android.widget.ImageView btnBack;
    private android.widget.ImageView btnSideBar;
    private android.widget.TextView tvToolbarTitle;
    private android.widget.AutoCompleteTextView autocompleteSearch;
    private android.widget.ImageView btnNotifications;
    private android.widget.ImageView btnMenu;
    private android.widget.ImageView btnSettings;
    private android.widget.EditText etOldPassword;
    private android.widget.EditText etNewPassword;
    private android.widget.EditText etConfirmNewPassword;
    private android.widget.Button btnUpdateData;

    @Override
    protected void initializeComponents() {
        initView();
        sharedPrefManager=new SharedPrefManager(EditPasswordActivity.this);
        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.edit_password));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnUpdateData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etNewPassword.getText().toString().trim())) {
                    etNewPassword.setError(getResources().getString(R.string.enter_new_password));
                } else if (etNewPassword.length() < 6) {
                    etNewPassword.setError(getResources().getString(R.string.short_password));
                } else if (TextUtils.isEmpty(etConfirmNewPassword.getText().toString().trim())) {
                    etConfirmNewPassword.setError(getResources().getString(R.string.enter_confirm_password));
                } else if (!etNewPassword.getText().toString().equals(etConfirmNewPassword.getText().toString())) {
                    etConfirmNewPassword.setError(getResources().getString(R.string.password_not_match));
                }else {
                    UserWebService.editPassword(sharedPrefManager.getUserData().getEmail(),etOldPassword.getText().toString(),etNewPassword.getText().toString());
                    customLoadingDialog.show();
                }
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_password;
    }

    private void initView() {
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        etOldPassword = (EditText) findViewById(R.id.et_old_password);
        etNewPassword = (EditText) findViewById(R.id.et_new_password);
        etConfirmNewPassword = (EditText) findViewById(R.id.et_confirm_new_password);
        btnUpdateData = (Button) findViewById(R.id.btn_update_data);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EditPasswordEvent editPasswordEvent) {
        if (editPasswordEvent.getEditPasswordResponseModel().getSuccess()) {
            finish();
            Toast.makeText(EditPasswordActivity.this,getResources().getString(R.string.data_updated),Toast.LENGTH_LONG).show();
            customLoadingDialog.cancel();

        } else {
            ViewsUtils.showSnackBarWithLong(context, etOldPassword, getString(R.string.wrong_old_password), R.color.white);
            customLoadingDialog.dismiss();
        }
    }
}
