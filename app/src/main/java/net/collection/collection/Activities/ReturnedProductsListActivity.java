package net.collection.collection.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.ReturnedProductsListAdapter;
import net.collection.collection.Adapters.WishListAdapter;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Networks.Events.ReturnedProductsListEvent;
import net.collection.collection.Networks.Events.WishListEvent;
import net.collection.collection.Networks.ResponseModels.ReturnedProductsListResponseModel;
import net.collection.collection.Networks.WebServices.OrdersWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class ReturnedProductsListActivity extends ParentActivity {

    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private RecyclerView returnedProductsRecyclerView;
    private ReturnedProductsListResponseModel.Returns returnedList;
    private List<ReturnedProductsListResponseModel.Results> resultsList;
    private CustomLoadingDialog customLoadingDialog;
    private android.widget.RelativeLayout linLoginFirst;
    private ImageView imgLogo;
    private TextView tvLogin;
    private android.widget.Button btnLogIn;
    private RelativeLayout linNoDataFound;


    @Override
    protected void initializeComponents() {
        initView();
        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.returned_products));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        customLoadingDialog = new CustomLoadingDialog(ReturnedProductsListActivity.this, R.style.DialogSlideAnim);
        sharedPrefManager = new SharedPrefManager(ReturnedProductsListActivity.this);

        if (sharedPrefManager.getLoginStatus()){
            OrdersWebService.returnedProductsList(sharedPrefManager.getUserData().getCustomerId());
            customLoadingDialog.show();
            linLoginFirst.setVisibility(View.GONE);
        }else {
           // Utils.logInFirest(ReturnedProductsListActivity.this);
            linLoginFirst.setVisibility(View.VISIBLE);
        }
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReturnedProductsListActivity.this,LogInActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_returned_products_list;
    }

    private void initView() {
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        returnedProductsRecyclerView = (RecyclerView) findViewById(R.id.returned_products_recycler_view);
        linLoginFirst = (RelativeLayout) findViewById(R.id.lin_login_first);
        imgLogo = (ImageView) findViewById(R.id.img_logo);
        tvLogin = (TextView) findViewById(R.id.tv_login);
        btnLogIn = (Button) findViewById(R.id.btn_log_in);
        linNoDataFound = (RelativeLayout) findViewById(R.id.lin_no_data_found);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefManager.getLoginStatus()){
            OrdersWebService.returnedProductsList(sharedPrefManager.getUserData().getCustomerId());
            customLoadingDialog.show();
            linLoginFirst.setVisibility(View.GONE);
        }else {
            // Utils.logInFirest(ReturnedProductsListActivity.this);
            linLoginFirst.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ReturnedProductsListEvent returnedProductsListEvent) {
        if (returnedProductsListEvent.getReturnedProductsListResponseModel().getSuccess()) {
            linNoDataFound.setVisibility(View.GONE);

            returnedList = returnedProductsListEvent.getReturnedProductsListResponseModel().getReturns();
            resultsList = returnedList.getResults();
            ReturnedProductsListAdapter adapter = new ReturnedProductsListAdapter(ReturnedProductsListActivity.this, resultsList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ReturnedProductsListActivity.this);
            layoutManager.setAutoMeasureEnabled(true);
            returnedProductsRecyclerView.setLayoutManager(layoutManager);
            returnedProductsRecyclerView.setNestedScrollingEnabled(false);
            returnedProductsRecyclerView.setAdapter(adapter);
            customLoadingDialog.cancel();
        } else {
            customLoadingDialog.cancel();
            linNoDataFound.setVisibility(View.VISIBLE);

        }
    }

}
