package net.collection.collection.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.MyOrdersAdapter;
import net.collection.collection.Adapters.SectionProductsAdapter;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Networks.Events.ForgotPasswordEvent;
import net.collection.collection.Networks.Events.GetOrdersEvent;
import net.collection.collection.Networks.ResponseModels.GetOrdersResponseModel;
import net.collection.collection.Networks.WebServices.OrdersWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.Utils;
import net.collection.collection.Utils.ViewsUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class MyOrdersActivity extends ParentActivity {

    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private RecyclerView ordersRecyclerView;
    CustomLoadingDialog customLoadingDialog;
    private List<GetOrdersResponseModel.Orders> ordersList;
    SharedPrefManager sharedPrefManager;
    private android.widget.RelativeLayout linLoginFirst;
    private ImageView imgLogo;
    private TextView tvLogin;
    private android.widget.Button btnLogIn;

    @Override
    protected void initializeComponents() {
        initView();
        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        customLoadingDialog = new CustomLoadingDialog(this, R.style.DialogSlideAnim);
        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.my_orders));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (sharedPrefManager.getLoginStatus()) {
            OrdersWebService.getOrders(sharedPrefManager.getUserData().getCustomerId());
            customLoadingDialog.show();
            linLoginFirst.setVisibility(View.GONE);
        } else {
            //Utils.logInFirest(MyOrdersActivity.this);
            linLoginFirst.setVisibility(View.VISIBLE);
        }
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyOrdersActivity.this,LogInActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my_orders;
    }

    private void initView() {
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        ordersRecyclerView = (RecyclerView) findViewById(R.id.orders_recycler_view);
        linLoginFirst = (RelativeLayout) findViewById(R.id.lin_login_first);
        imgLogo = (ImageView) findViewById(R.id.img_logo);
        tvLogin = (TextView) findViewById(R.id.tv_login);
        btnLogIn = (Button) findViewById(R.id.btn_log_in);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        LogInActivity.this.overridePendingTransition(R.anim.animation, R.anim.animation2);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefManager.getLoginStatus()) {
            OrdersWebService.getOrders(sharedPrefManager.getUserData().getCustomerId());
            customLoadingDialog.show();
            linLoginFirst.setVisibility(View.GONE);
        } else {
            //Utils.logInFirest(MyOrdersActivity.this);
            linLoginFirst.setVisibility(View.VISIBLE);
            customLoadingDialog.cancel();

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GetOrdersEvent getOrdersEvent) {
        if (getOrdersEvent.getGetOrdersResponseModel().getSuccess()) {
            ordersList = getOrdersEvent.getGetOrdersResponseModel().getOrders();
            MyOrdersAdapter adapter = new MyOrdersAdapter(MyOrdersActivity.this, ordersList);
            ordersRecyclerView.setLayoutManager(new LinearLayoutManager(MyOrdersActivity.this));
            ordersRecyclerView.setNestedScrollingEnabled(false);
            ordersRecyclerView.setAdapter(adapter);
            customLoadingDialog.cancel();
        } else {
            customLoadingDialog.cancel();

        }
    }
}
