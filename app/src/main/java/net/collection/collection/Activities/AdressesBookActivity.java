package net.collection.collection.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.CountriesAdapter;
import net.collection.collection.Adapters.CountryZonesAdapter;
import net.collection.collection.Adapters.GetAddressesAdapter;
import net.collection.collection.Adapters.ImagesAdapter;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Networks.Events.AddNNewAddressEvent;
import net.collection.collection.Networks.Events.CountriesEvent;
import net.collection.collection.Networks.Events.CountryZonesEvent;
import net.collection.collection.Networks.Events.DeleteAddressEvent;
import net.collection.collection.Networks.Events.GetAddressesEvent;
import net.collection.collection.Networks.ResponseModels.AddNewAddressResponseModel;
import net.collection.collection.Networks.ResponseModels.CountriesResponseModel;
import net.collection.collection.Networks.ResponseModels.CountryZonesResponseModel;
import net.collection.collection.Networks.ResponseModels.GetAddressesResponseModel;
import net.collection.collection.Networks.WebServices.AddressesWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import static net.collection.collection.Utils.Constants.itemClicked;

public class AdressesBookActivity extends ParentActivity implements IRecyclerItemClicked {

    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etCompany;
    private EditText etFirstAddress;
    private EditText etSecondAddress;
    private EditText etCity;
    private EditText etPostCode;
    private TextView tvCountry;
    private TextView tvZones;
    private RadioButton rdbtnYes;
    private RadioButton rdbtnNo;
    private TextView addressesMenu;
    private TextView addAddress;
    private LinearLayout linAddAddress;
    private RecyclerView countryRecyclerView;
    private RecyclerView zonesRecyclerView;
    private Button btnAddAddress;
    private RecyclerView addressesMenuRecyclerView;
    private List<CountriesResponseModel.Countries> countriesList;
    private List<CountryZonesResponseModel.Zones> countryZonesList = new ArrayList<>();

    String defaultAddress = "0";
    private List<GetAddressesResponseModel.Addressesdata> addressesDataList;
    private SharedPrefManager sharedPrefManager;
    CustomLoadingDialog customLoadingDialog;
    private LinearLayout lyoutAppBar;
    private LinearLayout linAddressesBookData;
    private android.widget.RelativeLayout linLoginFirst;
    private ImageView imgLogo;
    private TextView tvLogin;
    private Button btnLogIn;

    @Override
    protected void initializeComponents() {
        initView();
        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        customLoadingDialog = new CustomLoadingDialog(this, R.style.DialogSlideAnim);
        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.address_book));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        addAddress.setBackground(getResources().getDrawable(R.color.text_font_gray));
        addressesMenu.setBackground(getResources().getDrawable(R.color.colorPrimary));
        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAddress.setBackground(getResources().getDrawable(R.color.colorPrimary));
                addressesMenu.setBackground(getResources().getDrawable(R.color.text_font_gray));
                addressesMenuRecyclerView.setVisibility(View.GONE);
                linAddAddress.setVisibility(View.VISIBLE);
            }
        });
        addressesMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAddress.setBackground(getResources().getDrawable(R.color.text_font_gray));
                addressesMenu.setBackground(getResources().getDrawable(R.color.colorPrimary));
                addressesMenuRecyclerView.setVisibility(View.VISIBLE);
                linAddAddress.setVisibility(View.GONE);
            }
        });
        tvCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countryRecyclerView.getVisibility() == View.VISIBLE) {
                    countryRecyclerView.setVisibility(View.GONE);
                } else {
                    countryRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        tvZones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (zonesRecyclerView.getVisibility() == View.VISIBLE) {
                    zonesRecyclerView.setVisibility(View.GONE);
                } else {
                    zonesRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        rdbtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultAddress = "1";
            }
        });
        rdbtnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultAddress = "0";
            }
        });
        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sharedPrefManager.getLoginStatus()) {
                    if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
                        etFirstName.setError(getResources().getString(R.string.enter_first_name));
                    } else if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
                        etLastName.setError(getResources().getString(R.string.enter_last_name));
                    } else if (TextUtils.isEmpty(etFirstAddress.getText().toString().trim())) {
                        etFirstAddress.setError(getResources().getString(R.string.enter_first_address));
                    } else if (TextUtils.isEmpty(etCity.getText().toString().trim())) {
                        etCity.setError(getResources().getString(R.string.enter_city));
                    } else if (TextUtils.isEmpty(Constants.countryId)) {
                        tvCountry.setError(getResources().getString(R.string.choose_country));
                    } else if (TextUtils.isEmpty(Constants.zoneId)) {
                        tvZones.setError(getResources().getString(R.string.choose_zone));
                    } else {
                        customLoadingDialog.show();
                        AddressesWebService.addNewAddress(sharedPrefManager.getUserData().getCustomerId(), etFirstName.getText().toString(),
                                etLastName.getText().toString(), etCompany.getText().toString(), etFirstAddress.getText().toString(),
                                etSecondAddress.getText().toString(), etCity.getText().toString(), etPostCode.getText().toString(),
                                Constants.countryId, Constants.zoneId, defaultAddress);

                    }
                }else {
                    Utils.logInFirest(AdressesBookActivity.this);
                }
            }
        });
        AddressesWebService.getCountries();
        if (sharedPrefManager.getLoginStatus()){
            AddressesWebService.getAddresses(sharedPrefManager.getUserData().getCustomerId());
            linLoginFirst.setVisibility(View.GONE);
            linAddressesBookData.setVisibility(View.VISIBLE);
            customLoadingDialog.show();
        }else {
            //Utils.logInFirest(AdressesBookActivity.this);
            linLoginFirst.setVisibility(View.VISIBLE);
            linAddressesBookData.setVisibility(View.GONE);
        }
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdressesBookActivity.this,LogInActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_adresses_book;
    }

    private void initView() {
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etCompany = (EditText) findViewById(R.id.et_company);
        etFirstAddress = (EditText) findViewById(R.id.et_first_address);
        etSecondAddress = (EditText) findViewById(R.id.et_second_address);
        etCity = (EditText) findViewById(R.id.et_city);
        etPostCode = (EditText) findViewById(R.id.et_post_code);
        tvCountry = (TextView) findViewById(R.id.tv_country);
        tvZones = (TextView) findViewById(R.id.tv_zones);
        rdbtnYes = (RadioButton) findViewById(R.id.rdbtn_yes);
        rdbtnNo = (RadioButton) findViewById(R.id.rdbtn_no);
        addressesMenu = (TextView) findViewById(R.id.addresses_menu);
        addAddress = (TextView) findViewById(R.id.add_address);
        linAddAddress = (LinearLayout) findViewById(R.id.lin_add_address);
        countryRecyclerView = (RecyclerView) findViewById(R.id.country_recycler_view);
        zonesRecyclerView = (RecyclerView) findViewById(R.id.zones_recycler_view);
        btnAddAddress = (Button) findViewById(R.id.btn_add_address);
        addressesMenuRecyclerView = (RecyclerView) findViewById(R.id.addresses_menu_recycler_view);
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        linAddressesBookData = (LinearLayout) findViewById(R.id.lin_addresses_book_data);
        linLoginFirst = (RelativeLayout) findViewById(R.id.lin_login_first);
        imgLogo = (ImageView) findViewById(R.id.img_logo);
        tvLogin = (TextView) findViewById(R.id.tv_login);
        btnLogIn = (Button) findViewById(R.id.btn_log_in);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPrefManager.getLoginStatus()){
            AddressesWebService.getAddresses(sharedPrefManager.getUserData().getCustomerId());
            linLoginFirst.setVisibility(View.GONE);
            linAddressesBookData.setVisibility(View.VISIBLE);
            customLoadingDialog.show();
        }else {
            //Utils.logInFirest(AdressesBookActivity.this);
            linLoginFirst.setVisibility(View.VISIBLE);
            linAddressesBookData.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CountriesEvent countriesEvent) {
        if (countriesEvent.getCountriesResponseModel().getSuccess()) {
            countriesList = countriesEvent.getCountriesResponseModel().getCountries();

            CountriesAdapter adapter = new CountriesAdapter(AdressesBookActivity.this, countriesList, this);
            countryRecyclerView.setLayoutManager(new LinearLayoutManager(AdressesBookActivity.this));
            countryRecyclerView.setNestedScrollingEnabled(false);
            countryRecyclerView.setAdapter(adapter);
        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CountryZonesEvent countryZonesEvent) {
        if (countryZonesEvent.getCountryZonesResponseModel().getSuccess()) {
            tvZones.setClickable(true);
            countryZonesList = new ArrayList<>();
            countryZonesList = countryZonesEvent.getCountryZonesResponseModel().getZones();
            CountryZonesAdapter adapter = new CountryZonesAdapter(AdressesBookActivity.this, countryZonesList, this);
            zonesRecyclerView.setLayoutManager(new LinearLayoutManager(AdressesBookActivity.this));
            zonesRecyclerView.setNestedScrollingEnabled(false);
            zonesRecyclerView.setAdapter(adapter);
        } else {
            countryZonesList = new ArrayList<>();
            zonesRecyclerView.setVisibility(View.GONE);
            tvZones.setClickable(false);
            tvZones.setText(getResources().getString(R.string.choose_zone));
            Toast.makeText(AdressesBookActivity.this,getResources().getString(R.string.no_zones),Toast.LENGTH_LONG).show();

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GetAddressesEvent getAddressesEvent) {
        if (getAddressesEvent.getGetAddressesResponseModel().getSuccess()) {
            addressesDataList = getAddressesEvent.getGetAddressesResponseModel().getAddressesdata();
            GetAddressesAdapter adapter = new GetAddressesAdapter(AdressesBookActivity.this, addressesDataList,this);
            addressesMenuRecyclerView.setLayoutManager(new LinearLayoutManager(AdressesBookActivity.this));
            addressesMenuRecyclerView.setNestedScrollingEnabled(false);
            addressesMenuRecyclerView.setAdapter(adapter);
            customLoadingDialog.cancel();
        } else {

            customLoadingDialog.cancel();

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AddNNewAddressEvent addNNewAddressEvent) {
        if (addNNewAddressEvent.getAddNewAddressResponseModel().getSuccess()) {
            AddressesWebService.getAddresses(sharedPrefManager.getUserData().getCustomerId());
            customLoadingDialog.cancel();
            countriesList = new ArrayList<>();
            countryZonesList = new ArrayList<>();
            Constants.countryId = "";
            Constants.zoneId = "";
            defaultAddress = "0";
            etCompany.setText("");
            etFirstAddress.setText("");
            etSecondAddress.setText("");
            etCity.setText("");
            etPostCode.setText("");
            linAddAddress.setVisibility(View.GONE);
            addressesMenuRecyclerView.setVisibility(View.VISIBLE);
            addAddress.setBackground(getResources().getDrawable(R.color.text_font_gray));
            addressesMenu.setBackground(getResources().getDrawable(R.color.colorPrimary));
            Toast.makeText(AdressesBookActivity.this,getResources().getString(R.string.saved),Toast.LENGTH_LONG).show();


        } else {


        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(DeleteAddressEvent deleteAddressEvent) {
        if (deleteAddressEvent.getDeleteAddressResponseModel().getSuccess()) {
            AddressesWebService.getAddresses(sharedPrefManager.getUserData().getCustomerId());
            Toast.makeText(AdressesBookActivity.this,getResources().getString(R.string.deleted),Toast.LENGTH_LONG).show();

        } else {


        }
    }
    @Override
    public void onRecyclerItemClicked(int position) {
        if (itemClicked == 1) {
            //countryId = countriesList.get(position).getCountryId();
            tvCountry.setText(Constants.countryName);
            countryRecyclerView.setVisibility(View.GONE);
            AddressesWebService.getCountryZones(Constants.countryId);
        } else if (itemClicked == 2) {
            //zoneId = countryZonesList.get(position).getZoneId();
            tvZones.setText(Constants.zoneName);
            zonesRecyclerView.setVisibility(View.GONE);
        }else if (itemClicked == 3 ){
            final AlertDialog.Builder dialog = new AlertDialog.Builder(AdressesBookActivity.this);
            dialog.setTitle(R.string.app_name);
            dialog.setMessage(R.string.delete_msg);
            dialog.setIcon(R.drawable.logo);
            dialog.setCancelable(true);
            dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    AddressesWebService.deleteAddress(sharedPrefManager.getUserData().getCustomerId(),Constants.addressId);
                }
            });
            dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialog.show().cancel();
                }
            });
            dialog.show();
        }

    }
}
