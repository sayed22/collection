package net.collection.collection.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.CountriesAdapter;
import net.collection.collection.Adapters.CountryZonesAdapter;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Networks.Events.CountriesEvent;
import net.collection.collection.Networks.Events.CountryZonesEvent;
import net.collection.collection.Networks.Events.EditAddressEvent;
import net.collection.collection.Networks.ResponseModels.CountriesResponseModel;
import net.collection.collection.Networks.ResponseModels.CountryZonesResponseModel;
import net.collection.collection.Networks.ResponseModels.GetAddressesResponseModel;
import net.collection.collection.Networks.WebServices.AddressesWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.SharedPrefManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import static net.collection.collection.Utils.Constants.itemClicked;

public class EditAddressActivity extends ParentActivity implements IRecyclerItemClicked {

    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private LinearLayout linAddAddress;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etCompany;
    private EditText etFirstAddress;
    private EditText etSecondAddress;
    private EditText etCity;
    private EditText etPostCode;
    private TextView tvCountry;
    private RecyclerView countryRecyclerView;
    private TextView tvZones;
    private RecyclerView zonesRecyclerView;
    private RadioButton rdbtnYes;
    private RadioButton rdbtnNo;
    private Button btnAddAddress;
    private RecyclerView addressesMenuRecyclerView;
    private List<CountriesResponseModel.Countries> countriesList;
    private List<CountryZonesResponseModel.Zones> countryZonesList = new ArrayList<>();

    String defaultAddress = "0";
    private List<GetAddressesResponseModel.Addressesdata> addressesDataList;
    private SharedPrefManager sharedPrefManager;
    CustomLoadingDialog customLoadingDialog;
    String first_name, last_name, company, first_address, second_address, city,post_code, zone_name, country_name, set_default;
    private String address_id;

    @Override
    protected void initializeComponents() {
        initView();
        address_id = getIntent().getStringExtra("address_id");
        first_name = getIntent().getStringExtra("first_name");
        last_name = getIntent().getStringExtra("last_name");
        company = getIntent().getStringExtra("company");
        first_address = getIntent().getStringExtra("first_address");
        second_address = getIntent().getStringExtra("second_address");
        city = getIntent().getStringExtra("city");
        post_code = getIntent().getStringExtra("post_code");
        zone_name = getIntent().getStringExtra("zone_name");
        country_name = getIntent().getStringExtra("country_name");
        // set_default = getIntent().getStringExtra("set_default");

        etFirstName.setText(first_name);
        etLastName.setText(last_name);
        etCompany.setText(company);
        etFirstAddress.setText(first_address);
        etSecondAddress.setText(second_address);
        etCity.setText(city);
        etPostCode.setText(post_code);
        tvCountry.setText(country_name);
        tvZones.setText(zone_name);

        sharedPrefManager = new SharedPrefManager(getApplicationContext());
        customLoadingDialog = new CustomLoadingDialog(this, R.style.DialogSlideAnim);
        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.edit_address));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countryRecyclerView.getVisibility() == View.VISIBLE) {
                    countryRecyclerView.setVisibility(View.GONE);
                } else {
                    countryRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        tvZones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (zonesRecyclerView.getVisibility() == View.VISIBLE) {
                    zonesRecyclerView.setVisibility(View.GONE);
                } else {
                    zonesRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        rdbtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultAddress = "1";
            }
        });
        rdbtnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultAddress = "0";
            }
        });
        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
                    etFirstName.setError(getResources().getString(R.string.enter_first_name));
                } else if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
                    etLastName.setError(getResources().getString(R.string.enter_last_name));
                } else if (TextUtils.isEmpty(etFirstAddress.getText().toString().trim())) {
                    etFirstAddress.setError(getResources().getString(R.string.enter_first_address));
                } else if (TextUtils.isEmpty(etCity.getText().toString().trim())) {
                    etCity.setError(getResources().getString(R.string.enter_city));
                } else if (TextUtils.isEmpty(Constants.countryId)) {
                    tvCountry.setError(getResources().getString(R.string.choose_country));
                } else if (TextUtils.isEmpty(Constants.zoneId)) {
                    tvZones.setError(getResources().getString(R.string.choose_zone));
                } else {
                    customLoadingDialog.show();
                    AddressesWebService.editAddress(sharedPrefManager.getUserData().getCustomerId(), address_id, etFirstName.getText().toString(),
                            etLastName.getText().toString(), etCompany.getText().toString(), etFirstAddress.getText().toString(),
                            etSecondAddress.getText().toString(), etCity.getText().toString(), etPostCode.getText().toString(),
                            Constants.countryId, Constants.zoneId, defaultAddress);

                }
            }
        });
        AddressesWebService.getCountries();
        AddressesWebService.getAddresses(sharedPrefManager.getUserData().getCustomerId());

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_address;
    }

    private void initView() {
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        linAddAddress = (LinearLayout) findViewById(R.id.lin_add_address);
        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etCompany = (EditText) findViewById(R.id.et_company);
        etFirstAddress = (EditText) findViewById(R.id.et_first_address);
        etSecondAddress = (EditText) findViewById(R.id.et_second_address);
        etCity = (EditText) findViewById(R.id.et_city);
        etPostCode = (EditText) findViewById(R.id.et_post_code);
        tvCountry = (TextView) findViewById(R.id.tv_country);
        countryRecyclerView = (RecyclerView) findViewById(R.id.country_recycler_view);
        tvZones = (TextView) findViewById(R.id.tv_zones);
        zonesRecyclerView = (RecyclerView) findViewById(R.id.zones_recycler_view);
        rdbtnYes = (RadioButton) findViewById(R.id.rdbtn_yes);
        rdbtnNo = (RadioButton) findViewById(R.id.rdbtn_no);
        btnAddAddress = (Button) findViewById(R.id.btn_add_address);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CountriesEvent countriesEvent) {
        if (countriesEvent.getCountriesResponseModel().getSuccess()) {
            countriesList = countriesEvent.getCountriesResponseModel().getCountries();

            CountriesAdapter adapter = new CountriesAdapter(EditAddressActivity.this, countriesList, this);
            countryRecyclerView.setLayoutManager(new LinearLayoutManager(EditAddressActivity.this));
            countryRecyclerView.setNestedScrollingEnabled(false);
            countryRecyclerView.setAdapter(adapter);
        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CountryZonesEvent countryZonesEvent) {
        if (countryZonesEvent.getCountryZonesResponseModel().getSuccess()) {
            tvZones.setClickable(true);
            countryZonesList = new ArrayList<>();
            countryZonesList = countryZonesEvent.getCountryZonesResponseModel().getZones();
            CountryZonesAdapter adapter = new CountryZonesAdapter(EditAddressActivity.this, countryZonesList, this);
            zonesRecyclerView.setLayoutManager(new LinearLayoutManager(EditAddressActivity.this));
            zonesRecyclerView.setNestedScrollingEnabled(false);
            zonesRecyclerView.setAdapter(adapter);
        } else {
            countryZonesList = new ArrayList<>();
            zonesRecyclerView.setVisibility(View.GONE);
            tvZones.setClickable(false);
            tvZones.setText(getResources().getString(R.string.choose_zone));
            Toast.makeText(EditAddressActivity.this,getResources().getString(R.string.no_zones),Toast.LENGTH_LONG).show();

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EditAddressEvent editAddressEvent) {
        if (editAddressEvent.getEditAddressResponseModel().getSuccess()) {
            customLoadingDialog.cancel();
            Toast.makeText(EditAddressActivity.this, getResources().getString(R.string.edited), Toast.LENGTH_LONG).show();
            finish();
        } else {
            customLoadingDialog.cancel();

        }
    }

    @Override
    public void onRecyclerItemClicked(int position) {
        if (itemClicked == 1) {
            tvCountry.setText(Constants.countryName);
            countryRecyclerView.setVisibility(View.GONE);
            AddressesWebService.getCountryZones(Constants.countryId);
        } else if (itemClicked == 2) {
            tvZones.setText(Constants.zoneName);
            zonesRecyclerView.setVisibility(View.GONE);
        }
    }
}
