package net.collection.collection.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.OneOrderProductsAdapter;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Networks.Events.OneOrderEvent;
import net.collection.collection.Networks.ResponseModels.OneOrderResponseModel;
import net.collection.collection.Networks.WebServices.OrdersWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class OneOrderDetailsActivity extends ParentActivity {

    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private TextView tvOrderNo;
    private TextView tvOrderStatus;
    private TextView tvOrderDate;
    private TextView tvProductsNo;
    private TextView tvClient;
    private TextView tvTotal;
    private TextView tvFreeCharge;
    private TextView tvFinalTotal;
    private TextView tvPaymentAddress;
    private TextView tvShippingAddress;
    private RecyclerView orderProductsRecyclerView;
    CustomLoadingDialog customLoadingDialog;
    private List<OneOrderResponseModel.Products> productsList;
    private TextView tvShipingAddress;
    private TextView tvComment;
    private List<OneOrderResponseModel.Historiesdata> historyData;
    String order_id,order_status;
    private LinearLayout linOrderData;

    @Override
    protected void initializeComponents() {
        initView();
        order_id = getIntent().getStringExtra("order_id");
        order_status = getIntent().getStringExtra("order_status");

        customLoadingDialog = new CustomLoadingDialog(this, R.style.DialogSlideAnim);
        customLoadingDialog.show();
        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        tvToolbarTitle.setText(getResources().getString(R.string.order) + " " + order_id);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        OrdersWebService.getOneOrder(order_id);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_one_order_details;
    }

    private void initView() {
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        tvOrderNo = (TextView) findViewById(R.id.tv_order_no);
        tvOrderStatus = (TextView) findViewById(R.id.tv_order_status);
        tvOrderDate = (TextView) findViewById(R.id.tv_order_date);
        tvProductsNo = (TextView) findViewById(R.id.tv_products_no);
        tvClient = (TextView) findViewById(R.id.tv_client);
        tvTotal = (TextView) findViewById(R.id.tv_total);
        tvFreeCharge = (TextView) findViewById(R.id.tv_free_charge);
        tvFinalTotal = (TextView) findViewById(R.id.tv_final_total);
        tvPaymentAddress = (TextView) findViewById(R.id.tv_payment_address);
        tvShippingAddress = (TextView) findViewById(R.id.tv_shiping_address);
        orderProductsRecyclerView = (RecyclerView) findViewById(R.id.order_products_recycler_view);
        tvShipingAddress = (TextView) findViewById(R.id.tv_shiping_address);
        tvComment = (TextView) findViewById(R.id.tv_comment);
        linOrderData = (LinearLayout) findViewById(R.id.lin_order_data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OneOrderEvent oneOrderEvent) {
        if (oneOrderEvent.getOneOrderResponseModel().getSuccess()) {
            productsList = oneOrderEvent.getOneOrderResponseModel().getOrders().getProducts();

            tvOrderNo.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getOrderId());
            tvOrderStatus.setText(order_status);
            tvOrderDate.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getDateAdded());
            Constants.orderDate = oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getDateAdded();
            tvProductsNo.setText(String.valueOf(productsList.size()));
            tvClient.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getFirstname() + " " +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getLastname());
            tvTotal.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getTotal());
//            tvFreeCharge.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getTotals().get(1).getValue());
  //          tvFinalTotal.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getTotals().get(2).getValue());
            tvPaymentAddress.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentFirstname() + " " +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentLastname() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentCompany() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentAddress1() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentAddress2() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentCity() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentZone() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentCountry() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getPaymentMethod() + "\n"
            );

            tvShippingAddress.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingFirstname() + " " +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingLastname() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingCompany() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingAddress1() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingAddress2() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingCity() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingZone() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingCountry() + "\n" +
                    oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getShippingMethod() + "\n");

            tvComment.setText(oneOrderEvent.getOneOrderResponseModel().getOrders().getOrderInfo().getComment());

            OneOrderProductsAdapter adapter = new OneOrderProductsAdapter(OneOrderDetailsActivity.this, productsList);
            orderProductsRecyclerView.setLayoutManager(new LinearLayoutManager(OneOrderDetailsActivity.this));
            orderProductsRecyclerView.setNestedScrollingEnabled(false);
            orderProductsRecyclerView.setAdapter(adapter);
            linOrderData.setVisibility(View.VISIBLE);
            customLoadingDialog.cancel();
        } else {

        }
    }
}
