package net.collection.collection.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.jakewharton.processphoenix.ProcessPhoenix;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.R;
import net.collection.collection.Utils.SharedPrefLanguage;
import net.collection.collection.Utils.Utils;

import java.util.Locale;

public class SettingsActivity extends ParentActivity {

    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private TextView tvEditPersonalData;
    private TextView tvChangePassword;
    private TextView tvAddressesBook;
    private ToggleButton toglNotification;
    private android.widget.LinearLayout lyoutAppBar;

    @Override
    protected void initializeComponents() {
        initView();
        btnBack.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvToolbarTitle.setText(getResources().getString(R.string.settings));
        tvEditPersonalData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPrefManager.getLoginStatus()) {
                    Intent intent = new Intent(SettingsActivity.this, ProfileActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Utils.logInFirest(SettingsActivity.this);
                }
            }
        });

        tvChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sharedPrefManager.getLoginStatus()) {
                    Intent intent = new Intent(SettingsActivity.this, EditPasswordActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Utils.logInFirest(SettingsActivity.this);
                }

            }
        });


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }

    private void initView() {
        btnBack = (ImageView) findViewById(R.id.btn_back);
        btnSideBar = (ImageView) findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) findViewById(R.id.btn_menu);
        btnSettings = (ImageView) findViewById(R.id.btn_settings);
        tvEditPersonalData = (TextView) findViewById(R.id.tv_edit_personal_data);
        tvChangePassword = (TextView) findViewById(R.id.tv_change_password);
        tvAddressesBook = (TextView) findViewById(R.id.tv_addresses_book);
        toglNotification = (ToggleButton) findViewById(R.id.togl_notification);
        lyoutAppBar = (LinearLayout) findViewById(R.id.lyout_app_bar);

    }
}
