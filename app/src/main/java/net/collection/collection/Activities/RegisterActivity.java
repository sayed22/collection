package net.collection.collection.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.rilixtech.CountryCodePicker;

import net.collection.collection.Activities.Parent.ParentActivity;
import net.collection.collection.Adapters.CountriesAdapter;
import net.collection.collection.Adapters.CountryZonesAdapter;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Models.Userdata;
import net.collection.collection.Networks.Events.CountriesEvent;
import net.collection.collection.Networks.Events.CountryZonesEvent;
import net.collection.collection.Networks.Events.RegisterEvent;
import net.collection.collection.Networks.ResponseModels.CountriesResponseModel;
import net.collection.collection.Networks.ResponseModels.CountryZonesResponseModel;
import net.collection.collection.Networks.WebServices.AddressesWebService;
import net.collection.collection.Networks.WebServices.UserWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.ViewsUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.collection.collection.Utils.Constants.itemClicked;

public class RegisterActivity extends ParentActivity implements IRecyclerItemClicked {

    private EditText etFirstName;
    private EditText etLastName;
    private EditText etPhoneNumber;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etConfirmPassword;
    private Button btnNewUser;
    private Button btnLogIn;
    private ScrollView mScrollView;
    private CountryCodePicker ccp;
    private String phone;
    private android.widget.LinearLayout container;
    private EditText etCompany;
    private EditText etFirstAddress;
    private EditText etSecondAddress;
    private EditText etCity;
    private EditText etPostCode;
    private android.widget.TextView tvCountry;
    private android.support.v7.widget.RecyclerView countryRecyclerView;
    private android.widget.TextView tvZones;
    private android.support.v7.widget.RecyclerView zonesRecyclerView;
    private List<CountriesResponseModel.Countries> countriesList;
    private List<CountryZonesResponseModel.Zones> countryZonesList = new ArrayList<>();

    @Override
    protected void initializeComponents() {
        initView();
        AddressesWebService.getCountries();

        tvCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countryRecyclerView.getVisibility() == View.VISIBLE) {
                    countryRecyclerView.setVisibility(View.GONE);
                } else {
                    countryRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        tvZones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (zonesRecyclerView.getVisibility() == View.VISIBLE) {
                    zonesRecyclerView.setVisibility(View.GONE);
                } else {
                    zonesRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });



        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LogInActivity.class);

                Bundle bndlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                startActivity(intent, bndlanimation);
            }
        });
        btnNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ccp.registerPhoneNumberTextView(etPhoneNumber);
                phone = ccp.getFullNumberWithPlus();

                // phone = String.format(phone, PhoneNumberUtil.PhoneNumberFormat.E164);
                if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
                    etFirstName.setError(getResources().getString(R.string.enter_first_name));
                } else if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
                    etLastName.setError(getResources().getString(R.string.enter_last_name));
                } else if (TextUtils.isEmpty(etPhoneNumber.getText().toString().trim())) {
                    etPhoneNumber.setError(getResources().getString(R.string.enter_phone_number));
                } else if (etPhoneNumber.length() < 8) {
                    etPhoneNumber.setError(getResources().getString(R.string.wrong_phone_number));
                } else if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    etEmail.setError(getResources().getString(R.string.enter_email));
                } else if (!emailValidator(etEmail.getText().toString())) {
                    etEmail.setError(getResources().getString(R.string.wrong_email));
                } else if (TextUtils.isEmpty(etFirstAddress.getText().toString().trim())) {
                    etFirstAddress.setError(getResources().getString(R.string.enter_first_address));
                } else if (TextUtils.isEmpty(etCity.getText().toString().trim())) {
                    etCity.setError(getResources().getString(R.string.enter_city));
                } else if (TextUtils.isEmpty(Constants.countryId)) {
                    tvCountry.setError(getResources().getString(R.string.choose_country));
                } else if (TextUtils.isEmpty(Constants.zoneId)) {
                    tvZones.setError(getResources().getString(R.string.choose_zone));
                } else if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                    etPassword.setError(getResources().getString(R.string.enter_password));
                } else if (etPassword.length()<6) {
                    etPassword.setError(getResources().getString(R.string.short_password));
                } else if (TextUtils.isEmpty(etConfirmPassword.getText().toString().trim())) {
                    etConfirmPassword.setError(getResources().getString(R.string.enter_confirm_password));
                } else if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
                    etConfirmPassword.setError(getResources().getString(R.string.password_not_match));
                } else {
//                    String frist_name = etFirstName.getText().toString();
//                    String last_name = etLastName.getText().toString();
//                    String email = etEmail.getText().toString();
//                    String phone = etPhoneNumber.getText().toString();
//                    String pass = etPassword.getText().toString();
                    UserWebService.registerUser("1", etFirstName.getText().toString(), etLastName.getText().toString(),
                            etEmail.getText().toString(), phone,
                            "012345", etCompany.getText().toString(), etFirstAddress.getText().toString(), etSecondAddress.getText().toString(),
                            etCity.getText().toString(), etPostCode.getText().toString(), Constants.countryId, Constants.zoneId, etPassword.getText().toString(), "1", "1");
                    customLoadingDialog.show();
                }
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }


    private void initView() {
        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etPhoneNumber = (EditText) findViewById(R.id.et_phone_number);
        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_password);
        etConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);
        btnNewUser = (Button) findViewById(R.id.btn_new_user);
        btnLogIn = (Button) findViewById(R.id.btn_log_in);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);

        container = (LinearLayout) findViewById(R.id.container);
        etCompany = (EditText) findViewById(R.id.et_company);
        etFirstAddress = (EditText) findViewById(R.id.et_first_address);
        etSecondAddress = (EditText) findViewById(R.id.et_second_address);
        etCity = (EditText) findViewById(R.id.et_city);
        etPostCode = (EditText) findViewById(R.id.et_post_code);
        tvCountry = (TextView) findViewById(R.id.tv_country);
        countryRecyclerView = (RecyclerView) findViewById(R.id.country_recycler_view);
        tvZones = (TextView) findViewById(R.id.tv_zones);
        zonesRecyclerView = (RecyclerView) findViewById(R.id.zones_recycler_view);
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserRegistration(RegisterEvent registerEvent) {
        if (registerEvent.getRegisterResponseModel().getSuccess() && !registerEvent.getRegisterResponseModel().getUserdata().equals("Email Alredy Exist")) {
            Userdata userdata = registerEvent.getRegisterResponseModel().getUserdata();
            sharedPrefManager.setUserData(userdata);
            sharedPrefManager.setLoginStatus(true);
            Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
            Bundle bndlanimation =
                    ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
            startActivity(intent, bndlanimation);
            finish();
            customLoadingDialog.cancel();

        } else {
            ViewsUtils.showSnackBarWithLong(context, etEmail, getString(R.string.email_already_exists), R.color.white);
            customLoadingDialog.dismiss();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CountriesEvent countriesEvent) {
        if (countriesEvent.getCountriesResponseModel().getSuccess()) {
            countriesList = countriesEvent.getCountriesResponseModel().getCountries();

            CountriesAdapter adapter = new CountriesAdapter(RegisterActivity.this, countriesList, this);
            countryRecyclerView.setLayoutManager(new LinearLayoutManager(RegisterActivity.this));
            countryRecyclerView.setNestedScrollingEnabled(false);
            countryRecyclerView.setAdapter(adapter);
        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CountryZonesEvent countryZonesEvent) {
        if (countryZonesEvent.getCountryZonesResponseModel().getSuccess()) {
            tvZones.setClickable(true);
            countryZonesList = new ArrayList<>();
            countryZonesList = countryZonesEvent.getCountryZonesResponseModel().getZones();
            CountryZonesAdapter adapter = new CountryZonesAdapter(RegisterActivity.this, countryZonesList, this);
            zonesRecyclerView.setLayoutManager(new LinearLayoutManager(RegisterActivity.this));
            zonesRecyclerView.setNestedScrollingEnabled(false);
            zonesRecyclerView.setAdapter(adapter);
        } else {
            countryZonesList = new ArrayList<>();
            zonesRecyclerView.setVisibility(View.GONE);
            tvZones.setClickable(false);
            tvZones.setText(getResources().getString(R.string.choose_zone));
            Toast.makeText(RegisterActivity.this,getResources().getString(R.string.no_zones),Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onRecyclerItemClicked(int position) {
        if (itemClicked == 1) {
            //countryId = countriesList.get(position).getCountryId();
            tvCountry.setText(Constants.countryName);
            countryRecyclerView.setVisibility(View.GONE);
            AddressesWebService.getCountryZones(Constants.countryId);
        } else if (itemClicked == 2) {
            //zoneId = countryZonesList.get(position).getZoneId();
            tvZones.setText(Constants.zoneName);
            zonesRecyclerView.setVisibility(View.GONE);
        }
    }
}
