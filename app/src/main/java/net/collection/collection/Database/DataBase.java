package net.collection.collection.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper {
    public static String dbName = "CollectionDb";
    public static int version = 1;

    public static String MY_WISH_LIST_TABLE = "my_wish_list";
    public static String MY_CART_TABLE = "my_cart";

    public static String COLUMN_ID = "column_id";
    public static String ID_COL = "id";
    public static String NAME_COL = "name";
    public static String PRICE_COL = "price";
    public static String IMAGE_COL = "image";
    public static String MODEL_COL = "model";
    public static String QUANTITY_COL = "quantity";
    public static String OPTION_COL = "option";
    public static String DOWNLOAD_COL = "download";
    public static String SUBSTRACT_COL = "substract";
    public static String TOTAL_COL = "total";
    public static String TAX_COL = "tax";
    public static String REWARD_COL = "reward";

    public String CREATE_MY_WISH_LIST_TABLE = "CREATE TABLE " + MY_WISH_LIST_TABLE + " ( " +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            ID_COL + " TEXT, " +
            NAME_COL + " TEXT, " +
            PRICE_COL + " TEXT, " +
            IMAGE_COL + " TEXT "+
             ");";


    public String CREATE_MY_CART_TABLE = "CREATE TABLE " + MY_CART_TABLE + " ( " +
            ID_COL + " TEXT, " +
            NAME_COL + " TEXT, " +
            MODEL_COL + " TEXT, " +
            OPTION_COL + " TEXT, " +
            DOWNLOAD_COL + " TEXT, " +
            QUANTITY_COL + " TEXT, " +
            SUBSTRACT_COL + " TEXT, " +
            PRICE_COL + " TEXT, " +
            TOTAL_COL + " TEXT, " +
            TAX_COL + " TEXT, " +
            REWARD_COL + " TEXT, " +
            IMAGE_COL + " TEXT " +
            ");";

    public DataBase(Context context) {
        super(context, dbName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_MY_WISH_LIST_TABLE);
        sqLiteDatabase.execSQL(CREATE_MY_CART_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }

    public void insertIntoMyWishList(DataBase db, String id, String name, String price, String image) {
        SQLiteDatabase sdb = db.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ID_COL, id);
        cv.put(NAME_COL, name);
        cv.put(PRICE_COL, price);
        cv.put(IMAGE_COL, image);
        sdb.insert(MY_WISH_LIST_TABLE, null, cv);
    }

    public void insertIntoMyCart(DataBase db, String id, String name, String model, String option,
                                 String download, String quantity, String substract,
                                 String price, String total, String tax, String reward, String image) {
        SQLiteDatabase sdb = db.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ID_COL, id);
        cv.put(NAME_COL, name);
        cv.put(MODEL_COL, model);
        cv.put(OPTION_COL, String.valueOf(option));
        cv.put(DOWNLOAD_COL, download);
        cv.put(QUANTITY_COL, quantity);
        cv.put(SUBSTRACT_COL, substract);
        cv.put(PRICE_COL, price);
        cv.put(TOTAL_COL, total);
        cv.put(TAX_COL, tax);
        cv.put(REWARD_COL, reward);
        cv.put(IMAGE_COL, image);

        sdb.insert(MY_CART_TABLE, null, cv);
    }

    public void updateSellInTable(DataBase db,String tableName, String id, String colName, String value) {
        SQLiteDatabase sdb = db.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(colName, value);
        sdb.update(tableName, cv, ID_COL + "=" + id, null);
    }

    // General Methods For All Tables
    public Cursor getDataFromTable(DataBase db, String tableName) {
        SQLiteDatabase sdb = db.getReadableDatabase();
        Cursor cr = sdb.rawQuery("select * from " + tableName, null);
        return cr;
    }

    public boolean deleteSingleRow(DataBase db, String tableName, String id) {
        SQLiteDatabase sdb = db.getWritableDatabase();

        return sdb.delete(tableName, ID_COL + "=" + id, null) > 0;
    }

    public void dropTable(DataBase db, String tableName) {
        SQLiteDatabase sdb = db.getWritableDatabase();

        sdb.execSQL("DROP TABLE IF EXISTS " + tableName + ";");
        onCreate(sdb);
    }

    public void deletAllDataFromTble(DataBase db, String tableName) {
        SQLiteDatabase sdb = db.getWritableDatabase();
        sdb.execSQL("DELETE FROM " + tableName);
    }

    public int getTableRowsCount(String tableName) {
        String countQuery = "SELECT  * FROM " + tableName;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

}
