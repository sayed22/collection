package net.collection.collection.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.collection.collection.Adapters.GetAddressesPaymentAdapter;
import net.collection.collection.Adapters.GetAddressesShippingAdapter;
import net.collection.collection.Fragments.CartFragment;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Networks.Events.GetAddressesEvent;
import net.collection.collection.Networks.ResponseModels.GetAddressesResponseModel;
import net.collection.collection.Networks.WebServices.AddressesWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.DetectConnection;
import net.collection.collection.Utils.SharedPrefManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class CustomAddressesDialog extends Dialog {

    Context mContext;
    private TextView tvChoosePaymentAddress;
    public static RecyclerView addressesPaymentRecyclerView;
    private TextView tvChooseShippingAddress;
    public static RecyclerView addressesShippingRecyclerView;
    private List<GetAddressesResponseModel.Addressesdata> addressesDataList;
    private Button btnConfirm;

    public CustomAddressesDialog(Context mContext, int themeResId) {
        super(mContext);
        this.mContext = mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.custom_loading_background));
        setContentView(R.layout.custom_addresses_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        // getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);

        initView();
        SharedPrefManager sharedPrefManager = new SharedPrefManager(mContext);
        AddressesWebService.getAddresses(sharedPrefManager.getUserData().getCustomerId());

        tvChoosePaymentAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addressesPaymentRecyclerView.getVisibility() == View.VISIBLE) {
                    addressesPaymentRecyclerView.setVisibility(View.GONE);
                } else {
                    addressesPaymentRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        tvChooseShippingAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addressesShippingRecyclerView.getVisibility() == View.VISIBLE) {
                    addressesShippingRecyclerView.setVisibility(View.GONE);
                } else {
                    addressesShippingRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });

        if(!DetectConnection.isInternetAvailable(mContext)) {
            Toast.makeText(mContext.getApplicationContext(), mContext.getResources().getString(R.string.check_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void initView() {
        tvChoosePaymentAddress = (TextView) findViewById(R.id.tv_choose_payment_address);
        addressesPaymentRecyclerView = (RecyclerView) findViewById(R.id.addresses_payment_recycler_view);
        tvChooseShippingAddress = (TextView) findViewById(R.id.tv_choose_shipping_address);
        addressesShippingRecyclerView = (RecyclerView) findViewById(R.id.addresses_shipping_recycler_view);
        btnConfirm = (Button) findViewById(R.id.btn_confirm);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(GetAddressesEvent getAddressesEvent) {
        if (getAddressesEvent.getGetAddressesResponseModel().getSuccess()) {
            addressesDataList = getAddressesEvent.getGetAddressesResponseModel().getAddressesdata();

            GetAddressesPaymentAdapter adapter = new GetAddressesPaymentAdapter(mContext, addressesDataList);
            addressesPaymentRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            addressesPaymentRecyclerView.setNestedScrollingEnabled(false);
            addressesPaymentRecyclerView.setAdapter(adapter);

            GetAddressesShippingAdapter adapter2 = new GetAddressesShippingAdapter(mContext, addressesDataList);
            addressesShippingRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            addressesShippingRecyclerView.setNestedScrollingEnabled(false);
            addressesShippingRecyclerView.setAdapter(adapter2);
        } else {

        }
    }
}
