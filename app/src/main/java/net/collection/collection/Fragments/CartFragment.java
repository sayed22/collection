package net.collection.collection.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.collection.collection.Activities.AdressesBookActivity;
import net.collection.collection.Activities.OneOrderDetailsActivity;
import net.collection.collection.Adapters.CartProductsAdapter;
import net.collection.collection.Adapters.PaymentMethodAdapter;
import net.collection.collection.Adapters.ShippingMethodAdapter;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Dialogs.CustomAddressesDialog;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Interface.IRecyclerItemClicked;
import net.collection.collection.Models.AddOrderModuleBody;
import net.collection.collection.Models.CartMoudel;
import net.collection.collection.Models.MyCartModel;
import net.collection.collection.Models.OptionList;
import net.collection.collection.Networks.Events.AddOrderEvent;
import net.collection.collection.Networks.Events.PaymentMethodEvent;
import net.collection.collection.Networks.Events.ShippingMethodEvent;
import net.collection.collection.Networks.ResponseModels.CartProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.CountriesResponseModel;
import net.collection.collection.Networks.ResponseModels.CountryZonesResponseModel;
import net.collection.collection.Networks.ResponseModels.PaymentMethodResponseModel;
import net.collection.collection.Networks.ResponseModels.ShippingMethodResponseModel;
import net.collection.collection.Networks.WebServices.AddressesWebService;
import net.collection.collection.Networks.WebServices.CartWebService;
import net.collection.collection.Networks.WebServices.OrdersWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Constants;
import net.collection.collection.Utils.SharedPrefManager;
import net.collection.collection.Utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static net.collection.collection.Database.DataBase.DOWNLOAD_COL;
import static net.collection.collection.Database.DataBase.ID_COL;
import static net.collection.collection.Database.DataBase.IMAGE_COL;
import static net.collection.collection.Database.DataBase.MODEL_COL;
import static net.collection.collection.Database.DataBase.NAME_COL;
import static net.collection.collection.Database.DataBase.OPTION_COL;
import static net.collection.collection.Database.DataBase.PRICE_COL;
import static net.collection.collection.Database.DataBase.QUANTITY_COL;
import static net.collection.collection.Database.DataBase.REWARD_COL;
import static net.collection.collection.Database.DataBase.SUBSTRACT_COL;
import static net.collection.collection.Database.DataBase.TAX_COL;
import static net.collection.collection.Database.DataBase.TOTAL_COL;
import static net.collection.collection.Utils.Constants.itemClicked;

public class CartFragment extends Fragment implements IRecyclerItemClicked {

    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    public static RecyclerView shopRecyclerView;
    private TextView tvCountry;
    private RecyclerView countryRecyclerView;
    private TextView tvZones;
    private RecyclerView zonesRecyclerView;
    private EditText etExpectedShipingPostcode;
    public static TextView tvTotal;
    public static TextView tvFinalTotal;
    private Button sendRequest;
    private List<CartProductsResponseModel> cartProductList;
    private List<CountriesResponseModel.Countries> countriesList;
    private List<CountryZonesResponseModel.Zones> countryZonesList = new ArrayList<>();
    private SharedPrefManager sharedPrefManager;
    public static ArrayList<MyCartModel> cartMoudels;
    public ArrayList<CartMoudel> moudel = new ArrayList<>();
    Gson gson = new Gson();
    private CustomLoadingDialog customLoadingDialog;
    public static double total = 0;
    public static CartProductsAdapter cartdapter;
    private static LinearLayout linCart;
    private LinearLayout linTotal;
    private static RelativeLayout linNoDataFound;
    private ImageView imgLogo;
    public static String _PaymentFirstname, _PaymentLastname, _PaymentCompany, _PaymentAddress1, _PaymentAddress2, _PaymentCity,
            _PaymentPostcode, _PaymentZone, _PaymentZoneId, _PaymentCountry, _PaymentCountryId, _PaymentAddressFormat,
            _PaymentMethod, _PaymentCode;
    public static String _ShippingFirstname, _ShippingLastname, _ShippingCompany, _ShippingAddress1, _ShippingAddress2, _ShippingCity,
            _ShippingPostcode, _ShippingZone, _ShippingZoneId, _ShippingCountry, _ShippingCountryId, _ShippingAddressFormat,
            _ShippingMethod, _ShippingCode;

    public static int paymentAddressChoosed = 0;
    public static int shippingAddressChoosed = 0;
    private CustomAddressesDialog customAddressesDialog;
    public static DataBase dbTotal;
    private TextView addAddress;
    private TextView tvPaymentMethod;
    private RecyclerView paymentMethodRecyclerView;
    private TextView tvShippingMethod;
    private RecyclerView shippingMethodRecyclerView;
    public static TextView tvShipping;
    public static TextView tvShippingCost;
    private List<PaymentMethodResponseModel.Payment> paymentMethodsList;
    private List<ShippingMethodResponseModel.Shipping> shippingMethodsList;
    public static String shippingCode = "";
    public static String shippingTitle = "";
    public static String shippingValue = "";
    public static String shippingSort_order = "";
    public static String paymentCode = "";
    public static String paymentTitle = "";
    public static String paymentSort_order = "";
    public static LinearLayout linShipping;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cart_fragment, container, false);
        lyoutAppBar = (LinearLayout) view.findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) view.findViewById(R.id.btn_back);
        btnSideBar = (ImageView) view.findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) view.findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) view.findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) view.findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) view.findViewById(R.id.btn_menu);
        btnSettings = (ImageView) view.findViewById(R.id.btn_settings);
        shopRecyclerView = (RecyclerView) view.findViewById(R.id.shop_recycler_view);

        tvCountry = (TextView) view.findViewById(R.id.tv_country);
        countryRecyclerView = (RecyclerView) view.findViewById(R.id.country_recycler_view);
        tvZones = (TextView) view.findViewById(R.id.tv_zones);
        zonesRecyclerView = (RecyclerView) view.findViewById(R.id.zones_recycler_view);
        tvTotal = (TextView) view.findViewById(R.id.tv_total);
        tvFinalTotal = (TextView) view.findViewById(R.id.tv_final_total);
        sendRequest = (Button) view.findViewById(R.id.send_request);
        addAddress = (TextView) view.findViewById(R.id.add_address);

        linCart = (LinearLayout) view.findViewById(R.id.lin_cart);
        linNoDataFound = (RelativeLayout) view.findViewById(R.id.lin_no_data_found);

        tvPaymentMethod = (TextView) view.findViewById(R.id.tv_payment_method);
        paymentMethodRecyclerView = (RecyclerView) view.findViewById(R.id.payment_method_recycler_view);
        tvShippingMethod = (TextView) view.findViewById(R.id.tv_shipping_method);
        shippingMethodRecyclerView = (RecyclerView) view.findViewById(R.id.shipping_method_recycler_view);
        linShipping = (LinearLayout) view.findViewById(R.id.lin_shipping);
        tvShipping = (TextView) view.findViewById(R.id.tv_shipping);
        tvShippingCost = (TextView) view.findViewById(R.id.tv_shipping_cost);
        customLoadingDialog = new CustomLoadingDialog(getActivity(), R.style.DialogSlideAnim);

        sharedPrefManager = new SharedPrefManager(getActivity());
        AddressesWebService.getCountries();
        CartWebService.getPaymentMethods();
        CartWebService.getShippingMethods();
        customAddressesDialog = new CustomAddressesDialog(getActivity(), R.style.DialogSlideAnim);

        dbTotal = new DataBase(getActivity());
        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AdressesBookActivity.class);
                startActivity(intent);
            }
        });
        tvPaymentMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (paymentMethodRecyclerView.getVisibility() == View.VISIBLE) {
                    paymentMethodRecyclerView.setVisibility(View.GONE);
                } else {
                    paymentMethodRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        tvShippingMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shippingMethodRecyclerView.getVisibility() == View.VISIBLE) {
                    shippingMethodRecyclerView.setVisibility(View.GONE);
                } else {
                    shippingMethodRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
        sendRequest.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                if (sharedPrefManager.getLoginStatus()) {

                    if (shippingCode.equals("")) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.choose_shipping_method), Toast.LENGTH_LONG).show();
                    } else if (paymentCode.equals("")) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.choose_payment_method), Toast.LENGTH_LONG).show();
                    } else if (paymentAddressChoosed == 0) {
                        customAddressesDialog.show();
                        CustomAddressesDialog.addressesPaymentRecyclerView.setVisibility(View.VISIBLE);
                    } else if (shippingAddressChoosed == 0) {
                        customAddressesDialog.show();
                        CustomAddressesDialog.addressesShippingRecyclerView.setVisibility(View.VISIBLE);
                    } else {

                        AddOrderModuleBody addOrderModuleBody = new AddOrderModuleBody();
                        List<String> vr = new ArrayList<>();

                        List<AddOrderModuleBody.Totals> listTotals = new ArrayList<>();
                        AddOrderModuleBody.Totals totalModel = new AddOrderModuleBody.Totals();
                        AddOrderModuleBody.Totals shippngModel = new AddOrderModuleBody.Totals();
                        AddOrderModuleBody.Totals finalTotalModel = new AddOrderModuleBody.Totals();
                        totalModel.setCode("sub_total");
                        totalModel.setTitle("Sub-Total");
                        totalModel.setValue(Double.parseDouble(tvTotal.getText().toString()));
                        totalModel.setSortOrder("1");

                        shippngModel.setCode(shippingCode);
                        shippngModel.setTitle(shippingTitle);
                        shippngModel.setValue(Double.parseDouble(shippingValue));
                        shippngModel.setSortOrder(shippingSort_order);

                        finalTotalModel.setCode("total");
                        finalTotalModel.setTitle("Total");
                        finalTotalModel.setValue(Double.parseDouble(shippingValue) + Double.parseDouble(tvTotal.getText().toString()));
                        finalTotalModel.setSortOrder("9");

                        listTotals.add(totalModel);
                        listTotals.add(shippngModel);
                        listTotals.add(finalTotalModel);

                        addOrderModuleBody.setTotals(listTotals);
                        addOrderModuleBody.setStoreId(0);
                        addOrderModuleBody.setStoreName("http://www.collection-kw.com");
                        addOrderModuleBody.setStoreUrl("http://www.collection-kw.com");
                        addOrderModuleBody.setCustomerId(sharedPrefManager.getUserData().getCustomerId());
                        addOrderModuleBody.setCustomerGroupId(sharedPrefManager.getUserData().getCustomerGroupId());
                        addOrderModuleBody.setFirstname(sharedPrefManager.getUserData().getFirstname());
                        addOrderModuleBody.setLastname(sharedPrefManager.getUserData().getLastname());
                        addOrderModuleBody.setEmail(sharedPrefManager.getUserData().getEmail());
                        addOrderModuleBody.setTelephone(sharedPrefManager.getUserData().getTelephone());
                        addOrderModuleBody.setFax(sharedPrefManager.getUserData().getFax());
                        addOrderModuleBody.setCustomField(false);
                        addOrderModuleBody.setPaymentFirstname(_PaymentFirstname);
                        addOrderModuleBody.setPaymentLastname(_PaymentLastname);
                        addOrderModuleBody.setPaymentCompany(_PaymentCompany);
                        addOrderModuleBody.setPaymentAddress1(_PaymentAddress1);
                        addOrderModuleBody.setPaymentAddress2(_PaymentAddress2);
                        addOrderModuleBody.setPaymentCity(_PaymentCity);
                        addOrderModuleBody.setPaymentPostcode(_PaymentPostcode);
                        addOrderModuleBody.setPaymentZone(_PaymentZone);
                        addOrderModuleBody.setPaymentZoneId(_PaymentZoneId);
                        addOrderModuleBody.setPaymentCountry(_PaymentCountry);
                        addOrderModuleBody.setPaymentCountryId(_PaymentCountryId);
                        addOrderModuleBody.setPaymentAddressFormat("");
                        addOrderModuleBody.setPaymentCustomField(false);
                        addOrderModuleBody.setPaymentMethod(paymentTitle);
                        addOrderModuleBody.setPaymentCode(paymentCode);
                        addOrderModuleBody.setShippingFirstname(_ShippingFirstname);
                        addOrderModuleBody.setShippingLastname(_ShippingLastname);
                        addOrderModuleBody.setShippingCompany(_ShippingCompany);
                        addOrderModuleBody.setShippingAddress1(_ShippingAddress1);
                        addOrderModuleBody.setShippingAddress2(_ShippingAddress2);
                        addOrderModuleBody.setShippingCity(_ShippingCity);
                        addOrderModuleBody.setShippingPostcode(_ShippingPostcode);
                        addOrderModuleBody.setShippingZone(_ShippingZone);
                        addOrderModuleBody.setShippingZoneId(_ShippingZoneId);
                        addOrderModuleBody.setShippingCountry(_ShippingCountry);
                        addOrderModuleBody.setShippingCountryId(_ShippingCountryId);
                        addOrderModuleBody.setShippingAddressFormat("");
                        addOrderModuleBody.setShippingCustomField(false);
                        addOrderModuleBody.setShippingMethod(shippingTitle);
                        addOrderModuleBody.setShippingCode(shippingCode);
                        addOrderModuleBody.setVouchers(vr);
                        addOrderModuleBody.setComment("");


                        addOrderModuleBody.setTotal(Double.parseDouble(tvFinalTotal.getText().toString()));
                        addOrderModuleBody.setAffiliateId(0);
                        addOrderModuleBody.setCommission(0);
                        addOrderModuleBody.setMarketingId(0);
                        addOrderModuleBody.setTracking("");
                        addOrderModuleBody.setLanguageId("2");
                        addOrderModuleBody.setCurrencyId("4");
                        addOrderModuleBody.setCurrencyCode("KD");
                        addOrderModuleBody.setCurrencyValue("1.00000000");
                        addOrderModuleBody.setIp("");
                        addOrderModuleBody.setForwardedIp("");
                        addOrderModuleBody.setUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
                        addOrderModuleBody.setAcceptLanguage("en-US,en;q=0.9,ar;q=0.8,fr;q=0.7,ja;q=0.6");
                        addOrderModuleBody.setorder_status_id("1");
                        for (int i = 0; i < cartMoudels.size(); i++) {
                            CartMoudel cartMoudel = new CartMoudel();
                            cartMoudel.setProductId(cartMoudels.get(i).getId());
                            cartMoudel.setName(cartMoudels.get(i).getName());
                            cartMoudel.setModel(cartMoudels.get(i).getModel());
                            if (cartMoudels.get(i).getReward() != null && cartMoudels.get(i).getReward() != "") {
                                long reward = Long.parseLong(cartMoudels.get(i).getReward() + 0);
                                cartMoudel.setReward(reward);
                            } else {
                                long reward = 0;
                                cartMoudel.setReward(reward);
                            }
                            List<Object> optList = new ArrayList<>();
                            if (cartMoudels.get(i).getOption() != null && cartMoudels.get(i).getOption().equals("")) {

                                Type listType = new TypeToken<ArrayList<OptionList>>() {
                                }.getType();
                                List<OptionList> model = (List<OptionList>) gson.fromJson(cartMoudels.get(i).getOption(), listType);
                                for (OptionList model1 : model) {
                                    System.out.println("" + model1.getName());
                                    optList.add(model1);
                                }
//                                OptionList model1;
//                                for (int m = 0; m < model.size(); m++) {
//                                    model1 = model.get(m);
//                                    optList.add(model1);
//
//                                }

                            }
                            cartMoudel.setOption(optList);

                            cartMoudel.setQuantity(Double.parseDouble(cartMoudels.get(i).getQuantity()));
                            String price = cartMoudels.get(i).getPrice();
                            price = price.replaceAll("KD  ", "");
                            price = price.trim();
                            System.out.println("price : " + price);
                            //price = price.replaceAll("[^\\d-]", "");
                            cartMoudel.setPrice(Double.parseDouble(price));
                            double total = Double.parseDouble(price) * Double.parseDouble(cartMoudels.get(i).getQuantity());
                            cartMoudel.setTotal(total);
                            cartMoudel.setImage(cartMoudels.get(i).getImage());

                            moudel.add(cartMoudel);
                        }
                        addOrderModuleBody.setProducts(moudel);
                        Gson gson2 = new Gson();
                        try {
                            String json2 = gson2.toJson(addOrderModuleBody);
                            JSONObject mainObject = new JSONObject(json2);
                            OrdersWebService.addOrder(mainObject);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } else {
                    Utils.logInFirest(getActivity());
                }

            }
        });
        getMyCartData();
        double final_total = Double.parseDouble(tvTotal.getText().toString()) + Double.parseDouble(tvShippingCost.getText().toString());
        tvFinalTotal.setText(String.valueOf(final_total));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AddOrderEvent addOrderEvent) {
        if (addOrderEvent.getAddOrderResponseModel().getSuccess()) {
            DataBase db = new DataBase(getActivity());
            db.deletAllDataFromTble(db, DataBase.MY_CART_TABLE);
            db.close();
            getMyCartData();
            Toast.makeText(getActivity(), getResources().getString(R.string.order_added), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getActivity(), OneOrderDetailsActivity.class);
            intent.putExtra("order_id", addOrderEvent.getAddOrderResponseModel().getOrder().getOrderId());
            getActivity().startActivity(intent);
        } else {

        }
    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(CountriesEvent countriesEvent) {
//        if (countriesEvent.getCountriesResponseModel().getSuccess()) {
//            countriesList = countriesEvent.getCountriesResponseModel().getCountries();
//
//            CountriesAdapter adapter = new CountriesAdapter(getActivity(), countriesList, this);
//            countryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//            countryRecyclerView.setNestedScrollingEnabled(false);
//            countryRecyclerView.setAdapter(adapter);
//        } else {
//
//        }
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PaymentMethodEvent paymentMethodEvent) {
        if (paymentMethodEvent.getPaymentMethodResponseModel().getSuccess()) {
            paymentMethodsList = paymentMethodEvent.getPaymentMethodResponseModel().getPayment();

            PaymentMethodAdapter adapter = new PaymentMethodAdapter(getActivity(), paymentMethodsList);
            paymentMethodRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            paymentMethodRecyclerView.setNestedScrollingEnabled(false);
            paymentMethodRecyclerView.setAdapter(adapter);
        } else {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ShippingMethodEvent shippingMethodEvent) {
        if (shippingMethodEvent.getShippingMethodResponseModel().getSuccess()) {
            shippingMethodsList = shippingMethodEvent.getShippingMethodResponseModel().getShipping();

            ShippingMethodAdapter adapter = new ShippingMethodAdapter(getActivity(), shippingMethodsList);
            shippingMethodRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            shippingMethodRecyclerView.setNestedScrollingEnabled(false);
            shippingMethodRecyclerView.setAdapter(adapter);
        } else {

        }
    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(CountryZonesEvent countryZonesEvent) {
//        if (countryZonesEvent.getCountryZonesResponseModel().getSuccess()) {
//            tvZones.setClickable(true);
//            countryZonesList = new ArrayList<>();
//            countryZonesList = countryZonesEvent.getCountryZonesResponseModel().getZones();
//            CountryZonesAdapter adapter = new CountryZonesAdapter(getActivity(), countryZonesList, this);
//            zonesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//            zonesRecyclerView.setNestedScrollingEnabled(false);
//            zonesRecyclerView.setAdapter(adapter);
//        } else {
//            countryZonesList = new ArrayList<>();
//            zonesRecyclerView.setVisibility(View.GONE);
//            tvZones.setClickable(false);
//            tvZones.setText(getResources().getString(R.string.choose_zone));
//            Toast.makeText(getActivity(), getResources().getString(R.string.no_zones), Toast.LENGTH_LONG).show();
//
//        }
//    }

    @Override
    public void onRecyclerItemClicked(int position) {
        if (itemClicked == 1) {
            //countryId = countriesList.get(position).getCountryId();
            tvCountry.setText(Constants.countryName);
            countryRecyclerView.setVisibility(View.GONE);
            AddressesWebService.getCountryZones(Constants.countryId);
        } else if (itemClicked == 2) {
            //zoneId = countryZonesList.get(position).getZoneId();
            tvZones.setText(Constants.zoneName);
            zonesRecyclerView.setVisibility(View.GONE);
        } else if (itemClicked == 3) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setTitle(R.string.app_name);
            dialog.setMessage(R.string.delete_msg);
            dialog.setIcon(R.drawable.logo);
            dialog.setCancelable(true);
            dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    AddressesWebService.deleteAddress(sharedPrefManager.getUserData().getCustomerId(), Constants.addressId);
                }
            });
            dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialog.show().cancel();
                }
            });
            dialog.show();
        }

    }

    public static void getCartTotal() {
        MyCartModel listModel;
        cartMoudels = new ArrayList<>();
        Cursor cr = dbTotal.getDataFromTable(dbTotal, DataBase.MY_CART_TABLE);

        if (cr.getCount() > 0) {
            linNoDataFound.setVisibility(View.GONE);
            linCart.setVisibility(View.VISIBLE);
            cr.moveToFirst();
            do {
                listModel = new MyCartModel(cr.getString(cr.getColumnIndex(ID_COL))
                        , cr.getString(cr.getColumnIndex(NAME_COL))
                        , cr.getString(cr.getColumnIndex(MODEL_COL))
                        , cr.getString(cr.getColumnIndex(OPTION_COL))
                        , cr.getString(cr.getColumnIndex(DOWNLOAD_COL))
                        , cr.getString(cr.getColumnIndex(QUANTITY_COL))
                        , cr.getString(cr.getColumnIndex(SUBSTRACT_COL))
                        , cr.getString(cr.getColumnIndex(PRICE_COL))
                        , cr.getString(cr.getColumnIndex(TOTAL_COL))
                        , cr.getString(cr.getColumnIndex(TAX_COL))
                        , cr.getString(cr.getColumnIndex(REWARD_COL))
                        , cr.getString(cr.getColumnIndex(IMAGE_COL)));

                cartMoudels.add(listModel);

            } while (cr.moveToNext());
        } else {
            linNoDataFound.setVisibility(View.VISIBLE);
            linCart.setVisibility(View.GONE);
        }
        total = 0;
        for (int i = 0; i < cartMoudels.size(); i++) {
            total = total + Double.parseDouble(cartMoudels.get(i).getTotal());
        }
        tvTotal.setText(String.valueOf(total));

        cr.close();
        dbTotal.close();
        double final_total = Double.parseDouble(tvTotal.getText().toString()) + Double.parseDouble(tvShippingCost.getText().toString());
        tvFinalTotal.setText(String.valueOf(final_total));
    }

    public void getMyCartData() {

        MyCartModel listModel;
        cartMoudels = new ArrayList<>();

        DataBase db = new DataBase(getActivity());
        Cursor cr = db.getDataFromTable(db, DataBase.MY_CART_TABLE);

        if (cr.getCount() > 0) {
            linNoDataFound.setVisibility(View.GONE);
            linCart.setVisibility(View.VISIBLE);
            cr.moveToFirst();
            do {
                listModel = new MyCartModel(cr.getString(cr.getColumnIndex(ID_COL))
                        , cr.getString(cr.getColumnIndex(NAME_COL))
                        , cr.getString(cr.getColumnIndex(MODEL_COL))
                        , cr.getString(cr.getColumnIndex(OPTION_COL))
                        , cr.getString(cr.getColumnIndex(DOWNLOAD_COL))
                        , cr.getString(cr.getColumnIndex(QUANTITY_COL))
                        , cr.getString(cr.getColumnIndex(SUBSTRACT_COL))
                        , cr.getString(cr.getColumnIndex(PRICE_COL))
                        , cr.getString(cr.getColumnIndex(TOTAL_COL))
                        , cr.getString(cr.getColumnIndex(TAX_COL))
                        , cr.getString(cr.getColumnIndex(REWARD_COL))
                        , cr.getString(cr.getColumnIndex(IMAGE_COL)));

                //String mytotal = cr.getString(cr.getColumnIndex(TOTAL_COL));
                //mytotal = mytotal.replaceAll("[^\\d-]", "");
                //total = total + Double.parseDouble(mytotal);

                cartMoudels.add(listModel);

            } while (cr.moveToNext());
        } else {
            linNoDataFound.setVisibility(View.VISIBLE);
            linCart.setVisibility(View.GONE);
        }
        total = 0;
        for (int i = 0; i < cartMoudels.size(); i++) {
            total = total + Double.parseDouble(cartMoudels.get(i).getTotal());
        }
        tvTotal.setText(String.valueOf(total));

        cr.close();
        db.close();
        cartdapter = new CartProductsAdapter(getActivity(), cartMoudels);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setAutoMeasureEnabled(true);
        shopRecyclerView.setLayoutManager(layoutManager);
        shopRecyclerView.setNestedScrollingEnabled(false);
        shopRecyclerView.setAdapter(cartdapter);
    }

}
