package net.collection.collection.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import net.collection.collection.Activities.AdressesBookActivity;
import net.collection.collection.Activities.MyOrdersActivity;
import net.collection.collection.Activities.ReturnedProductsListActivity;
import net.collection.collection.Activities.SettingsActivity;
import net.collection.collection.R;

public class ProfileFragment extends Fragment {
    Context context;
    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private TextView tvAddressesBook;
    private TextView tvMyOrders;
    private TextView tvDigitalDownloadFiles;
    private TextView tvPeriodicPayments;
    private TextView tvRewardsPoints;
    private TextView tvReturnedProducts;
    private TextView tvMyBalance;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        lyoutAppBar = (LinearLayout) view.findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) view.findViewById(R.id.btn_back);
        btnSideBar = (ImageView) view.findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) view.findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) view.findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) view.findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) view.findViewById(R.id.btn_menu);
        btnSettings = (ImageView) view.findViewById(R.id.btn_settings);
        tvAddressesBook = (TextView) view.findViewById(R.id.tv_addresses_book);
        tvMyOrders = (TextView) view.findViewById(R.id.tv_my_orders);
        tvDigitalDownloadFiles = (TextView) view.findViewById(R.id.tv_digital_download_files);
        tvPeriodicPayments = (TextView) view.findViewById(R.id.tv_periodic_payments);
        tvRewardsPoints = (TextView) view.findViewById(R.id.tv_rewards_points);
        tvReturnedProducts = (TextView) view.findViewById(R.id.tv_returned_products);
        tvMyBalance = (TextView) view.findViewById(R.id.tv_my_balance);

        tvAddressesBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animZoomIn = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in);
                tvAddressesBook.startAnimation(animZoomIn);
                Intent intent = new Intent(getActivity(),AdressesBookActivity.class);
                startActivity(intent);
            }
        });

        tvMyOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animZoomIn = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in);
                tvMyOrders.startAnimation(animZoomIn);
                Intent intent = new Intent(getActivity(),MyOrdersActivity.class);
                startActivity(intent);
            }
        });

        tvReturnedProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animZoomIn = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in);
                tvReturnedProducts.startAnimation(animZoomIn);
                Intent intent = new Intent(getActivity(),ReturnedProductsListActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }


}
