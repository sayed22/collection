package net.collection.collection.Fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.collection.collection.Adapters.WishListAdapter;
import net.collection.collection.Database.DataBase;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Models.WishListModel;
import net.collection.collection.Networks.ResponseModels.WishListResponseModel;
import net.collection.collection.R;

import java.util.ArrayList;
import java.util.List;

import static net.collection.collection.Database.DataBase.COLUMN_ID;
import static net.collection.collection.Database.DataBase.ID_COL;
import static net.collection.collection.Database.DataBase.IMAGE_COL;
import static net.collection.collection.Database.DataBase.NAME_COL;
import static net.collection.collection.Database.DataBase.PRICE_COL;

public class WishListFragment extends Fragment {

    private LinearLayout lyoutAppBar;
    private ImageView btnBack;
    private ImageView btnSideBar;
    private TextView tvToolbarTitle;
    private AutoCompleteTextView autocompleteSearch;
    private ImageView btnNotifications;
    private ImageView btnMenu;
    private ImageView btnSettings;
    private RecyclerView wishlistRecyclerView;
    private List<WishListResponseModel.Wishlist> wishList;
    private CustomLoadingDialog customLoadingDialog;
    private RelativeLayout linNoDataFound;
    private ImageView imgLogo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wish_list_fragment, container, false);
        lyoutAppBar = (LinearLayout) view.findViewById(R.id.lyout_app_bar);
        btnBack = (ImageView) view.findViewById(R.id.btn_back);
        btnSideBar = (ImageView) view.findViewById(R.id.btn_side_bar);
        tvToolbarTitle = (TextView) view.findViewById(R.id.tv_toolbar_title);
        autocompleteSearch = (AutoCompleteTextView) view.findViewById(R.id.autocomplete_search);
        btnNotifications = (ImageView) view.findViewById(R.id.btn_notifications);
        btnMenu = (ImageView) view.findViewById(R.id.btn_menu);
        btnSettings = (ImageView) view.findViewById(R.id.btn_settings);
        wishlistRecyclerView = (RecyclerView) view.findViewById(R.id.wishlist_recycler_view);
        linNoDataFound = (RelativeLayout) view.findViewById(R.id.lin_no_data_found);

        customLoadingDialog = new CustomLoadingDialog(getActivity(), R.style.DialogSlideAnim);

        //SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        //WishListWebService.wishList(sharedPrefManager.getUserData().getCustomerId());
        //customLoadingDialog.show();
        getMyWishListData();
        return view;
    }

    public void getMyWishListData() {

        List<WishListModel> wishListModels = new ArrayList<>();
        WishListModel listModel;
        DataBase db = new DataBase(getActivity());
        Cursor cr = db.getDataFromTable(db,DataBase.MY_WISH_LIST_TABLE);

        if (cr.getCount() > 0) {
            linNoDataFound.setVisibility(View.GONE);
            cr.moveToFirst();
            do {
                listModel = new WishListModel(cr.getInt(cr.getColumnIndex(COLUMN_ID))
                        , cr.getString(cr.getColumnIndex(ID_COL))
                        , cr.getString(cr.getColumnIndex(NAME_COL))
                        , cr.getString(cr.getColumnIndex(PRICE_COL))
                        , cr.getString(cr.getColumnIndex(IMAGE_COL)));

                wishListModels.add(listModel);

            } while (cr.moveToNext());
        } else {
            linNoDataFound.setVisibility(View.VISIBLE);
        }
        cr.close();
        db.close();
        WishListAdapter adapter = new WishListAdapter(getActivity(), wishListModels);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setAutoMeasureEnabled(true);
        wishlistRecyclerView.setLayoutManager(layoutManager);
        wishlistRecyclerView.setNestedScrollingEnabled(false);
        wishlistRecyclerView.setAdapter(adapter);
    }

    /*@Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(WishListEvent wishListEvent) {
        if (wishListEvent.getWishListResponseModel().getSuccess()) {

            wishList = wishListEvent.getWishListResponseModel().getWishlist();
            WishListAdapter adapter = new WishListAdapter(getActivity(), wishList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            layoutManager.setAutoMeasureEnabled(true);
            wishlistRecyclerView.setLayoutManager(layoutManager);
            wishlistRecyclerView.setNestedScrollingEnabled(false);
            wishlistRecyclerView.setAdapter(adapter);
            SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
            sharedPrefManager.setWishList(wishListEvent.getWishListResponseModel());
            customLoadingDialog.cancel();
        } else {
            customLoadingDialog.cancel();

        }
    }*/


}