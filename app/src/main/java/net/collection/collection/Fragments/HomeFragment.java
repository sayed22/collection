package net.collection.collection.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.collection.collection.Adapters.BestsellerAdapter;
import net.collection.collection.Adapters.FeaturedAdapter;
import net.collection.collection.Adapters.LatestAdapter;
import net.collection.collection.Adapters.OffersAdapter;
import net.collection.collection.Adapters.SectionsAdapter;
import net.collection.collection.Adapters.SliderAdapter;
import net.collection.collection.Adapters.SliderBottomImagesAdapter;
import net.collection.collection.Dialogs.CustomLoadingDialog;
import net.collection.collection.Networks.Events.LatestEvent;
import net.collection.collection.Networks.Events.OffersEvent;
import net.collection.collection.Networks.Events.SectionsEvent;
import net.collection.collection.Networks.Events.SliderBottomBannarEvent;
import net.collection.collection.Networks.Events.SliderEvent;
import net.collection.collection.Networks.Events.getBestsellerEvent;
import net.collection.collection.Networks.Events.getFeaturedEvent;
import net.collection.collection.Networks.ResponseModels.LatestResponseModel;
import net.collection.collection.Networks.ResponseModels.OffersResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionsResponseModel;
import net.collection.collection.Networks.ResponseModels.SliderBottomBannarResponseModel;
import net.collection.collection.Networks.ResponseModels.SliderResponseModel;
import net.collection.collection.Networks.ResponseModels.getBestsellerResponseModel;
import net.collection.collection.Networks.ResponseModels.getFeaturedResponseModel;
import net.collection.collection.Networks.ResponseModels.getPerfumesResponseModel;
import net.collection.collection.Networks.WebServices.ProductsWebService;
import net.collection.collection.R;
import net.collection.collection.Utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment {
    Context context;
    private int currentPage = 0;
    private int NUM_PAGES;
    private List<SliderResponseModel.Banners> sliderList;
    public static RelativeLayout frmHomeImagesSlider;
    private ViewPager photosViewpager;
    private TabLayout tabLayoutImages;
    private RecyclerView sectionsRecyclerView;
    private RecyclerView latestRecyclerView;
    private RecyclerView offersRecyclerView;
    private List<SectionsResponseModel.CategoryInfo> sectionsList;
    private List<LatestResponseModel.Latest> latestList;
    private List<OffersResponseModel.Special> offersList;
    private CustomLoadingDialog customLoadingDialog;
    private TextView tvBrowseCategory;
    private TextView tvLatestProduct;
    private TextView tvSpecialOffers;
    private TextView tvBestseller;
    private RecyclerView bestsellerRecyclerView;
    private TextView tvFeatured, tvShopByBrand;
    private RecyclerView featuredRecyclerView;
    private TextView tvPerfume;
    private RecyclerView perfumesRecyclerView;
    private List<getBestsellerResponseModel.Bestseller> bestSellerList;
    private List<getFeaturedResponseModel.Feature> featuresList;
    private List<getPerfumesResponseModel.Products> perfumesList;
    private List<SliderBottomBannarResponseModel.Banners> sliderBottomBannerList;
    private RecyclerView bottomBannarRecyclerView;
    private List<SliderBottomBannarResponseModel.Banners> bottomBannarList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        frmHomeImagesSlider = (RelativeLayout) view.findViewById(R.id.frm_home_images_slider);
        photosViewpager = (ViewPager) view.findViewById(R.id.photos_viewpager);
        tabLayoutImages = (TabLayout) view.findViewById(R.id.tab_layout_images);
        sectionsRecyclerView = (RecyclerView) view.findViewById(R.id.sections_recycler_view);
        latestRecyclerView = (RecyclerView) view.findViewById(R.id.latest_recycler_view);
        offersRecyclerView = (RecyclerView) view.findViewById(R.id.offers_recycler_view);
        tvBrowseCategory = (TextView) view.findViewById(R.id.tv_browse_category);
        tvLatestProduct = (TextView) view.findViewById(R.id.tv_latest_product);
        tvSpecialOffers = (TextView) view.findViewById(R.id.tv_special_offers);
        tvBestseller = (TextView) view.findViewById(R.id.tv_bestseller);
        bestsellerRecyclerView = (RecyclerView) view.findViewById(R.id.bestseller_recycler_view);
        tvFeatured = (TextView) view.findViewById(R.id.tv_featured);
        tvShopByBrand = (TextView) view.findViewById(R.id.tv_shop_by_brand);
        featuredRecyclerView = (RecyclerView) view.findViewById(R.id.featured_recycler_view);
        // tvPerfume = (TextView) view.findViewById(R.id.tv_perfume);
        //perfumesRecyclerView = (RecyclerView) view.findViewById(R.id.perfumes_recycler_view);
        bottomBannarRecyclerView = (RecyclerView) view.findViewById(R.id.bottom_bannar_recycler_view);


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        customLoadingDialog = new CustomLoadingDialog(getActivity(), R.style.DialogSlideAnim);
        customLoadingDialog.show();
        ProductsWebService.getSlider();
        ProductsWebService.getBestSeller();
        ProductsWebService.getFeatured();
        ProductsWebService.getSections();
        // ProductsWebService.getPerfumes();
        ProductsWebService.getLatest();
        ProductsWebService.getOffers();
        ProductsWebService.getSliderBottomBannar();

        Utils.checkConnection(getActivity(), customLoadingDialog);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SliderEvent sliderEvent) {
        if (sliderEvent.getSliderResponseModel().getSuccess()) {
            frmHomeImagesSlider.setVisibility(View.VISIBLE);
            sliderList = sliderEvent.getSliderResponseModel().getBanners();
            NUM_PAGES = sliderList.size();
            SliderAdapter adapter = new SliderAdapter(getActivity(), sliderList);
            photosViewpager.setAdapter(adapter);
            tabLayoutImages.setupWithViewPager(photosViewpager, true);
            final Handler handler = new Handler();
            final Runnable update = new Runnable() {
                public void run() {
                    //if (currentPage == NUM_PAGES - 1) {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    photosViewpager.setCurrentItem(currentPage++, true);
                }
            };

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(update);
                }
            }, 100, 6000);

        } else {
            frmHomeImagesSlider.setVisibility(View.GONE);
            customLoadingDialog.cancel();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(getBestsellerEvent getBestsellerEvent) {
        if (getBestsellerEvent.getGetBestsellerResponseModel().getSuccess()) {
            bestSellerList = getBestsellerEvent.getGetBestsellerResponseModel().getBestseller();
            BestsellerAdapter adapter = new BestsellerAdapter(getActivity(), bestSellerList);
            bestsellerRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            bestsellerRecyclerView.setNestedScrollingEnabled(false);
            bestsellerRecyclerView.setAdapter(adapter);
            tvBestseller.setVisibility(View.VISIBLE);

//            perfumesList = getPerfumesEvent.getGetPerfumesResponseModel().getProducts();
//            PerfumeAdapter adapter = new PerfumeAdapter(getActivity(), perfumesList);
//            perfumesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false));
//            perfumesRecyclerView.setNestedScrollingEnabled(false);
//            perfumesRecyclerView.setAdapter(adapter);
//            tvPerfume.setVisibility(View.VISIBLE);


        } else {
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(getFeaturedEvent getFeaturedEvent) {
        if (getFeaturedEvent.getGetFeaturedResponseModel().getSuccess()) {
            featuresList = getFeaturedEvent.getGetFeaturedResponseModel().getFeature();
            FeaturedAdapter adapter = new FeaturedAdapter(getActivity(), featuresList);
            featuredRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            featuredRecyclerView.setNestedScrollingEnabled(false);
            featuredRecyclerView.setAdapter(adapter);
            tvFeatured.setVisibility(View.VISIBLE);
        } else {
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SectionsEvent sectionsEvent) {
        if (sectionsEvent.getSectionsResponseModel().getSuccess()) {
            sectionsList = sectionsEvent.getSectionsResponseModel().getCategoryInfo();
            System.out.println("count sections : " + sectionsList.size());

            SectionsAdapter adapter = new SectionsAdapter(getActivity(), sectionsList);
            //RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            layoutManager.setAutoMeasureEnabled(true);
            sectionsRecyclerView.setLayoutManager(layoutManager);
            sectionsRecyclerView.setNestedScrollingEnabled(false);
            sectionsRecyclerView.setAdapter(adapter);
            tvBrowseCategory.setVisibility(View.VISIBLE);
            customLoadingDialog.cancel();
        } else {
            customLoadingDialog.cancel();
        }
    }

   /* @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(getPerfumesEvent getPerfumesEvent) {
        if (getPerfumesEvent.getGetPerfumesResponseModel().getSuccess()) {
            perfumesList = getPerfumesEvent.getGetPerfumesResponseModel().getProducts();
            PerfumeAdapter adapter = new PerfumeAdapter(getActivity(), perfumesList);
            perfumesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false));
            perfumesRecyclerView.setNestedScrollingEnabled(false);
            perfumesRecyclerView.setAdapter(adapter);
            tvPerfume.setVisibility(View.VISIBLE);
        } else {
        }
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LatestEvent latestEvent) {
        if (latestEvent.getLatestResponseModel().getSuccess()) {
            latestList = latestEvent.getLatestResponseModel().getLatest();
            System.out.println("count latest : " + latestList.size());

            LatestAdapter adapter = new LatestAdapter(getActivity(), latestList);
            latestRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            latestRecyclerView.setNestedScrollingEnabled(false);
            latestRecyclerView.setAdapter(adapter);
            tvLatestProduct.setVisibility(View.VISIBLE);
        } else {
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OffersEvent offersEvent) {
        if (offersEvent.getOffersResponseModel().getSuccess()) {
            offersList = offersEvent.getOffersResponseModel().getSpecial();
            System.out.println("count offers : " + offersList.size());
            OffersAdapter adapter = new OffersAdapter(getActivity(), offersList);
            offersRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            offersRecyclerView.setNestedScrollingEnabled(false);
            offersRecyclerView.setAdapter(adapter);
            tvSpecialOffers.setVisibility(View.VISIBLE);
        } else {
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SliderBottomBannarEvent sliderBottomBannarEvent) {
        if (sliderBottomBannarEvent.getSliderBottomBannarResponseModel().getSuccess()) {
            bottomBannarList = sliderBottomBannarEvent.getSliderBottomBannarResponseModel().getBanners();
            SliderBottomImagesAdapter adapter = new SliderBottomImagesAdapter(getActivity(), bottomBannarList);
            // bottomBannarRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
            bottomBannarRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            bottomBannarRecyclerView.setNestedScrollingEnabled(false);
            bottomBannarRecyclerView.setAdapter(adapter);
            tvShopByBrand.setVisibility(View.VISIBLE);
        }
    }

}
