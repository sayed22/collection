package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EditAddressResponseModel {

    @Expose
    @SerializedName("addressesdata")
    private List<Addressesdata> addressesdata;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Addressesdata> getAddressesdata() {
        return addressesdata;
    }

    public void setAddressesdata(List<Addressesdata> addressesdata) {
        this.addressesdata = addressesdata;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Addressesdata {
        @Expose
        @SerializedName("set_default")
        private String setDefault;
        @Expose
        @SerializedName("zone_id")
        private String zoneId;
        @Expose
        @SerializedName("country_id")
        private String countryId;
        @Expose
        @SerializedName("postcode")
        private String postcode;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("address_2")
        private String address2;
        @Expose
        @SerializedName("address_1")
        private String address1;
        @Expose
        @SerializedName("company")
        private String company;
        @Expose
        @SerializedName("lastname")
        private String lastname;
        @Expose
        @SerializedName("firstname")
        private String firstname;
        @Expose
        @SerializedName("address_id")
        private String addressId;
        @Expose
        @SerializedName("customer_id")
        private String customerId;

        public String getSetDefault() {
            return setDefault;
        }

        public void setSetDefault(String setDefault) {
            this.setDefault = setDefault;
        }

        public String getZoneId() {
            return zoneId;
        }

        public void setZoneId(String zoneId) {
            this.zoneId = zoneId;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getAddressId() {
            return addressId;
        }

        public void setAddressId(String addressId) {
            this.addressId = addressId;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }
    }
}
