package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.AddNewAddressResponseModel;

public class AddNNewAddressEvent {
    private AddNewAddressResponseModel addNewAddressResponseModel;

    public AddNNewAddressEvent(AddNewAddressResponseModel addNewAddressResponseModel) {
        this.addNewAddressResponseModel = addNewAddressResponseModel;
    }

    public AddNewAddressResponseModel getAddNewAddressResponseModel() {
        return addNewAddressResponseModel;
    }

    public void setAddNewAddressResponseModel(AddNewAddressResponseModel addNewAddressResponseModel) {
        this.addNewAddressResponseModel = addNewAddressResponseModel;
    }
}
