package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.AddToWishListResponseModel;

public class AddToWishListEvent {
    private AddToWishListResponseModel addToWishListResponseModel;

    public AddToWishListEvent(AddToWishListResponseModel addToWishListResponseModel) {
        this.addToWishListResponseModel = addToWishListResponseModel;
    }

    public AddToWishListResponseModel getAddToWishListResponseModel() {
        return addToWishListResponseModel;
    }

    public void setAddToWishListResponseModel(AddToWishListResponseModel addToWishListResponseModel) {
        this.addToWishListResponseModel = addToWishListResponseModel;
    }
}
