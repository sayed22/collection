package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.GetProductResponseModel;

public class GetProductEvent {
    private GetProductResponseModel getProductResponseModel;

    public GetProductEvent(GetProductResponseModel getProductResponseModel) {
        this.getProductResponseModel = getProductResponseModel;
    }

    public GetProductResponseModel getGetProductResponseModel() {
        return getProductResponseModel;
    }

    public void setGetProductResponseModel(GetProductResponseModel getProductResponseModel) {
        this.getProductResponseModel = getProductResponseModel;
    }
}
