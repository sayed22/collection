package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetReturnReasonsResponseModel {

    @Expose
    @SerializedName("Returnreasons")
    private List<Returnreasons> returnreasons;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Returnreasons> getReturnreasons() {
        return returnreasons;
    }

    public void setReturnreasons(List<Returnreasons> returnreasons) {
        this.returnreasons = returnreasons;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Returnreasons {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("return_reason_id")
        private String returnReasonId;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getReturnReasonId() {
            return returnReasonId;
        }

        public void setReturnReasonId(String returnReasonId) {
            this.returnReasonId = returnReasonId;
        }
    }
}
