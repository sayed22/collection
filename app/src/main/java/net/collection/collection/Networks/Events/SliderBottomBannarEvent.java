package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.SliderBottomBannarResponseModel;

public class SliderBottomBannarEvent {
    private SliderBottomBannarResponseModel sliderBottomBannarResponseModel;

    public SliderBottomBannarEvent(SliderBottomBannarResponseModel sliderBottomBannarResponseModel) {
        this.sliderBottomBannarResponseModel = sliderBottomBannarResponseModel;
    }

    public SliderBottomBannarResponseModel getSliderBottomBannarResponseModel() {
        return sliderBottomBannarResponseModel;
    }

    public void setSliderBottomBannarResponseModel(SliderBottomBannarResponseModel sliderBottomBannarResponseModel) {
        this.sliderBottomBannarResponseModel = sliderBottomBannarResponseModel;
    }
}
