package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.RemoveFromWishListResponseModel;

public class RemoveFromWishListEvent {
    private RemoveFromWishListResponseModel removeFromWishListResponseModel;

    public RemoveFromWishListEvent(RemoveFromWishListResponseModel removeFromWishListResponseModel) {
        this.removeFromWishListResponseModel = removeFromWishListResponseModel;
    }

    public RemoveFromWishListResponseModel getRemoveFromWishListResponseModel() {
        return removeFromWishListResponseModel;
    }

    public void setRemoveFromWishListResponseModel(RemoveFromWishListResponseModel removeFromWishListResponseModel) {
        this.removeFromWishListResponseModel = removeFromWishListResponseModel;
    }
}
