package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.ReturnedProductsListResponseModel;

public class ReturnedProductsListEvent {
    private ReturnedProductsListResponseModel returnedProductsListResponseModel;

    public ReturnedProductsListEvent(ReturnedProductsListResponseModel returnedProductsListResponseModel) {
        this.returnedProductsListResponseModel = returnedProductsListResponseModel;
    }

    public ReturnedProductsListResponseModel getReturnedProductsListResponseModel() {
        return returnedProductsListResponseModel;
    }

    public void setReturnedProductsListResponseModel(ReturnedProductsListResponseModel returnedProductsListResponseModel) {
        this.returnedProductsListResponseModel = returnedProductsListResponseModel;
    }
}
