package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.LogInResponseModel;

public class LogInEvent {
    private LogInResponseModel logInResponseModel;

    public LogInEvent(LogInResponseModel logInResponseModel) {
        this.logInResponseModel = logInResponseModel;
    }

    public LogInResponseModel getLogInResponseModel() {
        return logInResponseModel;
    }

    public void setLogInResponseModel(LogInResponseModel logInResponseModel) {
        this.logInResponseModel = logInResponseModel;
    }
}
