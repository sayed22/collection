package net.collection.collection.Networks.WebServices;

import android.util.Log;

import net.collection.collection.Networks.Api.ApiClient;
import net.collection.collection.Networks.Api.ApiInterface;
import net.collection.collection.Networks.Events.AddToWishListEvent;
import net.collection.collection.Networks.Events.CartProductsEvent;
import net.collection.collection.Networks.Events.RemoveFromWishListEvent;
import net.collection.collection.Networks.Events.WishListEvent;
import net.collection.collection.Networks.ResponseModels.AddToWishListResponseModel;
import net.collection.collection.Networks.ResponseModels.CartProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.RemoveFromWishListResponseModel;
import net.collection.collection.Networks.ResponseModels.WishListResponseModel;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishListWebService {
    public static final String TAG = WishListWebService.class.getName();

    public static void wishList(String customer_id) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<WishListResponseModel> call = apiService.WISH_LIST_RESPONSE_MODEL_CALL(customer_id);
        call.enqueue(new Callback<WishListResponseModel>() {
            @Override
            public void onResponse(Call<WishListResponseModel> call, Response<WishListResponseModel> response) {
                EventBus.getDefault().post(new WishListEvent(response.body()));
            }

            @Override
            public void onFailure(Call<WishListResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void addToWishList(String customer_id, String product_id) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<AddToWishListResponseModel> call = apiService.ADD_TO_WISH_LIST_RESPONSE_MODEL_CALL(customer_id, product_id);
        call.enqueue(new Callback<AddToWishListResponseModel>() {
            @Override
            public void onResponse(Call<AddToWishListResponseModel> call, Response<AddToWishListResponseModel> response) {
                EventBus.getDefault().post(new AddToWishListEvent(response.body()));
            }

            @Override
            public void onFailure(Call<AddToWishListResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void removeFromWishList(String customer_id, String product_id) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<RemoveFromWishListResponseModel> call = apiService.REMOVE_FROM_WISH_LIST_RESPONSE_MODEL_CALL(customer_id, product_id);
        call.enqueue(new Callback<RemoveFromWishListResponseModel>() {
            @Override
            public void onResponse(Call<RemoveFromWishListResponseModel> call, Response<RemoveFromWishListResponseModel> response) {
                EventBus.getDefault().post(new RemoveFromWishListEvent(response.body()));
            }

            @Override
            public void onFailure(Call<RemoveFromWishListResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
}
