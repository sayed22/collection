package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProductResponseModel {

    @Expose
    @SerializedName("product")
    private Product product;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Product {
        @Expose
        @SerializedName("rating")
        private int rating;
        @Expose
        @SerializedName("special")
        private String special;
        @Expose
        @SerializedName("thumb")
        private String thumb;
        @Expose
        @SerializedName("href")
        private String href;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("viewed")
        private String viewed;
        @Expose
        @SerializedName("date_modified")
        private String dateModified;
        @Expose
        @SerializedName("date_added")
        private String dateAdded;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("sort_order")
        private String sortOrder;
        @Expose
        @SerializedName("minimum")
        private String minimum;
        @Expose
        @SerializedName("reviews")
        private String reviews;
        @Expose
        @SerializedName("subtract")
        private String subtract;
        @Expose
        @SerializedName("length_class_id")
        private String lengthClassId;
        @Expose
        @SerializedName("height")
        private String height;
        @Expose
        @SerializedName("width")
        private String width;
        @Expose
        @SerializedName("length")
        private String length;
        @Expose
        @SerializedName("weight_class_id")
        private String weightClassId;
        @Expose
        @SerializedName("tax_class_id")
        private String taxClassId;
        @Expose
        @SerializedName("date_available")
        private String dateAvailable;
        @Expose
        @SerializedName("points")
        private String points;
        @Expose
        @SerializedName("reward")
        private String reward;
        @Expose
        @SerializedName("stock_status")
        private String stockStatus;
        @Expose
        @SerializedName("quantity")
        private String quantity;
        @Expose
        @SerializedName("location")
        private String location;
        @Expose
        @SerializedName("mpn")
        private String mpn;
        @Expose
        @SerializedName("isbn")
        private String isbn;
        @Expose
        @SerializedName("jan")
        private String jan;
        @Expose
        @SerializedName("ean")
        private String ean;
        @Expose
        @SerializedName("upc")
        private String upc;
        @Expose
        @SerializedName("sku")
        private String sku;
        @Expose
        @SerializedName("model")
        private String model;
        @Expose
        @SerializedName("tag")
        private String tag;
        @Expose
        @SerializedName("meta_keyword")
        private String metaKeyword;
        @Expose
        @SerializedName("meta_description")
        private String metaDescription;
        @Expose
        @SerializedName("meta_title")
        private String metaTitle;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getSpecial() {
            return special;
        }

        public void setSpecial(String special) {
            this.special = special;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getViewed() {
            return viewed;
        }

        public void setViewed(String viewed) {
            this.viewed = viewed;
        }

        public String getDateModified() {
            return dateModified;
        }

        public void setDateModified(String dateModified) {
            this.dateModified = dateModified;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public String getMinimum() {
            return minimum;
        }

        public void setMinimum(String minimum) {
            this.minimum = minimum;
        }

        public String getReviews() {
            return reviews;
        }

        public void setReviews(String reviews) {
            this.reviews = reviews;
        }

        public String getSubtract() {
            return subtract;
        }

        public void setSubtract(String subtract) {
            this.subtract = subtract;
        }

        public String getLengthClassId() {
            return lengthClassId;
        }

        public void setLengthClassId(String lengthClassId) {
            this.lengthClassId = lengthClassId;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getLength() {
            return length;
        }

        public void setLength(String length) {
            this.length = length;
        }

        public String getWeightClassId() {
            return weightClassId;
        }

        public void setWeightClassId(String weightClassId) {
            this.weightClassId = weightClassId;
        }

        public String getTaxClassId() {
            return taxClassId;
        }

        public void setTaxClassId(String taxClassId) {
            this.taxClassId = taxClassId;
        }

        public String getDateAvailable() {
            return dateAvailable;
        }

        public void setDateAvailable(String dateAvailable) {
            this.dateAvailable = dateAvailable;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getReward() {
            return reward;
        }

        public void setReward(String reward) {
            this.reward = reward;
        }

        public String getStockStatus() {
            return stockStatus;
        }

        public void setStockStatus(String stockStatus) {
            this.stockStatus = stockStatus;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getMpn() {
            return mpn;
        }

        public void setMpn(String mpn) {
            this.mpn = mpn;
        }

        public String getIsbn() {
            return isbn;
        }

        public void setIsbn(String isbn) {
            this.isbn = isbn;
        }

        public String getJan() {
            return jan;
        }

        public void setJan(String jan) {
            this.jan = jan;
        }

        public String getEan() {
            return ean;
        }

        public void setEan(String ean) {
            this.ean = ean;
        }

        public String getUpc() {
            return upc;
        }

        public void setUpc(String upc) {
            this.upc = upc;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getMetaKeyword() {
            return metaKeyword;
        }

        public void setMetaKeyword(String metaKeyword) {
            this.metaKeyword = metaKeyword;
        }

        public String getMetaDescription() {
            return metaDescription;
        }

        public void setMetaDescription(String metaDescription) {
            this.metaDescription = metaDescription;
        }

        public String getMetaTitle() {
            return metaTitle;
        }

        public void setMetaTitle(String metaTitle) {
            this.metaTitle = metaTitle;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
