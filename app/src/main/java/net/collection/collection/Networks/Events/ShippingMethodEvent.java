package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.ShippingMethodResponseModel;

public class ShippingMethodEvent {
    private ShippingMethodResponseModel shippingMethodResponseModel;

    public ShippingMethodEvent(ShippingMethodResponseModel shippingMethodResponseModel) {
        this.shippingMethodResponseModel = shippingMethodResponseModel;
    }

    public ShippingMethodResponseModel getShippingMethodResponseModel() {
        return shippingMethodResponseModel;
    }

    public void setShippingMethodResponseModel(ShippingMethodResponseModel shippingMethodResponseModel) {
        this.shippingMethodResponseModel = shippingMethodResponseModel;
    }
}
