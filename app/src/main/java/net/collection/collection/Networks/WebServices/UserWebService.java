package net.collection.collection.Networks.WebServices;

import android.util.Log;

import net.collection.collection.Networks.Api.ApiClient;
import net.collection.collection.Networks.Api.ApiInterface;
import net.collection.collection.Networks.Events.EditPasswordEvent;
import net.collection.collection.Networks.Events.EditProfileEvent;
import net.collection.collection.Networks.Events.ForgotPasswordEvent;
import net.collection.collection.Networks.Events.LogInEvent;
import net.collection.collection.Networks.Events.RegisterEvent;
import net.collection.collection.Networks.ResponseModels.EditPasswordResponseModel;
import net.collection.collection.Networks.ResponseModels.EditProfileResponseModel;
import net.collection.collection.Networks.ResponseModels.ForgotPasswordResponseModel;
import net.collection.collection.Networks.ResponseModels.LogInResponseModel;
import net.collection.collection.Networks.ResponseModels.RegisterResponseModel;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserWebService {
    public static final String TAG = UserWebService.class.getName();

    public static void registerUser(String customer_group_id, String firstname, String lastname, String email, String telephone,
                                    String fax, String company, String address_1, String address_2, String city, String postcode,
                                    String country_id, String zone_id, String password, String newsletter, String agree) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<RegisterResponseModel> call = apiService.REGISTER_RESPONSE_MODEL_CALL(customer_group_id, firstname, lastname, email, telephone,
                fax, company, address_1, address_2, city, postcode, country_id, zone_id, password, newsletter, agree);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                EventBus.getDefault().post(new RegisterEvent(response.body()));
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void logInUser(String email, String password) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<LogInResponseModel> call = apiService.LOG_IN_RESPONSE_MODEL_CALL(email, password);
        call.enqueue(new Callback<LogInResponseModel>() {
            @Override
            public void onResponse(Call<LogInResponseModel> call, Response<LogInResponseModel> response) {
                EventBus.getDefault().post(new LogInEvent(response.body()));
            }

            @Override
            public void onFailure(Call<LogInResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void forgotPassword(String email) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ForgotPasswordResponseModel> call = apiService.FORGOT_PASSWORD_RESPONSE_MODEL_CALL(email);
        call.enqueue(new Callback<ForgotPasswordResponseModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponseModel> call, Response<ForgotPasswordResponseModel> response) {
                EventBus.getDefault().post(new ForgotPasswordEvent(response.body()));
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void editProfile(String firstname, String lastname, String email, String telephone, String fax, String customer_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<EditProfileResponseModel> call = apiService.EDIT_PROFILE_RESPONSE_MODEL_CALL(firstname, lastname, email, telephone, fax, customer_id);
        call.enqueue(new Callback<EditProfileResponseModel>() {
            @Override
            public void onResponse(Call<EditProfileResponseModel> call, Response<EditProfileResponseModel> response) {
                EventBus.getDefault().post(new EditProfileEvent(response.body()));
            }

            @Override
            public void onFailure(Call<EditProfileResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void editPassword(String email, String old_password, String new_password) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<EditPasswordResponseModel> call = apiService.EDIT_PASSWORD_RESPONSE_MODEL_CALL(email, old_password, new_password);
        call.enqueue(new Callback<EditPasswordResponseModel>() {
            @Override
            public void onResponse(Call<EditPasswordResponseModel> call, Response<EditPasswordResponseModel> response) {
                EventBus.getDefault().post(new EditPasswordEvent(response.body()));
            }

            @Override
            public void onFailure(Call<EditPasswordResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
}
