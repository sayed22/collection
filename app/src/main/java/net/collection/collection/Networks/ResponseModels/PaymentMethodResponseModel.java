package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentMethodResponseModel {

    @Expose
    @SerializedName("payment")
    private List<Payment> payment;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Payment> getPayment() {
        return payment;
    }

    public void setPayment(List<Payment> payment) {
        this.payment = payment;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Payment {
        @Expose
        @SerializedName("sort_order")
        private String sortOrder;
        @Expose
        @SerializedName("terms")
        private String terms;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("code")
        private String code;

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public String getTerms() {
            return terms;
        }

        public void setTerms(String terms) {
            this.terms = terms;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
