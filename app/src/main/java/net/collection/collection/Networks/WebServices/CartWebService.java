package net.collection.collection.Networks.WebServices;

import android.util.Log;

import com.google.gson.GsonBuilder;

import net.collection.collection.Networks.Api.ApiClient;
import net.collection.collection.Networks.Api.ApiInterface;
import net.collection.collection.Networks.Events.AddProductToCartEvent;
import net.collection.collection.Networks.Events.CartProductsEvent;
import net.collection.collection.Networks.Events.ClearCartEvent;
import net.collection.collection.Networks.Events.GetProductEvent;
import net.collection.collection.Networks.Events.ImagesCommentsDiscountDataEvent;
import net.collection.collection.Networks.Events.LatestEvent;
import net.collection.collection.Networks.Events.OffersEvent;
import net.collection.collection.Networks.Events.PaymentMethodEvent;
import net.collection.collection.Networks.Events.ProductOptionsEvent;
import net.collection.collection.Networks.Events.ProductsEvent;
import net.collection.collection.Networks.Events.RelatedProductsEvent;
import net.collection.collection.Networks.Events.SectionProductsEvent;
import net.collection.collection.Networks.Events.SectionsEvent;
import net.collection.collection.Networks.Events.ShippingMethodEvent;
import net.collection.collection.Networks.Events.SliderEvent;
import net.collection.collection.Networks.ResponseModels.AddProductToCartResponseModel;
import net.collection.collection.Networks.ResponseModels.CartProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.ClearCartResponseModel;
import net.collection.collection.Networks.ResponseModels.GetProductResponseModel;
import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;
import net.collection.collection.Networks.ResponseModels.LatestResponseModel;
import net.collection.collection.Networks.ResponseModels.OffersResponseModel;
import net.collection.collection.Networks.ResponseModels.PaymentMethodResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.RelatedProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionsResponseModel;
import net.collection.collection.Networks.ResponseModels.ShippingMethodResponseModel;
import net.collection.collection.Networks.ResponseModels.SliderResponseModel;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartWebService {
    public static final String TAG = CartWebService.class.getName();
    public static String responseBody;

    public static void addProductToCart(String customer_id, String product_id, String quantity, String[] option) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<AddProductToCartResponseModel> call = apiService.ADD_PRODUCT_TO_CART_RESPONSE_MODEL_CALL(customer_id, product_id, quantity, option);
        call.enqueue(new Callback<AddProductToCartResponseModel>() {
            @Override
            public void onResponse(Call<AddProductToCartResponseModel> call, Response<AddProductToCartResponseModel> response) {
                EventBus.getDefault().post(new AddProductToCartEvent(response.body()));
            }

            @Override
            public void onFailure(Call<AddProductToCartResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void CartProducts(String customer_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.CART_PRODUCTS_RESPONSE_MODEL_CALL(customer_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    EventBus.getDefault().post(new CartProductsEvent(response.body().string()));
                    System.out.println("print : " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        /*call.enqueue(new Callback<CartProductsResponseModel>() {
            @Override
            public void onResponse(Call<CartProductsResponseModel> call, Response<CartProductsResponseModel> response) {
                EventBus.getDefault().post(new CartProductsEvent(response.body()));

            }

            @Override
            public void onFailure(Call<CartProductsResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });*/
    }

    public static void clearCart(String customer_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ClearCartResponseModel> call = apiService.CLEAR_CART_RESPONSE_MODEL_CALL(customer_id);
        call.enqueue(new Callback<ClearCartResponseModel>() {
            @Override
            public void onResponse(Call<ClearCartResponseModel> call, Response<ClearCartResponseModel> response) {
                EventBus.getDefault().post(new ClearCartEvent(response.body()));
            }

            @Override
            public void onFailure(Call<ClearCartResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getPaymentMethods() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentMethodResponseModel> call = apiService.PAYMENT_METHOD_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<PaymentMethodResponseModel>() {
            @Override
            public void onResponse(Call<PaymentMethodResponseModel> call, Response<PaymentMethodResponseModel> response) {
                EventBus.getDefault().post(new PaymentMethodEvent(response.body()));
            }

            @Override
            public void onFailure(Call<PaymentMethodResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getShippingMethods() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ShippingMethodResponseModel> call = apiService.SHIPPING_METHOD_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<ShippingMethodResponseModel>() {
            @Override
            public void onResponse(Call<ShippingMethodResponseModel> call, Response<ShippingMethodResponseModel> response) {
                EventBus.getDefault().post(new ShippingMethodEvent(response.body()));
            }

            @Override
            public void onFailure(Call<ShippingMethodResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
}
