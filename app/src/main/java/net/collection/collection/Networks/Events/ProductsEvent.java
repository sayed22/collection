package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.ProductsResponseModel;

public class ProductsEvent {
    private ProductsResponseModel productsResponseModel;

    public ProductsEvent(ProductsResponseModel productsResponseModel) {
        this.productsResponseModel = productsResponseModel;
    }

    public ProductsResponseModel getProductsResponseModel() {
        return productsResponseModel;
    }

    public void setProductsResponseModel(ProductsResponseModel productsResponseModel) {
        this.productsResponseModel = productsResponseModel;
    }
}
