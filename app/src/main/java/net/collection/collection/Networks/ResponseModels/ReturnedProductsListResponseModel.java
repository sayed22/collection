package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReturnedProductsListResponseModel {

    @Expose
    @SerializedName("returns")
    private Returns returns;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Returns getReturns() {
        return returns;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Returns {
        @Expose
        @SerializedName("results")
        private List<Results> results;

        public List<Results> getResults() {
            return results;
        }

        public void setResults(List<Results> results) {
            this.results = results;
        }
    }

    public static class Results {
        @Expose
        @SerializedName("date_added")
        private String dateAdded;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("order_id")
        private String orderId;
        @Expose
        @SerializedName("return_id")
        private String returnId;

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getReturnId() {
            return returnId;
        }

        public void setReturnId(String returnId) {
            this.returnId = returnId;
        }
    }
}
