package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.AddProductToCartResponseModel;

public class AddProductToCartEvent {
    private AddProductToCartResponseModel addProductToCartResponseModel;

    public AddProductToCartEvent(AddProductToCartResponseModel addProductToCartResponseModel) {
        this.addProductToCartResponseModel = addProductToCartResponseModel;
    }

    public AddProductToCartResponseModel getAddProductToCartResponseModel() {
        return addProductToCartResponseModel;
    }

    public void setAddProductToCartResponseModel(AddProductToCartResponseModel addProductToCartResponseModel) {
        this.addProductToCartResponseModel = addProductToCartResponseModel;
    }
}
