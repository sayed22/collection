package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.collection.collection.Models.CartMoudel;

import java.util.ArrayList;
import java.util.List;

public class CartProductsResponseModel {

    @Expose
    @SerializedName("success")
    private boolean success;
    @Expose
    @SerializedName("cartproduct")
    private ArrayList<CartMoudel> cartproduct;

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<CartMoudel> getCartproduct() {
        return cartproduct;
    }

    public void setCartproduct(ArrayList<CartMoudel> cartproduct) {
        this.cartproduct = cartproduct;
    }

}
