package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShippingMethodResponseModel {

    @Expose
    @SerializedName("shipping")
    private List<Shipping> shipping;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Shipping> getShipping() {
        return shipping;
    }

    public void setShipping(List<Shipping> shipping) {
        this.shipping = shipping;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Shipping {
        @Expose
        @SerializedName("sort_order")
        private String sortOrder;
        @Expose
        @SerializedName("quote")
        private List<Quote> quote;

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public List<Quote> getQuote() {
            return quote;
        }

        public void setQuote(List<Quote> quote) {
            this.quote = quote;
        }
    }

    public static class Quote {
        @Expose
        @SerializedName("text")
        private String text;
        @Expose
        @SerializedName("tax_class_id")
        private String taxClassId;
        @Expose
        @SerializedName("cost")
        private String cost;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("code")
        private String code;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getTaxClassId() {
            return taxClassId;
        }

        public void setTaxClassId(String taxClassId) {
            this.taxClassId = taxClassId;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
