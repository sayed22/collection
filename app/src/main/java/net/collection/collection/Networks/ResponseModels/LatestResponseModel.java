package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LatestResponseModel {

    @Expose
    @SerializedName("Latest")
    private List<Latest> latest;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Latest> getLatest() {
        return latest;
    }

    public void setLatest(List<Latest> latest) {
        this.latest = latest;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Latest {
        @Expose
        @SerializedName("href")
        private String href;
        @Expose
        @SerializedName("reviews")
        private int reviews;
        @Expose
        @SerializedName("quantity")
        private String quantity;
        @Expose
        @SerializedName("rating")
        private int rating;
        @Expose
        @SerializedName("stock_status")
        private String stockStatus;
        @Expose
        @SerializedName("tax")
        private boolean tax;
        @Expose
        @SerializedName("special")
        private boolean special;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("thumb")
        private String thumb;
        @Expose
        @SerializedName("product_id")
        private String productId;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }

        public int getReviews() {
            return reviews;
        }

        public void setReviews(int reviews) {
            this.reviews = reviews;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getStockStatus() {
            return stockStatus;
        }

        public void setStockStatus(String stockStatus) {
            this.stockStatus = stockStatus;
        }

        public boolean getTax() {
            return tax;
        }

        public void setTax(boolean tax) {
            this.tax = tax;
        }

        public boolean getSpecial() {
            return special;
        }

        public void setSpecial(boolean special) {
            this.special = special;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }
    }
}
