package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.GetChildCategoryResponseModel;

public class GetChildCategoryEvent {
    private GetChildCategoryResponseModel getChildCategoryResponseModel;

    public GetChildCategoryEvent(GetChildCategoryResponseModel getChildCategoryResponseModel) {
        this.getChildCategoryResponseModel = getChildCategoryResponseModel;
    }

    public GetChildCategoryResponseModel getGetChildCategoryResponseModel() {
        return getChildCategoryResponseModel;
    }

    public void setGetChildCategoryResponseModel(GetChildCategoryResponseModel getChildCategoryResponseModel) {
        this.getChildCategoryResponseModel = getChildCategoryResponseModel;
    }
}
