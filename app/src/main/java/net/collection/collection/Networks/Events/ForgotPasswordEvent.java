package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.ForgotPasswordResponseModel;
import net.collection.collection.Networks.ResponseModels.LogInResponseModel;

public class ForgotPasswordEvent {
private ForgotPasswordResponseModel forgotPasswordResponseModel;

    public ForgotPasswordEvent(ForgotPasswordResponseModel forgotPasswordResponseModel) {
        this.forgotPasswordResponseModel = forgotPasswordResponseModel;
    }

    public ForgotPasswordResponseModel getForgotPasswordResponseModel() {
        return forgotPasswordResponseModel;
    }

    public void setForgotPasswordResponseModel(ForgotPasswordResponseModel forgotPasswordResponseModel) {
        this.forgotPasswordResponseModel = forgotPasswordResponseModel;
    }
}
