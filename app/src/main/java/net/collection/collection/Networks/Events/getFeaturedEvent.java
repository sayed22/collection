package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.getFeaturedResponseModel;

public class getFeaturedEvent {
    private getFeaturedResponseModel getFeaturedResponseModel;

    public getFeaturedEvent(net.collection.collection.Networks.ResponseModels.getFeaturedResponseModel getFeaturedResponseModel) {
        this.getFeaturedResponseModel = getFeaturedResponseModel;
    }

    public net.collection.collection.Networks.ResponseModels.getFeaturedResponseModel getGetFeaturedResponseModel() {
        return getFeaturedResponseModel;
    }

    public void setGetFeaturedResponseModel(net.collection.collection.Networks.ResponseModels.getFeaturedResponseModel getFeaturedResponseModel) {
        this.getFeaturedResponseModel = getFeaturedResponseModel;
    }
}
