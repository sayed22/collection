package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.LatestResponseModel;

public class LatestEvent {
    private LatestResponseModel latestResponseModel;

    public LatestEvent(LatestResponseModel latestResponseModel) {
        this.latestResponseModel = latestResponseModel;
    }

    public LatestResponseModel getLatestResponseModel() {
        return latestResponseModel;
    }

    public void setLatestResponseModel(LatestResponseModel latestResponseModel) {
        this.latestResponseModel = latestResponseModel;
    }
}
