package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.CountryZonesResponseModel;

public class CountryZonesEvent {
    private CountryZonesResponseModel countryZonesResponseModel;

    public CountryZonesEvent(CountryZonesResponseModel countryZonesResponseModel) {
        this.countryZonesResponseModel = countryZonesResponseModel;
    }

    public CountryZonesResponseModel getCountryZonesResponseModel() {
        return countryZonesResponseModel;
    }

    public void setCountryZonesResponseModel(CountryZonesResponseModel countryZonesResponseModel) {
        this.countryZonesResponseModel = countryZonesResponseModel;
    }
}
