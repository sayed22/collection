package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.ReturnProductResponseModel;

public class ReturnProductEvent {
    private ReturnProductResponseModel returnProductResponseModel;

    public ReturnProductEvent(ReturnProductResponseModel returnProductResponseModel) {
        this.returnProductResponseModel = returnProductResponseModel;
    }

    public ReturnProductResponseModel getReturnProductResponseModel() {
        return returnProductResponseModel;
    }

    public void setReturnProductResponseModel(ReturnProductResponseModel returnProductResponseModel) {
        this.returnProductResponseModel = returnProductResponseModel;
    }
}
