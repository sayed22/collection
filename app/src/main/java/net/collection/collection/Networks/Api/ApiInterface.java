package net.collection.collection.Networks.Api;


import net.collection.collection.Networks.ResponseModels.AddNewAddressResponseModel;
import net.collection.collection.Networks.ResponseModels.AddOrderResponseModel;
import net.collection.collection.Networks.ResponseModels.AddProductToCartResponseModel;
import net.collection.collection.Networks.ResponseModels.AddToWishListResponseModel;
import net.collection.collection.Networks.ResponseModels.ClearCartResponseModel;
import net.collection.collection.Networks.ResponseModels.CountriesResponseModel;
import net.collection.collection.Networks.ResponseModels.CountryZonesResponseModel;
import net.collection.collection.Networks.ResponseModels.DeleteAddressResponseModel;
import net.collection.collection.Networks.ResponseModels.EditAddressResponseModel;
import net.collection.collection.Networks.ResponseModels.GetAddressesResponseModel;
import net.collection.collection.Networks.ResponseModels.GetChildCategoryResponseModel;
import net.collection.collection.Networks.ResponseModels.GetOrdersResponseModel;
import net.collection.collection.Networks.ResponseModels.GetReturnReasonsResponseModel;
import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;
import net.collection.collection.Networks.ResponseModels.OneOrderResponseModel;
import net.collection.collection.Networks.ResponseModels.PaymentMethodResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.Networks.ResponseModels.RemoveFromWishListResponseModel;
import net.collection.collection.Networks.ResponseModels.ReturnProductResponseModel;
import net.collection.collection.Networks.ResponseModels.ReturnedProductsListResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.GetProductResponseModel;
import net.collection.collection.Networks.ResponseModels.RelatedProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.LatestResponseModel;
import net.collection.collection.Networks.ResponseModels.OffersResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.EditPasswordResponseModel;
import net.collection.collection.Networks.ResponseModels.EditProfileResponseModel;
import net.collection.collection.Networks.ResponseModels.ForgotPasswordResponseModel;
import net.collection.collection.Networks.ResponseModels.LogInResponseModel;
import net.collection.collection.Networks.ResponseModels.RegisterResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionsResponseModel;
import net.collection.collection.Networks.ResponseModels.ShippingMethodResponseModel;
import net.collection.collection.Networks.ResponseModels.SliderBottomBannarResponseModel;
import net.collection.collection.Networks.ResponseModels.SliderResponseModel;
import net.collection.collection.Networks.ResponseModels.WishListResponseModel;
import net.collection.collection.Networks.ResponseModels.getBestsellerResponseModel;
import net.collection.collection.Networks.ResponseModels.getFeaturedResponseModel;
import net.collection.collection.Networks.ResponseModels.getPerfumesResponseModel;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
    //http://brook1.com/api/
    @FormUrlEncoded
    @POST("register/key/12345")
    Call<RegisterResponseModel> REGISTER_RESPONSE_MODEL_CALL(@Field("customer_group_id") String customer_group_id,
                                                             @Field("firstname") String firstname,
                                                             @Field("lastname") String lastname,
                                                             @Field("email") String email,
                                                             @Field("telephone") String telephone,
                                                             @Field("fax") String fax,
                                                             @Field("company") String company,
                                                             @Field("address_1") String address_1,
                                                             @Field("address_2") String address_2,
                                                             @Field("city") String city,
                                                             @Field("postcode") String postcode,
                                                             @Field("country_id") String country_id,
                                                             @Field("zone_id") String zone_id,
                                                             @Field("password") String password,
                                                             @Field("newsletter") String newsletter,
                                                             @Field("agree") String agree);

    @FormUrlEncoded
    @POST("login/key/12345")
    Call<LogInResponseModel> LOG_IN_RESPONSE_MODEL_CALL(@Field("email") String email,
                                                        @Field("password") String password);

    @FormUrlEncoded
    @POST("forgotten/key/12345")
    Call<ForgotPasswordResponseModel> FORGOT_PASSWORD_RESPONSE_MODEL_CALL(@Field("email") String email);

    @FormUrlEncoded
    @POST("editCustomer/key/12345")
    Call<EditProfileResponseModel> EDIT_PROFILE_RESPONSE_MODEL_CALL(@Field("firstname") String firstname,
                                                                    @Field("lastname") String lastname,
                                                                    @Field("email") String email,
                                                                    @Field("telephone") String telephone,
                                                                    @Field("fax") String fax,
                                                                    @Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("editPassword/key/12345")
    Call<EditPasswordResponseModel> EDIT_PASSWORD_RESPONSE_MODEL_CALL(@Field("email") String email,
                                                                      @Field("old_password") String old_password,
                                                                      @Field("new_password") String new_password);

    @GET("products/key/12345")
    Call<ProductsResponseModel> PRODUCTS_RESPONSE_MODEL_CALL();

    @GET("getbannerbyname/bannername/slideshow/key/12345")
    Call<SliderResponseModel> SLIDER_RESPONSE_MODEL_CALL();

    @GET("getCategories/key/12345")
    Call<SectionsResponseModel> SECTIONS_RESPONSE_MODEL_CALL();

    @GET("getLatest/limit/4/key/12345")
    Call<LatestResponseModel> LATEST_RESPONSE_MODEL_CALL();

    @GET("getSpecial/limit/20/key/12345/")
    Call<OffersResponseModel> OFFERS_RESPONSE_MODEL_CALL();

    @GET("getProduct/p_id/{p_id}/key/12345")
    Call<GetProductResponseModel> GET_PRODUCT_RESPONSE_MODEL_CALL(@Path("p_id") String p_id);

    @GET("getOptionbyid/p_id/{p_id}/key/12345")
    Call<ProductOptionsResponseModel> PRODUCT_OPTIONS_RESPONSE_MODEL_CALL(@Path("p_id") String p_id);

    @GET("getProdadditional/p_id/{p_id}/key/12345")
    Call<ImagesCommentsDiscountDataResponseModel> IMAGES_COMMENTS_DISCOUNT_DATA_RESPONSE_MODEL_CALL(@Path("p_id") String p_id);

    @GET("getrelaProduct/p_id/{p_id}/key/12345")
    Call<RelatedProductsResponseModel> GET_RELATED_PRODUCTS_RESPONSE_MODEL_CALL(@Path("p_id") String p_id);

//    @GET("products/category/{category_id}/key/12345")
//    Call<SectionProductsResponseModel> GET_CATEGORY_PRODUCTS_RESPONSE_MODEL_CALL(@Path("category_id") String category_id);

    @GET("http://www.collection-kw.com//index.php?route=feed/rest_api/productspagination&limit=20&key=12345")
    Call<SectionProductsResponseModel> GET_CATEGORY_PRODUCTS_RESPONSE_MODEL_CALL(
            @Query("category") String category_id,
            @Query("page") int page
    );
    @GET("getchildCategories/cate_id/{cate_id}/key/12345")
    Call<GetChildCategoryResponseModel> GET_CHILD_CATEGORY_RESPONSE_MODEL_CALL(@Path("cate_id") String category_id);

    @GET("getCountries/key/12345")
    Call<CountriesResponseModel> COUNTRIES_RESPONSE_MODEL_CALL();

    @GET("getZonesByCountryId/country_id/{country_id}/key/12345")
    Call<CountryZonesResponseModel> COUNTRY_ZONES_RESPONSE_MODEL_CALL(@Path("country_id") String country_id);

    @FormUrlEncoded
    @POST("getaddress/key/12345")
    Call<GetAddressesResponseModel> GET_ADDRESSES_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("addaddress/key/12345")
    Call<AddNewAddressResponseModel> ADD_NEW_ADDRESS_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id,
                                                                         @Field("firstname") String firstname,
                                                                         @Field("lastname") String lastname,
                                                                         @Field("company") String company,
                                                                         @Field("address_1") String address_1,
                                                                         @Field("address_2") String address_2,
                                                                         @Field("city") String city,
                                                                         @Field("postcode") String postcode,
                                                                         @Field("country_id") String country_id,
                                                                         @Field("zone_id") String zone_id,
                                                                         @Field("set_default") String set_default);

    @FormUrlEncoded
    @POST("editaddress/key/12345")
    Call<EditAddressResponseModel> EDIT_ADDRESS_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id,
                                                                    @Field("address_id") String address_id,
                                                                    @Field("firstname") String firstname,
                                                                    @Field("lastname") String lastname,
                                                                    @Field("company") String company,
                                                                    @Field("address_1") String address_1,
                                                                    @Field("address_2") String address_2,
                                                                    @Field("city") String city,
                                                                    @Field("postcode") String postcode,
                                                                    @Field("country_id") String country_id,
                                                                    @Field("zone_id") String zone_id,
                                                                    @Field("set_default") String set_default);

    @FormUrlEncoded
    @POST("deleteaddress/key/12345")
    Call<DeleteAddressResponseModel> DELETE_ADDRESS_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id,
                                                                        @Field("address_id") String address_id);

    @FormUrlEncoded
    @POST("getorderbycus/key/12345")
    Call<GetOrdersResponseModel> GET_ORDERS_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("getorderbyid/key/12345")
    Call<OneOrderResponseModel> ONE_ORDER_RESPONSE_MODEL_CALL(@Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("returnlist/key/12345")
    Call<ReturnedProductsListResponseModel> RETURNED_PRODUCTS_LIST_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("productreturn/key/12345")
    Call<ReturnProductResponseModel> RETURN_PRODUCT_RESPONSE_MODEL_CALL(@Field("firstname") String firstname,
                                                                        @Field("lastname") String lastname,
                                                                        @Field("email") String email,
                                                                        @Field("telephone") String telephone,
                                                                        @Field("order_id") String order_id,
                                                                        @Field("date_ordered") String date_ordered,
                                                                        @Field("product") String product,
                                                                        @Field("model") String model,
                                                                        @Field("quantity") String quantity,
                                                                        @Field("return_reason_id") String return_reason_id,
                                                                        @Field("opened") String opened,
                                                                        @Field("comment") String comment,
                                                                        @Field("customer_id") String customer_id);


//    @FormUrlEncoded
//    @POST("productreturn/key/12345")
//    Call<ReturnProductResponseModel> RETURN_PRODUCT_RESPONSE_MODEL_CALL(@Field("return_data") JSONObject return_data);

    @GET("getReturnReasons/key/12345")
    Call<GetReturnReasonsResponseModel> GET_RETURN_REASONS_RESPONSE_MODEL_CALL();

    @FormUrlEncoded
    @POST("addcart/key/12345")
    Call<AddProductToCartResponseModel> ADD_PRODUCT_TO_CART_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id,
                                                                                @Field("product_id") String product_id,
                                                                                @Field("quantity") String quantity,
                                                                                @Field("option[]") String[] option);

    @FormUrlEncoded
    @POST("cartproduct/key/12345")
    Call<ResponseBody> CART_PRODUCTS_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("wishlist/key/12345")
    Call<WishListResponseModel> WISH_LIST_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("addwishlist/key/12345")
    Call<AddToWishListResponseModel> ADD_TO_WISH_LIST_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id,
                                                                          @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST("removewishlist/key/12345")
    Call<RemoveFromWishListResponseModel> REMOVE_FROM_WISH_LIST_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id,
                                                                                    @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST("addorder/key/12345")
    Call<AddOrderResponseModel> ADD_ORDER_RESPONSE_MODEL_CALL(@Field("addorder") JSONObject addOrderModuleBody);

    @FormUrlEncoded
    @POST("clearcart/key/12345")
    Call<ClearCartResponseModel> CLEAR_CART_RESPONSE_MODEL_CALL(@Field("customer_id") String customer_id);

    @GET("getBestseller/limit/4/key/12345")
    Call<getBestsellerResponseModel> GET_BESTSELLER_RESPONSE_MODEL_CALL();

    @GET("getFeature/limit/16/mod_name/featured/key/12345")
    Call<getFeaturedResponseModel> GET_FEATURED_RESPONSE_MODEL_CALL();

    @GET("products/category/24/key/12345")
    Call<getPerfumesResponseModel> GET_PERFUMS_RESPONSE_MODEL_CALL();

    @GET("getbannerbyname/bannername/company/key/12345")
    Call<SliderBottomBannarResponseModel> SLIDER_BOTTOM_BANNAR_RESPONSE_MODEL_CALL();

    @GET("index.php?route=checkout/shippingmethod&json=1")
    Call<ShippingMethodResponseModel> SHIPPING_METHOD_RESPONSE_MODEL_CALL();

    @GET("index.php?route=checkout/paymentmethod&json=1")
    Call<PaymentMethodResponseModel> PAYMENT_METHOD_RESPONSE_MODEL_CALL();
}
