package net.collection.collection.Networks.WebServices;

import android.util.Log;

import net.collection.collection.Networks.Api.ApiClient;
import net.collection.collection.Networks.Api.ApiInterface;
import net.collection.collection.Networks.Events.AddNNewAddressEvent;
import net.collection.collection.Networks.Events.CountriesEvent;
import net.collection.collection.Networks.Events.CountryZonesEvent;
import net.collection.collection.Networks.Events.DeleteAddressEvent;
import net.collection.collection.Networks.Events.EditAddressEvent;
import net.collection.collection.Networks.Events.GetAddressesEvent;
import net.collection.collection.Networks.Events.ImagesCommentsDiscountDataEvent;
import net.collection.collection.Networks.ResponseModels.AddNewAddressResponseModel;
import net.collection.collection.Networks.ResponseModels.CountriesResponseModel;
import net.collection.collection.Networks.ResponseModels.CountryZonesResponseModel;
import net.collection.collection.Networks.ResponseModels.DeleteAddressResponseModel;
import net.collection.collection.Networks.ResponseModels.EditAddressResponseModel;
import net.collection.collection.Networks.ResponseModels.GetAddressesResponseModel;
import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressesWebService {
    public static final String TAG = AddressesWebService.class.getName();

    public static void getCountries() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<CountriesResponseModel> call = apiService.COUNTRIES_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<CountriesResponseModel>() {
            @Override
            public void onResponse(Call<CountriesResponseModel> call, Response<CountriesResponseModel> response) {
                EventBus.getDefault().post(new CountriesEvent(response.body()));
            }

            @Override
            public void onFailure(Call<CountriesResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void getCountryZones(String country_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<CountryZonesResponseModel> call = apiService.COUNTRY_ZONES_RESPONSE_MODEL_CALL(country_id);
        call.enqueue(new Callback<CountryZonesResponseModel>() {
            @Override
            public void onResponse(Call<CountryZonesResponseModel> call, Response<CountryZonesResponseModel> response) {
                EventBus.getDefault().post(new CountryZonesEvent(response.body()));
            }

            @Override
            public void onFailure(Call<CountryZonesResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void getAddresses(String customer_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<GetAddressesResponseModel> call = apiService.GET_ADDRESSES_RESPONSE_MODEL_CALL(customer_id);
        call.enqueue(new Callback<GetAddressesResponseModel>() {
            @Override
            public void onResponse(Call<GetAddressesResponseModel> call, Response<GetAddressesResponseModel> response) {
                EventBus.getDefault().post(new GetAddressesEvent(response.body()));
            }

            @Override
            public void onFailure(Call<GetAddressesResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void addNewAddress(String customer_id, String firstname, String lastname, String company, String address_1,
                                     String address_2, String city, String postcode, String country_id, String zone_id, String set_default) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<AddNewAddressResponseModel> call = apiService.ADD_NEW_ADDRESS_RESPONSE_MODEL_CALL(customer_id, firstname, lastname, company,
                address_1, address_2, city, postcode, country_id, zone_id, set_default);
        call.enqueue(new Callback<AddNewAddressResponseModel>() {
            @Override
            public void onResponse(Call<AddNewAddressResponseModel> call, Response<AddNewAddressResponseModel> response) {
                EventBus.getDefault().post(new AddNNewAddressEvent(response.body()));
            }

            @Override
            public void onFailure(Call<AddNewAddressResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void editAddress(String customer_id, String address_id, String firstname, String lastname, String company, String address_1,
                                     String address_2, String city, String postcode, String country_id, String zone_id, String set_default) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<EditAddressResponseModel> call = apiService.EDIT_ADDRESS_RESPONSE_MODEL_CALL(customer_id, address_id, firstname, lastname, company,
                address_1, address_2, city, postcode, country_id, zone_id, set_default);
        call.enqueue(new Callback<EditAddressResponseModel>() {
            @Override
            public void onResponse(Call<EditAddressResponseModel> call, Response<EditAddressResponseModel> response) {
                EventBus.getDefault().post(new EditAddressEvent(response.body()));
            }

            @Override
            public void onFailure(Call<EditAddressResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void deleteAddress(String customer_id, String address_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<DeleteAddressResponseModel> call = apiService.DELETE_ADDRESS_RESPONSE_MODEL_CALL(customer_id, address_id);
        call.enqueue(new Callback<DeleteAddressResponseModel>() {
            @Override
            public void onResponse(Call<DeleteAddressResponseModel> call, Response<DeleteAddressResponseModel> response) {
                EventBus.getDefault().post(new DeleteAddressEvent(response.body()));
            }

            @Override
            public void onFailure(Call<DeleteAddressResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
}
