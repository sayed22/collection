package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.PaymentMethodResponseModel;

public class PaymentMethodEvent {
    private PaymentMethodResponseModel paymentMethodResponseModel;

    public PaymentMethodEvent(PaymentMethodResponseModel paymentMethodResponseModel) {
        this.paymentMethodResponseModel = paymentMethodResponseModel;
    }

    public PaymentMethodResponseModel getPaymentMethodResponseModel() {
        return paymentMethodResponseModel;
    }

    public void setPaymentMethodResponseModel(PaymentMethodResponseModel paymentMethodResponseModel) {
        this.paymentMethodResponseModel = paymentMethodResponseModel;
    }
}
