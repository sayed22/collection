package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.AddOrderResponseModel;

public class AddOrderEvent {
    private AddOrderResponseModel addOrderResponseModel;

    public AddOrderEvent(AddOrderResponseModel addOrderResponseModel) {
        this.addOrderResponseModel = addOrderResponseModel;
    }

    public AddOrderResponseModel getAddOrderResponseModel() {
        return addOrderResponseModel;
    }

    public void setAddOrderResponseModel(AddOrderResponseModel addOrderResponseModel) {
        this.addOrderResponseModel = addOrderResponseModel;
    }
}
