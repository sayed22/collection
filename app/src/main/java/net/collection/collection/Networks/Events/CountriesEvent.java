package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.CountriesResponseModel;

public class CountriesEvent {
    private CountriesResponseModel countriesResponseModel;

    public CountriesEvent(CountriesResponseModel countriesResponseModel) {
        this.countriesResponseModel = countriesResponseModel;
    }

    public CountriesResponseModel getCountriesResponseModel() {
        return countriesResponseModel;
    }

    public void setCountriesResponseModel(CountriesResponseModel countriesResponseModel) {
        this.countriesResponseModel = countriesResponseModel;
    }
}
