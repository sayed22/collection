package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.EditAddressResponseModel;

public class EditAddressEvent {
    private EditAddressResponseModel editAddressResponseModel;

    public EditAddressEvent(EditAddressResponseModel editAddressResponseModel) {
        this.editAddressResponseModel = editAddressResponseModel;
    }

    public EditAddressResponseModel getEditAddressResponseModel() {
        return editAddressResponseModel;
    }

    public void setEditAddressResponseModel(EditAddressResponseModel editAddressResponseModel) {
        this.editAddressResponseModel = editAddressResponseModel;
    }
}
