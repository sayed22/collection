package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.OffersResponseModel;

public class OffersEvent {
    private OffersResponseModel offersResponseModel;

    public OffersEvent(OffersResponseModel offersResponseModel) {
        this.offersResponseModel = offersResponseModel;
    }

    public OffersResponseModel getOffersResponseModel() {
        return offersResponseModel;
    }

    public void setOffersResponseModel(OffersResponseModel offersResponseModel) {
        this.offersResponseModel = offersResponseModel;
    }
}
