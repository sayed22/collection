package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.SectionProductsResponseModel;

public class SectionProductsEvent {
    private SectionProductsResponseModel sectionProductsResponseModel;

    public SectionProductsEvent(SectionProductsResponseModel sectionProductsResponseModel) {
        this.sectionProductsResponseModel = sectionProductsResponseModel;
    }

    public SectionProductsResponseModel getSectionProductsResponseModel() {
        return sectionProductsResponseModel;
    }

    public void setSectionProductsResponseModel(SectionProductsResponseModel sectionProductsResponseModel) {
        this.sectionProductsResponseModel = sectionProductsResponseModel;
    }
}
