package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.DeleteAddressResponseModel;

public class DeleteAddressEvent {
    private DeleteAddressResponseModel deleteAddressResponseModel;

    public DeleteAddressEvent(DeleteAddressResponseModel deleteAddressResponseModel) {
        this.deleteAddressResponseModel = deleteAddressResponseModel;
    }

    public DeleteAddressResponseModel getDeleteAddressResponseModel() {
        return deleteAddressResponseModel;
    }

    public void setDeleteAddressResponseModel(DeleteAddressResponseModel deleteAddressResponseModel) {
        this.deleteAddressResponseModel = deleteAddressResponseModel;
    }
}
