package net.collection.collection.Networks.WebServices;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

import net.collection.collection.Models.AddOrderModuleBody;
import net.collection.collection.Networks.Api.ApiClient;
import net.collection.collection.Networks.Api.ApiInterface;
import net.collection.collection.Networks.Events.AddOrderEvent;
import net.collection.collection.Networks.Events.GetOrdersEvent;
import net.collection.collection.Networks.Events.GetReturnReasonEvent;
import net.collection.collection.Networks.Events.OneOrderEvent;
import net.collection.collection.Networks.Events.ReturnProductEvent;
import net.collection.collection.Networks.Events.ReturnedProductsListEvent;
import net.collection.collection.Networks.ResponseModels.AddOrderResponseModel;
import net.collection.collection.Networks.ResponseModels.GetOrdersResponseModel;
import net.collection.collection.Networks.ResponseModels.GetReturnReasonsResponseModel;
import net.collection.collection.Networks.ResponseModels.OneOrderResponseModel;
import net.collection.collection.Networks.ResponseModels.ReturnProductResponseModel;
import net.collection.collection.Networks.ResponseModels.ReturnedProductsListResponseModel;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersWebService {
    public static final String TAG = AddressesWebService.class.getName();

    public static void getOrders(String customer_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<GetOrdersResponseModel> call = apiService.GET_ORDERS_RESPONSE_MODEL_CALL(customer_id);
        call.enqueue(new Callback<GetOrdersResponseModel>() {
            @Override
            public void onResponse(Call<GetOrdersResponseModel> call, Response<GetOrdersResponseModel> response) {
                EventBus.getDefault().post(new GetOrdersEvent(response.body()));
            }

            @Override
            public void onFailure(Call<GetOrdersResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void addOrder(JSONObject addOrderModuleBody) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<AddOrderResponseModel> call = apiService.ADD_ORDER_RESPONSE_MODEL_CALL(addOrderModuleBody);
        call.enqueue(new Callback<AddOrderResponseModel>() {
            @Override
            public void onResponse(Call<AddOrderResponseModel> call, Response<AddOrderResponseModel> response) {
                EventBus.getDefault().post(new AddOrderEvent(response.body()));
            }

            @Override
            public void onFailure(Call<AddOrderResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void getOneOrder(String order_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<OneOrderResponseModel> call = apiService.ONE_ORDER_RESPONSE_MODEL_CALL(order_id);
        call.enqueue(new Callback<OneOrderResponseModel>() {
            @Override
            public void onResponse(Call<OneOrderResponseModel> call, Response<OneOrderResponseModel> response) {
                EventBus.getDefault().post(new OneOrderEvent(response.body()));
            }

            @Override
            public void onFailure(Call<OneOrderResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void returnedProductsList(String customer_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ReturnedProductsListResponseModel> call = apiService.RETURNED_PRODUCTS_LIST_RESPONSE_MODEL_CALL(customer_id);
        call.enqueue(new Callback<ReturnedProductsListResponseModel>() {
            @Override
            public void onResponse(Call<ReturnedProductsListResponseModel> call, Response<ReturnedProductsListResponseModel> response) {
                EventBus.getDefault().post(new ReturnedProductsListEvent(response.body()));
            }

            @Override
            public void onFailure(Call<ReturnedProductsListResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void returnProduct(String firstname, String lastname, String email, String telephone, String order_id,
                                     String date_ordered, String product, String model, String quantity,
                                     String return_reason_id, String opened, String comment, String customer_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ReturnProductResponseModel> call = apiService.RETURN_PRODUCT_RESPONSE_MODEL_CALL(firstname, lastname, email, telephone, order_id,
                date_ordered, product, model, quantity,
                return_reason_id, opened, comment, customer_id);
        call.enqueue(new Callback<ReturnProductResponseModel>() {
            @Override
            public void onResponse(Call<ReturnProductResponseModel> call, Response<ReturnProductResponseModel> response) {
                EventBus.getDefault().post(new ReturnProductEvent(response.body()));
                //Log.d(TAG, "Resend Activation Response " + new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));

                System.out.println("response body : " + new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
            }

            @Override
            public void onFailure(Call<ReturnProductResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    //    public static void returnProduct(JSONObject return_data) {
//        ApiInterface apiService =
//                ApiClient.getClient().create(ApiInterface.class);
//        Call<ReturnProductResponseModel> call = apiService.RETURN_PRODUCT_RESPONSE_MODEL_CALL(return_data);
//        call.enqueue(new Callback<ReturnProductResponseModel>() {
//            @Override
//            public void onResponse(Call<ReturnProductResponseModel> call, Response<ReturnProductResponseModel> response) {
//                EventBus.getDefault().post(new ReturnProductEvent(response.body()));
//                //Log.d(TAG, "Resend Activation Response " + new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
//
//                System.out.println("response body : " +  new GsonBuilder().setPrettyPrinting().create().toJson(response.body()));
//            }
//
//            @Override
//            public void onFailure(Call<ReturnProductResponseModel> call, Throwable t) {
//                // Log error here since request failed
//                Log.e(TAG, t.toString());
//                System.out.println("error : " + t.toString());
//            }
//        });
//    }
    public static void getRturnReason() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<GetReturnReasonsResponseModel> call = apiService.GET_RETURN_REASONS_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<GetReturnReasonsResponseModel>() {
            @Override
            public void onResponse(Call<GetReturnReasonsResponseModel> call, Response<GetReturnReasonsResponseModel> response) {
                EventBus.getDefault().post(new GetReturnReasonEvent(response.body()));
            }

            @Override
            public void onFailure(Call<GetReturnReasonsResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
}
