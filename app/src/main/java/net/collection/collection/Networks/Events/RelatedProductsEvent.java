package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.RelatedProductsResponseModel;

public class RelatedProductsEvent {
    private RelatedProductsResponseModel relatedProductsResponseModel;

    public RelatedProductsEvent(RelatedProductsResponseModel relatedProductsResponseModel) {
        this.relatedProductsResponseModel = relatedProductsResponseModel;
    }

    public RelatedProductsResponseModel getRelatedProductsResponseModel() {
        return relatedProductsResponseModel;
    }

    public void setRelatedProductsResponseModel(RelatedProductsResponseModel relatedProductsResponseModel) {
        this.relatedProductsResponseModel = relatedProductsResponseModel;
    }
}
