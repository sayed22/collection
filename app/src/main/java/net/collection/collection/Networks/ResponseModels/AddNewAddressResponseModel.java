package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddNewAddressResponseModel {

    @Expose
    @SerializedName("addressesdata")
    private Addressesdata addressesdata;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Addressesdata getAddressesdata() {
        return addressesdata;
    }

    public void setAddressesdata(Addressesdata addressesdata) {
        this.addressesdata = addressesdata;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Addressesdata {
        @Expose
        @SerializedName("custom_field")
        private boolean customField;
        @Expose
        @SerializedName("address_format")
        private String addressFormat;
        @Expose
        @SerializedName("iso_code_3")
        private String isoCode3;
        @Expose
        @SerializedName("iso_code_2")
        private String isoCode2;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("country_id")
        private String countryId;
        @Expose
        @SerializedName("zone_code")
        private String zoneCode;
        @Expose
        @SerializedName("zone")
        private String zone;
        @Expose
        @SerializedName("zone_id")
        private String zoneId;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("postcode")
        private String postcode;
        @Expose
        @SerializedName("address_2")
        private String address2;
        @Expose
        @SerializedName("address_1")
        private String address1;
        @Expose
        @SerializedName("company")
        private String company;
        @Expose
        @SerializedName("lastname")
        private String lastname;
        @Expose
        @SerializedName("firstname")
        private String firstname;
        @Expose
        @SerializedName("address_id")
        private String addressId;

        public boolean getCustomField() {
            return customField;
        }

        public void setCustomField(boolean customField) {
            this.customField = customField;
        }

        public String getAddressFormat() {
            return addressFormat;
        }

        public void setAddressFormat(String addressFormat) {
            this.addressFormat = addressFormat;
        }

        public String getIsoCode3() {
            return isoCode3;
        }

        public void setIsoCode3(String isoCode3) {
            this.isoCode3 = isoCode3;
        }

        public String getIsoCode2() {
            return isoCode2;
        }

        public void setIsoCode2(String isoCode2) {
            this.isoCode2 = isoCode2;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getZoneCode() {
            return zoneCode;
        }

        public void setZoneCode(String zoneCode) {
            this.zoneCode = zoneCode;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getZoneId() {
            return zoneId;
        }

        public void setZoneId(String zoneId) {
            this.zoneId = zoneId;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getAddressId() {
            return addressId;
        }

        public void setAddressId(String addressId) {
            this.addressId = addressId;
        }
    }
}
