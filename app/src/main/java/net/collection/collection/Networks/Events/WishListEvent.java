package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.WishListResponseModel;

public class WishListEvent {
    private WishListResponseModel wishListResponseModel;

    public WishListEvent(WishListResponseModel wishListResponseModel) {
        this.wishListResponseModel = wishListResponseModel;
    }

    public WishListResponseModel getWishListResponseModel() {
        return wishListResponseModel;
    }

    public void setWishListResponseModel(WishListResponseModel wishListResponseModel) {
        this.wishListResponseModel = wishListResponseModel;
    }
}
