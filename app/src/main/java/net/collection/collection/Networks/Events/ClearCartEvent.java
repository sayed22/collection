package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.ClearCartResponseModel;

public class ClearCartEvent {
    private ClearCartResponseModel clearCartResponseModel;

    public ClearCartEvent(ClearCartResponseModel clearCartResponseModel) {
        this.clearCartResponseModel = clearCartResponseModel;
    }

    public ClearCartResponseModel getClearCartResponseModel() {
        return clearCartResponseModel;
    }

    public void setClearCartResponseModel(ClearCartResponseModel clearCartResponseModel) {
        this.clearCartResponseModel = clearCartResponseModel;
    }

}
