package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetOrdersResponseModel {

    @Expose
    @SerializedName("orders")
    private List<Orders> orders;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Orders> getOrders() {
        return orders;
    }

    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Orders {
        @Expose
        @SerializedName("currency_value")
        private String currencyValue;
        @Expose
        @SerializedName("currency_code")
        private String currencyCode;
        @Expose
        @SerializedName("total")
        private String total;
        @Expose
        @SerializedName("products")
        private int products;
        @Expose
        @SerializedName("date_added")
        private String dateAdded;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("order_id")
        private String orderId;

        public String getCurrencyValue() {
            return currencyValue;
        }

        public void setCurrencyValue(String currencyValue) {
            this.currencyValue = currencyValue;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public int getProducts() {
            return products;
        }

        public void setProducts(int products) {
            this.products = products;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }
    }
}
