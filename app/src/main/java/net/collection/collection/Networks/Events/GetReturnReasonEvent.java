package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.GetReturnReasonsResponseModel;

public class GetReturnReasonEvent {
    private GetReturnReasonsResponseModel getReturnReasonsResponseModel;

    public GetReturnReasonEvent(GetReturnReasonsResponseModel getReturnReasonsResponseModel) {
        this.getReturnReasonsResponseModel = getReturnReasonsResponseModel;
    }

    public GetReturnReasonsResponseModel getGetReturnReasonsResponseModel() {
        return getReturnReasonsResponseModel;
    }

    public void setGetReturnReasonsResponseModel(GetReturnReasonsResponseModel getReturnReasonsResponseModel) {
        this.getReturnReasonsResponseModel = getReturnReasonsResponseModel;
    }
}
