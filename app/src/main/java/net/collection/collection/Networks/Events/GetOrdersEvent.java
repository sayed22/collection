package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.GetOrdersResponseModel;

public class GetOrdersEvent {
    private GetOrdersResponseModel getOrdersResponseModel;

    public GetOrdersEvent(GetOrdersResponseModel getOrdersResponseModel) {
        this.getOrdersResponseModel = getOrdersResponseModel;
    }

    public GetOrdersResponseModel getGetOrdersResponseModel() {
        return getOrdersResponseModel;
    }

    public void setGetOrdersResponseModel(GetOrdersResponseModel getOrdersResponseModel) {
        this.getOrdersResponseModel = getOrdersResponseModel;
    }
}
