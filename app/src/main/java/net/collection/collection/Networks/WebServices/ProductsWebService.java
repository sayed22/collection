package net.collection.collection.Networks.WebServices;

import android.util.Log;

import net.collection.collection.Networks.Api.ApiClient;
import net.collection.collection.Networks.Api.ApiInterface;
import net.collection.collection.Networks.Events.GetChildCategoryEvent;
import net.collection.collection.Networks.Events.ImagesCommentsDiscountDataEvent;
import net.collection.collection.Networks.Events.ProductOptionsEvent;
import net.collection.collection.Networks.Events.SectionProductsEvent;
import net.collection.collection.Networks.Events.GetProductEvent;
import net.collection.collection.Networks.Events.RelatedProductsEvent;
import net.collection.collection.Networks.Events.LatestEvent;
import net.collection.collection.Networks.Events.OffersEvent;
import net.collection.collection.Networks.Events.ProductsEvent;
import net.collection.collection.Networks.Events.SectionsEvent;
import net.collection.collection.Networks.Events.SliderBottomBannarEvent;
import net.collection.collection.Networks.Events.SliderEvent;
import net.collection.collection.Networks.Events.getBestsellerEvent;
import net.collection.collection.Networks.Events.getFeaturedEvent;
import net.collection.collection.Networks.Events.getPerfumesEvent;
import net.collection.collection.Networks.ResponseModels.GetChildCategoryResponseModel;
import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.GetProductResponseModel;
import net.collection.collection.Networks.ResponseModels.RelatedProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.LatestResponseModel;
import net.collection.collection.Networks.ResponseModels.OffersResponseModel;
import net.collection.collection.Networks.ResponseModels.ProductsResponseModel;
import net.collection.collection.Networks.ResponseModels.SectionsResponseModel;
import net.collection.collection.Networks.ResponseModels.SliderBottomBannarResponseModel;
import net.collection.collection.Networks.ResponseModels.SliderResponseModel;
import net.collection.collection.Networks.ResponseModels.getBestsellerResponseModel;
import net.collection.collection.Networks.ResponseModels.getFeaturedResponseModel;
import net.collection.collection.Networks.ResponseModels.getPerfumesResponseModel;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsWebService {
    public static final String TAG = ProductsWebService.class.getName();

    public static void getBestSeller() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<getBestsellerResponseModel> call = apiService.GET_BESTSELLER_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<getBestsellerResponseModel>() {
            @Override
            public void onResponse(Call<getBestsellerResponseModel> call, Response<getBestsellerResponseModel> response) {
                EventBus.getDefault().post(new getBestsellerEvent(response.body()));
            }

            @Override
            public void onFailure(Call<getBestsellerResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getFeatured() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<getFeaturedResponseModel> call = apiService.GET_FEATURED_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<getFeaturedResponseModel>() {
            @Override
            public void onResponse(Call<getFeaturedResponseModel> call, Response<getFeaturedResponseModel> response) {
                EventBus.getDefault().post(new getFeaturedEvent(response.body()));
            }

            @Override
            public void onFailure(Call<getFeaturedResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getPerfumes() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<getPerfumesResponseModel> call = apiService.GET_PERFUMS_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<getPerfumesResponseModel>() {
            @Override
            public void onResponse(Call<getPerfumesResponseModel> call, Response<getPerfumesResponseModel> response) {
                EventBus.getDefault().post(new getPerfumesEvent(response.body()));
            }

            @Override
            public void onFailure(Call<getPerfumesResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getProducts() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ProductsResponseModel> call = apiService.PRODUCTS_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<ProductsResponseModel>() {
            @Override
            public void onResponse(Call<ProductsResponseModel> call, Response<ProductsResponseModel> response) {
                EventBus.getDefault().post(new ProductsEvent(response.body()));
            }

            @Override
            public void onFailure(Call<ProductsResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getSliderBottomBannar() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<SliderBottomBannarResponseModel> call = apiService.SLIDER_BOTTOM_BANNAR_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<SliderBottomBannarResponseModel>() {
            @Override
            public void onResponse(Call<SliderBottomBannarResponseModel> call, Response<SliderBottomBannarResponseModel> response) {
                EventBus.getDefault().post(new SliderBottomBannarEvent(response.body()));
            }

            @Override
            public void onFailure(Call<SliderBottomBannarResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getSlider() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<SliderResponseModel> call = apiService.SLIDER_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<SliderResponseModel>() {
            @Override
            public void onResponse(Call<SliderResponseModel> call, Response<SliderResponseModel> response) {
                EventBus.getDefault().post(new SliderEvent(response.body()));
            }

            @Override
            public void onFailure(Call<SliderResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getSections() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<SectionsResponseModel> call = apiService.SECTIONS_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<SectionsResponseModel>() {
            @Override
            public void onResponse(Call<SectionsResponseModel> call, Response<SectionsResponseModel> response) {
                EventBus.getDefault().post(new SectionsEvent(response.body()));
            }

            @Override
            public void onFailure(Call<SectionsResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getLatest() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<LatestResponseModel> call = apiService.LATEST_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<LatestResponseModel>() {
            @Override
            public void onResponse(Call<LatestResponseModel> call, Response<LatestResponseModel> response) {
                EventBus.getDefault().post(new LatestEvent(response.body()));
            }

            @Override
            public void onFailure(Call<LatestResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getOffers() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<OffersResponseModel> call = apiService.OFFERS_RESPONSE_MODEL_CALL();
        call.enqueue(new Callback<OffersResponseModel>() {
            @Override
            public void onResponse(Call<OffersResponseModel> call, Response<OffersResponseModel> response) {
                EventBus.getDefault().post(new OffersEvent(response.body()));
            }

            @Override
            public void onFailure(Call<OffersResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void getProduct(String p_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<GetProductResponseModel> call = apiService.GET_PRODUCT_RESPONSE_MODEL_CALL(p_id);
        call.enqueue(new Callback<GetProductResponseModel>() {
            @Override
            public void onResponse(Call<GetProductResponseModel> call, Response<GetProductResponseModel> response) {
                EventBus.getDefault().post(new GetProductEvent(response.body()));
            }

            @Override
            public void onFailure(Call<GetProductResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void getProductOptions(String p_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ProductOptionsResponseModel> call = apiService.PRODUCT_OPTIONS_RESPONSE_MODEL_CALL(p_id);
        call.enqueue(new Callback<ProductOptionsResponseModel>() {
            @Override
            public void onResponse(Call<ProductOptionsResponseModel> call, Response<ProductOptionsResponseModel> response) {
                EventBus.getDefault().post(new ProductOptionsEvent(response.body()));
            }

            @Override
            public void onFailure(Call<ProductOptionsResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getImagesCommentsDiscountData(String p_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ImagesCommentsDiscountDataResponseModel> call = apiService.IMAGES_COMMENTS_DISCOUNT_DATA_RESPONSE_MODEL_CALL(p_id);
        call.enqueue(new Callback<ImagesCommentsDiscountDataResponseModel>() {
            @Override
            public void onResponse(Call<ImagesCommentsDiscountDataResponseModel> call, Response<ImagesCommentsDiscountDataResponseModel> response) {
                EventBus.getDefault().post(new ImagesCommentsDiscountDataEvent(response.body()));
            }

            @Override
            public void onFailure(Call<ImagesCommentsDiscountDataResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getRelatedProduct(String p_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<RelatedProductsResponseModel> call = apiService.GET_RELATED_PRODUCTS_RESPONSE_MODEL_CALL(p_id);
        call.enqueue(new Callback<RelatedProductsResponseModel>() {
            @Override
            public void onResponse(Call<RelatedProductsResponseModel> call, Response<RelatedProductsResponseModel> response) {
                EventBus.getDefault().post(new RelatedProductsEvent(response.body()));
            }

            @Override
            public void onFailure(Call<RelatedProductsResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }

    public static void getSectionProducts(String category_id,int page) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<SectionProductsResponseModel> call = apiService.GET_CATEGORY_PRODUCTS_RESPONSE_MODEL_CALL(category_id, page);
        call.enqueue(new Callback<SectionProductsResponseModel>() {
            @Override
            public void onResponse(Call<SectionProductsResponseModel> call, Response<SectionProductsResponseModel> response) {
                EventBus.getDefault().post(new SectionProductsEvent(response.body()));
            }

            @Override
            public void onFailure(Call<SectionProductsResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
    public static void getSectionCategory(String cate_id) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<GetChildCategoryResponseModel> call = apiService.GET_CHILD_CATEGORY_RESPONSE_MODEL_CALL(cate_id);
        call.enqueue(new Callback<GetChildCategoryResponseModel>() {
            @Override
            public void onResponse(Call<GetChildCategoryResponseModel> call, Response<GetChildCategoryResponseModel> response) {
                EventBus.getDefault().post(new GetChildCategoryEvent(response.body()));
            }

            @Override
            public void onFailure(Call<GetChildCategoryResponseModel> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                System.out.println("error : " + t.toString());
            }
        });
    }
}
