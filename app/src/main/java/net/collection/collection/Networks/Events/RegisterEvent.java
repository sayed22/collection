package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.RegisterResponseModel;

public class RegisterEvent {
    private RegisterResponseModel registerResponseModel;

    public RegisterEvent(RegisterResponseModel registerResponseModel) {
        this.registerResponseModel = registerResponseModel;
    }

    public RegisterResponseModel getRegisterResponseModel() {
        return registerResponseModel;
    }

    public void setRegisterResponseModel(RegisterResponseModel registerResponseModel) {
        this.registerResponseModel = registerResponseModel;
    }
}
