package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SliderBottomBannarResponseModel {

    @Expose
    @SerializedName("banners")
    private List<Banners> banners;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Banners> getBanners() {
        return banners;
    }

    public void setBanners(List<Banners> banners) {
        this.banners = banners;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Banners {
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("language_id")
        private String languageId;
        @Expose
        @SerializedName("sort_order")
        private String sortOrder;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("link")
        private String link;
        @Expose
        @SerializedName("banner_id")
        private String bannerId;
        @Expose
        @SerializedName("banner_image_id")
        private String bannerImageId;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLanguageId() {
            return languageId;
        }

        public void setLanguageId(String languageId) {
            this.languageId = languageId;
        }

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getBannerId() {
            return bannerId;
        }

        public void setBannerId(String bannerId) {
            this.bannerId = bannerId;
        }

        public String getBannerImageId() {
            return bannerImageId;
        }

        public void setBannerImageId(String bannerImageId) {
            this.bannerImageId = bannerImageId;
        }
    }
}
