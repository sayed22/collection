package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.getPerfumesResponseModel;

public class getPerfumesEvent {
    private getPerfumesResponseModel getPerfumesResponseModel;

    public getPerfumesEvent(getPerfumesResponseModel getPerfumesResponseModel) {
        this.getPerfumesResponseModel = getPerfumesResponseModel;
    }

    public getPerfumesResponseModel getGetPerfumesResponseModel() {
        return getPerfumesResponseModel;
    }

    public void setGetPerfumesResponseModel(getPerfumesResponseModel getPerfumesResponseModel) {
        this.getPerfumesResponseModel = getPerfumesResponseModel;
    }
}
