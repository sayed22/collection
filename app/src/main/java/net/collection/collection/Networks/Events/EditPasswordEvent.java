package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.EditPasswordResponseModel;

public class EditPasswordEvent {
    private EditPasswordResponseModel editPasswordResponseModel;

    public EditPasswordEvent(EditPasswordResponseModel editPasswordResponseModel) {
        this.editPasswordResponseModel = editPasswordResponseModel;
    }

    public EditPasswordResponseModel getEditPasswordResponseModel() {
        return editPasswordResponseModel;
    }

    public void setEditPasswordResponseModel(EditPasswordResponseModel editPasswordResponseModel) {
        this.editPasswordResponseModel = editPasswordResponseModel;
    }
}
