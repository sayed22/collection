package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.SliderResponseModel;

public class SliderEvent {
    private SliderResponseModel sliderResponseModel;

    public SliderEvent(SliderResponseModel sliderResponseModel) {
        this.sliderResponseModel = sliderResponseModel;
    }

    public SliderResponseModel getSliderResponseModel() {
        return sliderResponseModel;
    }

    public void setSliderResponseModel(SliderResponseModel sliderResponseModel) {
        this.sliderResponseModel = sliderResponseModel;
    }
}
