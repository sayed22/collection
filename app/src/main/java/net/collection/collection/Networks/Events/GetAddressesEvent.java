package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.GetAddressesResponseModel;

public class GetAddressesEvent {
    private GetAddressesResponseModel getAddressesResponseModel;

    public GetAddressesEvent(GetAddressesResponseModel getAddressesResponseModel) {
        this.getAddressesResponseModel = getAddressesResponseModel;
    }

    public GetAddressesResponseModel getGetAddressesResponseModel() {
        return getAddressesResponseModel;
    }

    public void setGetAddressesResponseModel(GetAddressesResponseModel getAddressesResponseModel) {
        this.getAddressesResponseModel = getAddressesResponseModel;
    }
}
