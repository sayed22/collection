package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.collection.collection.Models.Userdata;

public class LogInResponseModel {

    @Expose
    @SerializedName("userdata")
    private Userdata userdata;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Userdata getUserdata() {
        return userdata;
    }

    public void setUserdata(Userdata userdata) {
        this.userdata = userdata;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
