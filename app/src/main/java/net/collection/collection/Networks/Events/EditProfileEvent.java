package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.EditProfileResponseModel;

public class EditProfileEvent {
    private EditProfileResponseModel editProfileResponseModel;

    public EditProfileEvent(EditProfileResponseModel editProfileResponseModel) {
        this.editProfileResponseModel = editProfileResponseModel;
    }

    public EditProfileResponseModel getEditProfileResponseModel() {
        return editProfileResponseModel;
    }

    public void setEditProfileResponseModel(EditProfileResponseModel editProfileResponseModel) {
        this.editProfileResponseModel = editProfileResponseModel;
    }
}
