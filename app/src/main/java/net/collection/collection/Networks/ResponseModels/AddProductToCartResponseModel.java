package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddProductToCartResponseModel {

    @Expose
    @SerializedName("cartproduct")
    private Cartproduct cartproduct;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Cartproduct getCartproduct() {
        return cartproduct;
    }

    public void setCartproduct(Cartproduct cartproduct) {
        this.cartproduct = cartproduct;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Cartproduct {
        @Expose
        @SerializedName("YToyOntzOjEwOiJwcm9kdWN0X2lkIjtpOjMwO3M6Njoib3B0aW9uIjthOjI6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjgiO319")
        private ProductData productData;
        public ProductData getProductData() {
            return productData;
        }

        public void setProductData(ProductData productData) {
            this.productData = productData;
        }
    }

    public static class ProductData {
        @Expose
        @SerializedName("recurring")
        private boolean recurring;
        @Expose
        @SerializedName("length_class_id")
        private String lengthClassId;
        @Expose
        @SerializedName("height")
        private String height;
        @Expose
        @SerializedName("width")
        private String width;
        @Expose
        @SerializedName("length")
        private String length;
        @Expose
        @SerializedName("weight_class_id")
        private String weightClassId;
        @Expose
        @SerializedName("weight")
        private int weight;
        @Expose
        @SerializedName("tax_class_id")
        private String taxClassId;
        @Expose
        @SerializedName("points")
        private int points;
        @Expose
        @SerializedName("reward")
        private int reward;
        @Expose
        @SerializedName("total")
        private double total;
        @Expose
        @SerializedName("price")
        private double price;
        @Expose
        @SerializedName("stock")
        private boolean stock;
        @Expose
        @SerializedName("subtract")
        private String subtract;
        @Expose
        @SerializedName("minimum")
        private String minimum;
        @Expose
        @SerializedName("quantity")
        private int quantity;
        @Expose
        @SerializedName("download")
        private List<Download> download;
        @Expose
        @SerializedName("option")
        private List<String> option;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("shipping")
        private String shipping;
        @Expose
        @SerializedName("model")
        private String model;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("product_id")
        private String productId;
        @Expose
        @SerializedName("key")
        private String key;

        public boolean getRecurring() {
            return recurring;
        }

        public void setRecurring(boolean recurring) {
            this.recurring = recurring;
        }

        public String getLengthClassId() {
            return lengthClassId;
        }

        public void setLengthClassId(String lengthClassId) {
            this.lengthClassId = lengthClassId;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getLength() {
            return length;
        }

        public void setLength(String length) {
            this.length = length;
        }

        public String getWeightClassId() {
            return weightClassId;
        }

        public void setWeightClassId(String weightClassId) {
            this.weightClassId = weightClassId;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public String getTaxClassId() {
            return taxClassId;
        }

        public void setTaxClassId(String taxClassId) {
            this.taxClassId = taxClassId;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public int getReward() {
            return reward;
        }

        public void setReward(int reward) {
            this.reward = reward;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public boolean getStock() {
            return stock;
        }

        public void setStock(boolean stock) {
            this.stock = stock;
        }

        public String getSubtract() {
            return subtract;
        }

        public void setSubtract(String subtract) {
            this.subtract = subtract;
        }

        public String getMinimum() {
            return minimum;
        }

        public void setMinimum(String minimum) {
            this.minimum = minimum;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public List<Download> getDownload() {
            return download;
        }

        public void setDownload(List<Download> download) {
            this.download = download;
        }

        public List<String> getOption() {
            return option;
        }

        public void setOption(List<String> option) {
            this.option = option;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getShipping() {
            return shipping;
        }

        public void setShipping(String shipping) {
            this.shipping = shipping;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }

    public static class Download {
        @Expose
        @SerializedName("mask")
        private String mask;
        @Expose
        @SerializedName("filename")
        private String filename;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("download_id")
        private String downloadId;

        public String getMask() {
            return mask;
        }

        public void setMask(String mask) {
            this.mask = mask;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDownloadId() {
            return downloadId;
        }

        public void setDownloadId(String downloadId) {
            this.downloadId = downloadId;
        }
    }

    public static class Option {
        @Expose
        @SerializedName("weight_prefix")
        private String weightPrefix;
        @Expose
        @SerializedName("weight")
        private String weight;
        @Expose
        @SerializedName("points_prefix")
        private String pointsPrefix;
        @Expose
        @SerializedName("points")
        private String points;
        @Expose
        @SerializedName("price_prefix")
        private String pricePrefix;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("subtract")
        private String subtract;
        @Expose
        @SerializedName("quantity")
        private String quantity;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("value")
        private String value;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("option_value_id")
        private String optionValueId;
        @Expose
        @SerializedName("option_id")
        private String optionId;
        @Expose
        @SerializedName("product_option_value_id")
        private String productOptionValueId;
        @Expose
        @SerializedName("product_option_id")
        private int productOptionId;

        public String getWeightPrefix() {
            return weightPrefix;
        }

        public void setWeightPrefix(String weightPrefix) {
            this.weightPrefix = weightPrefix;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getPointsPrefix() {
            return pointsPrefix;
        }

        public void setPointsPrefix(String pointsPrefix) {
            this.pointsPrefix = pointsPrefix;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getPricePrefix() {
            return pricePrefix;
        }

        public void setPricePrefix(String pricePrefix) {
            this.pricePrefix = pricePrefix;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getSubtract() {
            return subtract;
        }

        public void setSubtract(String subtract) {
            this.subtract = subtract;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOptionValueId() {
            return optionValueId;
        }

        public void setOptionValueId(String optionValueId) {
            this.optionValueId = optionValueId;
        }

        public String getOptionId() {
            return optionId;
        }

        public void setOptionId(String optionId) {
            this.optionId = optionId;
        }

        public String getProductOptionValueId() {
            return productOptionValueId;
        }

        public void setProductOptionValueId(String productOptionValueId) {
            this.productOptionValueId = productOptionValueId;
        }

        public int getProductOptionId() {
            return productOptionId;
        }

        public void setProductOptionId(int productOptionId) {
            this.productOptionId = productOptionId;
        }
    }
}
