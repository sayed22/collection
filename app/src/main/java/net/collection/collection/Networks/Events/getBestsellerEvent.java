package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.getBestsellerResponseModel;

public class getBestsellerEvent {
    private getBestsellerResponseModel getBestsellerResponseModel;

    public getBestsellerEvent(net.collection.collection.Networks.ResponseModels.getBestsellerResponseModel getBestsellerResponseModel) {
        this.getBestsellerResponseModel = getBestsellerResponseModel;
    }

    public net.collection.collection.Networks.ResponseModels.getBestsellerResponseModel getGetBestsellerResponseModel() {
        return getBestsellerResponseModel;
    }

    public void setGetBestsellerResponseModel(net.collection.collection.Networks.ResponseModels.getBestsellerResponseModel getBestsellerResponseModel) {
        this.getBestsellerResponseModel = getBestsellerResponseModel;
    }
}
