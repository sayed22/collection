package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.SectionsResponseModel;

public class SectionsEvent {
    private SectionsResponseModel sectionsResponseModel;

    public SectionsEvent(SectionsResponseModel sectionsResponseModel) {
        this.sectionsResponseModel = sectionsResponseModel;
    }

    public SectionsResponseModel getSectionsResponseModel() {
        return sectionsResponseModel;
    }

    public void setSectionsResponseModel(SectionsResponseModel sectionsResponseModel) {
        this.sectionsResponseModel = sectionsResponseModel;
    }
}
