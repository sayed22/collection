package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.ProductOptionsResponseModel;

public class ProductOptionsEvent {
    private ProductOptionsResponseModel productOptionsResponseModel;

    public ProductOptionsEvent(ProductOptionsResponseModel productOptionsResponseModel) {
        this.productOptionsResponseModel = productOptionsResponseModel;
    }

    public ProductOptionsResponseModel getProductOptionsResponseModel() {
        return productOptionsResponseModel;
    }

    public void setProductOptionsResponseModel(ProductOptionsResponseModel productOptionsResponseModel) {
        this.productOptionsResponseModel = productOptionsResponseModel;
    }
}
