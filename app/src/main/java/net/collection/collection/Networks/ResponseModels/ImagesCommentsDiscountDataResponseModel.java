package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ImagesCommentsDiscountDataResponseModel {

    @Expose
    @SerializedName("products_prodadditional")
    private ProductsProdadditional productsProdadditional;
    @Expose
    @SerializedName("success")
    private boolean success;

    public ProductsProdadditional getProductsProdadditional() {
        return productsProdadditional;
    }

    public void setProductsProdadditional(ProductsProdadditional productsProdadditional) {
        this.productsProdadditional = productsProdadditional;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class ProductsProdadditional {
        @Expose
        @SerializedName("discountsdata")
        private List<Discountsdata> discountsdata;
        @Expose
        @SerializedName("reviewsdata")
        private List<Reviewsdata> reviewsdata;
        @Expose
        @SerializedName("add_imgs")
        private List<AddImgs> addImgs;

        public List<Discountsdata> getDiscountsdata() {
            return discountsdata;
        }

        public void setDiscountsdata(List<Discountsdata> discountsdata) {
            this.discountsdata = discountsdata;
        }

        public List<Reviewsdata> getReviewsdata() {
            return reviewsdata;
        }

        public void setReviewsdata(List<Reviewsdata> reviewsdata) {
            this.reviewsdata = reviewsdata;
        }

        public List<AddImgs> getAddImgs() {
            return addImgs;
        }

        public void setAddImgs(List<AddImgs> addImgs) {
            this.addImgs = addImgs;
        }
    }

    public static class Discountsdata {
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("quantity")
        private String quantity;

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }
    }

    public static class Reviewsdata {
        @Expose
        @SerializedName("date_added")
        private String dateAdded;
        @Expose
        @SerializedName("rating")
        private int rating;
        @Expose
        @SerializedName("text")
        private String text;
        @Expose
        @SerializedName("author")
        private String author;

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }
    }

    public static class AddImgs {
        @Expose
        @SerializedName("sort_order")
        private String sortOrder;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("product_id")
        private String productId;
        @Expose
        @SerializedName("product_image_id")
        private String productImageId;

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductImageId() {
            return productImageId;
        }

        public void setProductImageId(String productImageId) {
            this.productImageId = productImageId;
        }
    }
}
