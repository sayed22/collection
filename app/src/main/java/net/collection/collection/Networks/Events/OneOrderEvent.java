package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.OneOrderResponseModel;

public class OneOrderEvent {
    private OneOrderResponseModel oneOrderResponseModel;

    public OneOrderEvent(OneOrderResponseModel oneOrderResponseModel) {
        this.oneOrderResponseModel = oneOrderResponseModel;
    }

    public OneOrderResponseModel getOneOrderResponseModel() {
        return oneOrderResponseModel;
    }

    public void setOneOrderResponseModel(OneOrderResponseModel oneOrderResponseModel) {
        this.oneOrderResponseModel = oneOrderResponseModel;
    }
}
