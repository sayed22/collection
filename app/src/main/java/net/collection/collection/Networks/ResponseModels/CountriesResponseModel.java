package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CountriesResponseModel {

    @Expose
    @SerializedName("Countries")
    private List<Countries> countries;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Countries> getCountries() {
        return countries;
    }

    public void setCountries(List<Countries> countries) {
        this.countries = countries;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Countries {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("postcode_required")
        private String postcodeRequired;
        @Expose
        @SerializedName("address_format")
        private String addressFormat;
        @Expose
        @SerializedName("iso_code_3")
        private String isoCode3;
        @Expose
        @SerializedName("iso_code_2")
        private String isoCode2;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("country_id")
        private String countryId;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPostcodeRequired() {
            return postcodeRequired;
        }

        public void setPostcodeRequired(String postcodeRequired) {
            this.postcodeRequired = postcodeRequired;
        }

        public String getAddressFormat() {
            return addressFormat;
        }

        public void setAddressFormat(String addressFormat) {
            this.addressFormat = addressFormat;
        }

        public String getIsoCode3() {
            return isoCode3;
        }

        public void setIsoCode3(String isoCode3) {
            this.isoCode3 = isoCode3;
        }

        public String getIsoCode2() {
            return isoCode2;
        }

        public void setIsoCode2(String isoCode2) {
            this.isoCode2 = isoCode2;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }
    }
}
