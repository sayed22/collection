package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OneOrderResponseModel {

    @Expose
    @SerializedName("orders")
    private Orders orders;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Orders {
        @Expose
        @SerializedName("historiesdata")
        private List<Historiesdata> historiesdata;
        @Expose
        @SerializedName("totals")
        private List<Totals> totals;
        @Expose
        @SerializedName("vouchersdata")
        private List<String> vouchersdata;
        @Expose
        @SerializedName("products")
        private List<Products> products;
        @Expose
        @SerializedName("order_info")
        private OrderInfo orderInfo;

        public List<Historiesdata> getHistoriesdata() {
            return historiesdata;
        }

        public void setHistoriesdata(List<Historiesdata> historiesdata) {
            this.historiesdata = historiesdata;
        }

        public List<Totals> getTotals() {
            return totals;
        }

        public void setTotals(List<Totals> totals) {
            this.totals = totals;
        }

        public List<String> getVouchersdata() {
            return vouchersdata;
        }

        public void setVouchersdata(List<String> vouchersdata) {
            this.vouchersdata = vouchersdata;
        }

        public List<Products> getProducts() {
            return products;
        }

        public void setProducts(List<Products> products) {
            this.products = products;
        }

        public OrderInfo getOrderInfo() {
            return orderInfo;
        }

        public void setOrderInfo(OrderInfo orderInfo) {
            this.orderInfo = orderInfo;
        }
    }

    public static class Historiesdata {
        @Expose
        @SerializedName("comment")
        private String comment;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("date_added")
        private String dateAdded;

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }
    }

    public static class Totals {
        @Expose
        @SerializedName("sort_order")
        private String sortOrder;
        @Expose
        @SerializedName("value")
        private String value;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("code")
        private String code;
        @Expose
        @SerializedName("order_id")
        private String orderId;
        @Expose
        @SerializedName("order_total_id")
        private String orderTotalId;

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderTotalId() {
            return orderTotalId;
        }

        public void setOrderTotalId(String orderTotalId) {
            this.orderTotalId = orderTotalId;
        }
    }

    public static class Products {
        @Expose
        @SerializedName("total")
        private String total;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("quantity")
        private String quantity;
        @Expose
        @SerializedName("option")
        private List<Option> option;
        @Expose
        @SerializedName("model")
        private String model;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("tax")
        private String tax;
        @Expose
        @SerializedName("order_id")
        private String orderId;
        @Expose
        @SerializedName("order_product_id")
        private String orderProductId;
        @Expose
        @SerializedName("product_id")
        private String productId;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public List<Option> getOption() {
            return option;
        }

        public void setOption(List<Option> option) {
            this.option = option;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderProductId() {
            return orderProductId;
        }

        public void setOrderProductId(String orderProductId) {
            this.orderProductId = orderProductId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }
    }

    public static class Option {
        @Expose
        @SerializedName("product_option_value_id")
        private String productOptionValueId;
        @Expose
        @SerializedName("product_option_id")
        private String productOptionId;
        @Expose
        @SerializedName("order_product_id")
        private String orderProductId;
        @Expose
        @SerializedName("order_option_id")
        private String orderOptionId;
        @Expose
        @SerializedName("value")
        private String value;
        @Expose
        @SerializedName("name")
        private String name;

        public String getProductOptionValueId() {
            return productOptionValueId;
        }

        public void setProductOptionValueId(String productOptionValueId) {
            this.productOptionValueId = productOptionValueId;
        }

        public String getProductOptionId() {
            return productOptionId;
        }

        public void setProductOptionId(String productOptionId) {
            this.productOptionId = productOptionId;
        }

        public String getOrderProductId() {
            return orderProductId;
        }

        public void setOrderProductId(String orderProductId) {
            this.orderProductId = orderProductId;
        }

        public String getOrderOptionId() {
            return orderOptionId;
        }

        public void setOrderOptionId(String orderOptionId) {
            this.orderOptionId = orderOptionId;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class OrderInfo {
        @Expose
        @SerializedName("ip")
        private String ip;
        @Expose
        @SerializedName("date_added")
        private String dateAdded;
        @Expose
        @SerializedName("date_modified")
        private String dateModified;
        @Expose
        @SerializedName("currency_value")
        private String currencyValue;
        @Expose
        @SerializedName("currency_code")
        private String currencyCode;
        @Expose
        @SerializedName("currency_id")
        private String currencyId;
        @Expose
        @SerializedName("language_id")
        private String languageId;
        @Expose
        @SerializedName("order_status_id")
        private String orderStatusId;
        @Expose
        @SerializedName("total")
        private String total;
        @Expose
        @SerializedName("comment")
        private String comment;
        @Expose
        @SerializedName("shipping_method")
        private String shippingMethod;
        @Expose
        @SerializedName("shipping_address_format")
        private String shippingAddressFormat;
        @Expose
        @SerializedName("shipping_iso_code_3")
        private String shippingIsoCode3;
        @Expose
        @SerializedName("shipping_iso_code_2")
        private String shippingIsoCode2;
        @Expose
        @SerializedName("shipping_country")
        private String shippingCountry;
        @Expose
        @SerializedName("shipping_country_id")
        private String shippingCountryId;
        @Expose
        @SerializedName("shipping_zone_code")
        private String shippingZoneCode;
        @Expose
        @SerializedName("shipping_zone")
        private String shippingZone;
        @Expose
        @SerializedName("shipping_zone_id")
        private String shippingZoneId;
        @Expose
        @SerializedName("shipping_city")
        private String shippingCity;
        @Expose
        @SerializedName("shipping_postcode")
        private String shippingPostcode;
        @Expose
        @SerializedName("shipping_address_2")
        private String shippingAddress2;
        @Expose
        @SerializedName("shipping_address_1")
        private String shippingAddress1;
        @Expose
        @SerializedName("shipping_company")
        private String shippingCompany;
        @Expose
        @SerializedName("shipping_lastname")
        private String shippingLastname;
        @Expose
        @SerializedName("shipping_firstname")
        private String shippingFirstname;
        @Expose
        @SerializedName("payment_method")
        private String paymentMethod;
        @Expose
        @SerializedName("payment_address_format")
        private String paymentAddressFormat;
        @Expose
        @SerializedName("payment_iso_code_3")
        private String paymentIsoCode3;
        @Expose
        @SerializedName("payment_iso_code_2")
        private String paymentIsoCode2;
        @Expose
        @SerializedName("payment_country")
        private String paymentCountry;
        @Expose
        @SerializedName("payment_country_id")
        private String paymentCountryId;
        @Expose
        @SerializedName("payment_zone_code")
        private String paymentZoneCode;
        @Expose
        @SerializedName("payment_zone")
        private String paymentZone;
        @Expose
        @SerializedName("payment_zone_id")
        private String paymentZoneId;
        @Expose
        @SerializedName("payment_city")
        private String paymentCity;
        @Expose
        @SerializedName("payment_postcode")
        private String paymentPostcode;
        @Expose
        @SerializedName("payment_address_2")
        private String paymentAddress2;
        @Expose
        @SerializedName("payment_address_1")
        private String paymentAddress1;
        @Expose
        @SerializedName("payment_company")
        private String paymentCompany;
        @Expose
        @SerializedName("payment_lastname")
        private String paymentLastname;
        @Expose
        @SerializedName("payment_firstname")
        private String paymentFirstname;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("fax")
        private String fax;
        @Expose
        @SerializedName("telephone")
        private String telephone;
        @Expose
        @SerializedName("lastname")
        private String lastname;
        @Expose
        @SerializedName("firstname")
        private String firstname;
        @Expose
        @SerializedName("customer_id")
        private String customerId;
        @Expose
        @SerializedName("store_url")
        private String storeUrl;
        @Expose
        @SerializedName("store_name")
        private String storeName;
        @Expose
        @SerializedName("store_id")
        private String storeId;
        @Expose
        @SerializedName("invoice_prefix")
        private String invoicePrefix;
        @Expose
        @SerializedName("invoice_no")
        private String invoiceNo;
        @Expose
        @SerializedName("order_id")
        private String orderId;

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public String getDateModified() {
            return dateModified;
        }

        public void setDateModified(String dateModified) {
            this.dateModified = dateModified;
        }

        public String getCurrencyValue() {
            return currencyValue;
        }

        public void setCurrencyValue(String currencyValue) {
            this.currencyValue = currencyValue;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getCurrencyId() {
            return currencyId;
        }

        public void setCurrencyId(String currencyId) {
            this.currencyId = currencyId;
        }

        public String getLanguageId() {
            return languageId;
        }

        public void setLanguageId(String languageId) {
            this.languageId = languageId;
        }

        public String getOrderStatusId() {
            return orderStatusId;
        }

        public void setOrderStatusId(String orderStatusId) {
            this.orderStatusId = orderStatusId;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getShippingMethod() {
            return shippingMethod;
        }

        public void setShippingMethod(String shippingMethod) {
            this.shippingMethod = shippingMethod;
        }

        public String getShippingAddressFormat() {
            return shippingAddressFormat;
        }

        public void setShippingAddressFormat(String shippingAddressFormat) {
            this.shippingAddressFormat = shippingAddressFormat;
        }

        public String getShippingIsoCode3() {
            return shippingIsoCode3;
        }

        public void setShippingIsoCode3(String shippingIsoCode3) {
            this.shippingIsoCode3 = shippingIsoCode3;
        }

        public String getShippingIsoCode2() {
            return shippingIsoCode2;
        }

        public void setShippingIsoCode2(String shippingIsoCode2) {
            this.shippingIsoCode2 = shippingIsoCode2;
        }

        public String getShippingCountry() {
            return shippingCountry;
        }

        public void setShippingCountry(String shippingCountry) {
            this.shippingCountry = shippingCountry;
        }

        public String getShippingCountryId() {
            return shippingCountryId;
        }

        public void setShippingCountryId(String shippingCountryId) {
            this.shippingCountryId = shippingCountryId;
        }

        public String getShippingZoneCode() {
            return shippingZoneCode;
        }

        public void setShippingZoneCode(String shippingZoneCode) {
            this.shippingZoneCode = shippingZoneCode;
        }

        public String getShippingZone() {
            return shippingZone;
        }

        public void setShippingZone(String shippingZone) {
            this.shippingZone = shippingZone;
        }

        public String getShippingZoneId() {
            return shippingZoneId;
        }

        public void setShippingZoneId(String shippingZoneId) {
            this.shippingZoneId = shippingZoneId;
        }

        public String getShippingCity() {
            return shippingCity;
        }

        public void setShippingCity(String shippingCity) {
            this.shippingCity = shippingCity;
        }

        public String getShippingPostcode() {
            return shippingPostcode;
        }

        public void setShippingPostcode(String shippingPostcode) {
            this.shippingPostcode = shippingPostcode;
        }

        public String getShippingAddress2() {
            return shippingAddress2;
        }

        public void setShippingAddress2(String shippingAddress2) {
            this.shippingAddress2 = shippingAddress2;
        }

        public String getShippingAddress1() {
            return shippingAddress1;
        }

        public void setShippingAddress1(String shippingAddress1) {
            this.shippingAddress1 = shippingAddress1;
        }

        public String getShippingCompany() {
            return shippingCompany;
        }

        public void setShippingCompany(String shippingCompany) {
            this.shippingCompany = shippingCompany;
        }

        public String getShippingLastname() {
            return shippingLastname;
        }

        public void setShippingLastname(String shippingLastname) {
            this.shippingLastname = shippingLastname;
        }

        public String getShippingFirstname() {
            return shippingFirstname;
        }

        public void setShippingFirstname(String shippingFirstname) {
            this.shippingFirstname = shippingFirstname;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getPaymentAddressFormat() {
            return paymentAddressFormat;
        }

        public void setPaymentAddressFormat(String paymentAddressFormat) {
            this.paymentAddressFormat = paymentAddressFormat;
        }

        public String getPaymentIsoCode3() {
            return paymentIsoCode3;
        }

        public void setPaymentIsoCode3(String paymentIsoCode3) {
            this.paymentIsoCode3 = paymentIsoCode3;
        }

        public String getPaymentIsoCode2() {
            return paymentIsoCode2;
        }

        public void setPaymentIsoCode2(String paymentIsoCode2) {
            this.paymentIsoCode2 = paymentIsoCode2;
        }

        public String getPaymentCountry() {
            return paymentCountry;
        }

        public void setPaymentCountry(String paymentCountry) {
            this.paymentCountry = paymentCountry;
        }

        public String getPaymentCountryId() {
            return paymentCountryId;
        }

        public void setPaymentCountryId(String paymentCountryId) {
            this.paymentCountryId = paymentCountryId;
        }

        public String getPaymentZoneCode() {
            return paymentZoneCode;
        }

        public void setPaymentZoneCode(String paymentZoneCode) {
            this.paymentZoneCode = paymentZoneCode;
        }

        public String getPaymentZone() {
            return paymentZone;
        }

        public void setPaymentZone(String paymentZone) {
            this.paymentZone = paymentZone;
        }

        public String getPaymentZoneId() {
            return paymentZoneId;
        }

        public void setPaymentZoneId(String paymentZoneId) {
            this.paymentZoneId = paymentZoneId;
        }

        public String getPaymentCity() {
            return paymentCity;
        }

        public void setPaymentCity(String paymentCity) {
            this.paymentCity = paymentCity;
        }

        public String getPaymentPostcode() {
            return paymentPostcode;
        }

        public void setPaymentPostcode(String paymentPostcode) {
            this.paymentPostcode = paymentPostcode;
        }

        public String getPaymentAddress2() {
            return paymentAddress2;
        }

        public void setPaymentAddress2(String paymentAddress2) {
            this.paymentAddress2 = paymentAddress2;
        }

        public String getPaymentAddress1() {
            return paymentAddress1;
        }

        public void setPaymentAddress1(String paymentAddress1) {
            this.paymentAddress1 = paymentAddress1;
        }

        public String getPaymentCompany() {
            return paymentCompany;
        }

        public void setPaymentCompany(String paymentCompany) {
            this.paymentCompany = paymentCompany;
        }

        public String getPaymentLastname() {
            return paymentLastname;
        }

        public void setPaymentLastname(String paymentLastname) {
            this.paymentLastname = paymentLastname;
        }

        public String getPaymentFirstname() {
            return paymentFirstname;
        }

        public void setPaymentFirstname(String paymentFirstname) {
            this.paymentFirstname = paymentFirstname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getStoreUrl() {
            return storeUrl;
        }

        public void setStoreUrl(String storeUrl) {
            this.storeUrl = storeUrl;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getInvoicePrefix() {
            return invoicePrefix;
        }

        public void setInvoicePrefix(String invoicePrefix) {
            this.invoicePrefix = invoicePrefix;
        }

        public String getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(String invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }
    }
}
