package net.collection.collection.Networks.Events;

import net.collection.collection.Networks.ResponseModels.ImagesCommentsDiscountDataResponseModel;

public class ImagesCommentsDiscountDataEvent {
    private ImagesCommentsDiscountDataResponseModel imagesCommentsDiscountDataResponseModel;

    public ImagesCommentsDiscountDataEvent(ImagesCommentsDiscountDataResponseModel imagesCommentsDiscountDataResponseModel) {
        this.imagesCommentsDiscountDataResponseModel = imagesCommentsDiscountDataResponseModel;
    }

    public ImagesCommentsDiscountDataResponseModel getImagesCommentsDiscountDataResponseModel() {
        return imagesCommentsDiscountDataResponseModel;
    }

    public void setImagesCommentsDiscountDataResponseModel(ImagesCommentsDiscountDataResponseModel imagesCommentsDiscountDataResponseModel) {
        this.imagesCommentsDiscountDataResponseModel = imagesCommentsDiscountDataResponseModel;
    }
}
