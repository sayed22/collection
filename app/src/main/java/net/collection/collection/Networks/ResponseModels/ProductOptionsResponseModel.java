package net.collection.collection.Networks.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductOptionsResponseModel {

    @Expose
    @SerializedName("products_option")
    private List<ProductsOption> productsOption;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<ProductsOption> getProductsOption() {
        return productsOption;
    }

    public void setProductsOption(List<ProductsOption> productsOption) {
        this.productsOption = productsOption;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class ProductsOption {
        @Expose
        @SerializedName("required")
        private String required;
        @Expose
        @SerializedName("value")
        private String value;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("option_id")
        private String optionId;
        @Expose
        @SerializedName("product_option_value")
        private List<ProductOptionValue> productOptionValue;
        @Expose
        @SerializedName("product_option_id")
        private String productOptionId;

        public String getRequired() {
            return required;
        }

        public void setRequired(String required) {
            this.required = required;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOptionId() {
            return optionId;
        }

        public void setOptionId(String optionId) {
            this.optionId = optionId;
        }

        public List<ProductOptionValue> getProductOptionValue() {
            return productOptionValue;
        }

        public void setProductOptionValue(List<ProductOptionValue> productOptionValue) {
            this.productOptionValue = productOptionValue;
        }

        public String getProductOptionId() {
            return productOptionId;
        }

        public void setProductOptionId(String productOptionId) {
            this.productOptionId = productOptionId;
        }
    }

    public static class ProductOptionValue {
        @Expose
        @SerializedName("opt_image")
        private int optImage;
        @Expose
        @SerializedName("price_prefix")
        private String pricePrefix;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("option_value_id")
        private String optionValueId;
        @Expose
        @SerializedName("product_option_value_id")
        private String productOptionValueId;

        public int getOptImage() {
            return optImage;
        }

        public void setOptImage(int optImage) {
            this.optImage = optImage;
        }

        public String getPricePrefix() {
            return pricePrefix;
        }

        public void setPricePrefix(String pricePrefix) {
            this.pricePrefix = pricePrefix;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOptionValueId() {
            return optionValueId;
        }

        public void setOptionValueId(String optionValueId) {
            this.optionValueId = optionValueId;
        }

        public String getProductOptionValueId() {
            return productOptionValueId;
        }

        public void setProductOptionValueId(String productOptionValueId) {
            this.productOptionValueId = productOptionValueId;
        }
    }
}
